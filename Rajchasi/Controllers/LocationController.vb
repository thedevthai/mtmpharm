﻿Imports Microsoft.ApplicationBlocks.Data

Public Class LocationController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function Location_GetAll() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Location_GetAll"))
        Return ds.Tables(0)
    End Function

    Public Function Location_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Location_Get"))
        Return ds.Tables(0)
    End Function
    Public Function Location_GetReport(pType As String, pProvince As String, ProjID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "RPT_LocationProject", pType, pProvince, ProjID)
        Return ds.Tables(0)
    End Function

    Public Function Location_GetByID(id As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Location_GetByID"), id)
        Return ds.Tables(0)
    End Function

    Public Function Location_GetHospital(lid As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Location_GetHospital"), lid)
        Return ds.Tables(0)
    End Function
    Public Function Location_GetHospitalByLocationName(lid As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Location_GetHospitalByLocationName"), lid)
        Return ds.Tables(0)
    End Function


    'Public Function Location_GetBySearch(prmKey As String) As DataTable
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Location_GetBySearch"), prmKey)
    '    Return ds.Tables(0)
    'End Function

    Public Function Location_GetBySearchAll(provid As String, id As String) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Location_GetByProvinceIDAll"), provid, id)
        Return ds.Tables(0)
    End Function

    Public Function Location_GetBySearch(id As String) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Location_GetBySearch"), id)
        Return ds.Tables(0)
    End Function

    Public Function Location_GetByProvince(provid As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Location_GetByProvinceID"), provid)
        Return ds.Tables(0)
    End Function
    Public Function Location_GetByProvinceGroup(provGroup As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Location_GetByProvinceGroup"), provGroup)
        Return ds.Tables(0)
    End Function


    Public Function Location_GetNameByID(id As String) As String
        SQL = "select LocationName  from Locations  where Locationid='" & id & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)("LocationName")
        Else
            Return ""
        End If

    End Function


    Public Function Location_Add(ByVal LocationID As String, ByVal LocationName As String, ByVal LocationGroupID As String, TypeShop As String, TypeName As String, ByVal Address As String, ByVal ProvinceID As String, ByVal ProvinceName As String, ByVal ZipCode As String, ByVal Office_Tel As String, ByVal Office_Fax As String, ByVal Co_Name As String, ByVal Co_Mail As String, ByVal Co_Tel As String, ByVal AccNo As String, ByVal AccName As String, ByVal BankID As String, ByVal BankBrunch As String, ByVal BankType As String, ByVal UpdBy As String, ByVal isPublic As Integer, ByVal Office_Mail As String, CardID As String, RYear As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Location_Add"), LocationID, LocationName, LocationGroupID, TypeShop, TypeName, Address, ProvinceID, ProvinceName, ZipCode, Office_Tel, Office_Fax, Co_Name, Co_Mail, Co_Tel, AccNo, AccName, BankID, BankBrunch, BankType, UpdBy, isPublic, Office_Mail, CardID, RYear)
    End Function

    Public Function Location_Update(ByVal LocationID_Old As String, ByVal LocationID_New As String, ByVal LocationName As String, ByVal LocationGroupID As String, TypeShop As String, TypeName As String, ByVal Address As String, ByVal ProvinceID As String, ByVal ProvinceName As String, ByVal ZipCode As String, ByVal Office_Tel As String, ByVal Office_Fax As String, ByVal Co_Name As String, ByVal Co_Mail As String, ByVal Co_Tel As String, ByVal AccNo As String, ByVal AccName As String, ByVal BankID As String, ByVal BankBrunch As String, ByVal BankType As String, ByVal UpdBy As String, ByVal isPublic As Integer, ByVal Office_Mail As String, CardID As String, RYear As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Location_Update", LocationID_Old, LocationID_New, LocationName, LocationGroupID, TypeShop, TypeName, Address, ProvinceID, ProvinceName, ZipCode, Office_Tel, Office_Fax, Co_Name, Co_Mail, Co_Tel, AccNo, AccName, BankID, BankBrunch, BankType, UpdBy, isPublic, Office_Mail, CardID, RYear)

    End Function

    Public Function Location_UpdateByUser(ByVal LocationID As String, ByVal Code As String, ByVal LocationName As String, TypeShop As String, TypeName As String, ByVal Address As String, ByVal ZipCode As String, ByVal Office_Tel As String, ByVal Office_Fax As String, ByVal Co_Name As String, ByVal Co_Mail As String, ByVal Co_Tel As String, ByVal AccNo As String, ByVal AccName As String, ByVal BankID As String, ByVal BankBrunch As String, ByVal BankType As String, ByVal UpdBy As String, ByVal Office_Mail As String, CardID As String, RYear As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Location_UpdateByUser", LocationID, Code, LocationName, TypeShop, TypeName, Address, ZipCode, Office_Tel, Office_Fax, Co_Name, Co_Mail, Co_Tel, AccNo, AccName, BankID, BankBrunch, BankType, UpdBy, Office_Mail, CardID, RYear)

    End Function

    Public Function Location_Delete(ByVal pID As String) As Integer
        SQL = "delete from Locations where Locationid ='" & pID & "'"
        Return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, SQL)
    End Function

    Public Function LocationHospital_Save(ByVal LocationID As String, ByVal HospitalUID As Integer, LocationName As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LocationHospital_Save"), LocationID, HospitalUID, LocationName)
    End Function
    Public Function LocationHospital_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LocationHospital_Delete"), UID)
    End Function

    Public Function LocationHospital_UpdateLocation(ByVal LocationID As String, ByVal SoundexName As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LocationHospital_UpdateLocation"), LocationID, SoundexName)
    End Function
End Class

Public Class LocationGroupController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function LocationGroup_Get() As DataTable

        SQL = "select * from  LocationGroup"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function

    Public Function LocationGroup_ByID(id As String) As DataSet
        SQL = "select * from LocationGroup where code='" & id & "'"
        Return SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
    End Function

    Public Function LocationGroup_Add(ByVal pCode As String, ByVal pName As String, desc As String, ByVal pStatus As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LocationGroup_Add"), pCode, pName, desc, pStatus)
    End Function

    Public Function LocationGroup_Update(ByVal pID_old As String, ByVal pID_new As String, ByVal pName As String, desc As String, ByVal pStatus As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LocationGroup_Update", pID_old, pID_new, pName, desc, pStatus)
    End Function

    Public Function LocationGroup_Delete(ByVal pID As String) As Integer
        SQL = "delete from LocationGroup where code ='" & pID & "'"
        Return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, SQL)
    End Function



End Class