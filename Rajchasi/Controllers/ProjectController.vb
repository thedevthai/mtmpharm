﻿Imports Microsoft.ApplicationBlocks.Data

Public Class ProjectController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet
    Public Function Project_GetAll() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Project_GetAll")
        Return ds.Tables(0)
    End Function
    Public Function Project_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Project_Get")
        Return ds.Tables(0)
    End Function

    Public Function Project_GetByUser(userid As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Project_GetByUser", userid)
        Return ds.Tables(0)
    End Function

    Public Function Project_GetName(ProjID As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Project_GetName", ProjID)

        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Str(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If
    End Function

End Class
