﻿Imports Microsoft.ApplicationBlocks.Data

Public Class ReferController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function Get_ReferReason() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("ReferenceValue_GetListByDomainCode"), "REFERREAS")
        Return ds.Tables(0)
    End Function

    Public Function Refer_Add(ReferDate As String, ByVal PatientID As Integer, Destination As String, ByVal ReferFrom As String, Remark As String, ReferBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Refer_Add"), ConvertStrDate2InformDateString(ReferDate) _
           , PatientID _
           , Destination _
           , ReferFrom _
           , Remark _
           , ReferBy)
    End Function

    Public Function Refer_CheckDup(ByVal PatientID As Integer) As Boolean

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Refer_CheckDup"), PatientID)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)(0) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function


    Public Function Refer_GetByPatient(ByVal PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Refer_GetByPatient"), PatientID)
        Return ds.Tables(0)
    End Function

    Public Function Refer_Get(MTMUID As Integer, PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Refer_Get", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function FollowUpRefer_Get(ReferID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "FollowUpRefer_Get", ReferID)
        Return ds.Tables(0)
    End Function

    Public Function GenFollowupCount(CRow As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "GenFollowupCount", CRow)
        Return ds.Tables(0)
    End Function
    Public Function Refer_Save(ReferID As Integer, ReferDate As String, PCU As String, LocationID As String, ByVal HN As String, Reason As String, ByVal ReferFrom As String, Remark As String, FollowCount As Integer, ReferBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Refer_Save"), ReferID, ConvertStrDate2InformDateString(ReferDate), PCU, LocationID _
           , HN _
           , Reason _
           , Remark, FollowCount _
           , ReferBy)
    End Function

    Public Function Refer_SaveDefineDate(ReferID As Integer, SEQ As Integer, DefineDate As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Refer_SaveDefineDate"), ReferID, SEQ, ConvertStrDate2InformDateString(DefineDate))
    End Function



    Public Function Refer_GetID(ReferDate As String, PCU As String, LocationID As String, ByVal HN As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Refer_GetID"), ConvertStrDate2InformDateString(ReferDate), PCU, LocationID, HN)
        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)(0)
        Else
            Return 0
        End If
    End Function

    Public Function Refer_Delete(
       ByVal MTMUID As Integer _
     , ByVal PatientID As Integer _
     , ByVal MUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Refer_Delete", MTMUID, PatientID, MUser)

    End Function

    Public Function Refer_GetByID(ByVal RID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Refer_GetByID"), RID)
        Return ds.Tables(0)
    End Function
    Public Function Refer_GetSearch(ByVal Search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Refer_GetSearch"), Search)
        Return ds.Tables(0)
    End Function
    Public Function Refer_GetSearch(PCU As String, ByVal Search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Refer_GetSearchByPCU"), PCU, Search)
        Return ds.Tables(0)
    End Function

    Public Function Refer_GetReceive(PCU As String, ByVal Search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Refer_GetReceiveByPCU"), PCU, Search)
        Return ds.Tables(0)
    End Function

    Public Function Refer_GetSearchByLocation(LocationID As String, ByVal Search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Refer_GetSearchByLocation"), LocationID, Search)
        Return ds.Tables(0)
    End Function
    Public Function Refer_GetLocation(ByVal PatientID As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Refer_GetLocation"), PatientID)
        Return ds.Tables(0).Rows(0)(0).ToString()
    End Function

    Public Function Refer_Delete(ReferID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Refer_Delete", ReferID)
    End Function
End Class
