﻿Imports Microsoft.ApplicationBlocks.Data
Public Class MTMController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

#Region "MTM 2563"
    Public Function MTM_GetVisit(PatientID As Integer, UserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_GetVisit", PatientID, UserID)
        Return ds.Tables(0)
    End Function

    Public Function MTM_Desease_Get(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "PatientDesease_Get2", PatientID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_Desease_Get2(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "PatientDesease_Get2", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function MTM_Desease_Get(PatientID As Integer, LocationID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "PatientDesease_GetByLocation", PatientID, LocationID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_DrugProblem_GetByUser(PatientID As Integer, UserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_DrugProblem_Get", PatientID, UserID)
        Return ds.Tables(0)
    End Function

    Public Function MTM_GetDocument(PatientID As Integer, UserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_GetDocument", PatientID, UserID)
        Return ds.Tables(0)
    End Function

    Public Function MTM_Master_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetByUID", pUID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_Master_GetLastInfo(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetLastInfo", PatientID)
        Return ds.Tables(0)
    End Function


    Public Function MTM_Master_GetBYear(UID As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetBYear", UID)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function

    Public Function MTM_Master_GetByProvinceGroup(pGRPID As String, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetByProvinceGroup", pGRPID, pType, pKey, pStatus, pProv, pYear)


        Return ds.Tables(0)
    End Function

    'Public Function MTM_Master_GetByProjectID(pProjID As Integer, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetByProjectID", pProjID, pType, pKey, pStatus, pProv, pYear)
    '    Return ds.Tables(0)
    'End Function

    Public Function MTM_Master_GetTypeIDByItemID(id As Integer, pid As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Smoking_GetTypeIDByItemID", id, pid)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Str(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If
    End Function

    Public Function MTM_Master_GetPatientIDByItemID(pID As Long) As Long
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("MTM_Master_GetPatientID"), pID)
        Try
            If ds.Tables(0).Rows.Count > 0 Then
                Return DBNull2Lng(ds.Tables(0).Rows(0)(0))
            Else
                Return 0
            End If
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Public Function MTM_Master_GetLastUID(ByVal LocationID As String _
           , ByVal PatientID As Integer _
           , ByVal ServiceDate As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetLastUID", LocationID, PatientID, ServiceDate)

        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function MTM_Master_GetLastSEQ(ByVal LocationID As String _
           , ByVal PatientID As Integer, MTMTYPE As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetLastSEQByMTMTYPE", LocationID, PatientID, MTMTYPE)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If

    End Function

    Public Function MTM_Master_ChkDupCustomer(PatientID As Integer, ServiceDate As Integer) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("MTM_Master_ChkDupCustomer"), PatientID, ServiceDate)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)(0) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Function MTM_Master_GetByStatus(pLID As String, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetByStatus", pLID, pType, pKey, pStatus, pProv, pYear)
        Return ds.Tables(0)
    End Function

    Public Function MTM_Master_Delete(itemID As Long) As Integer
        SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("MTM_Master_Delete"), itemID)
    End Function


    Public Function MTM_Master_Add(
             ByVal LocationID As String _
           , ByVal PatientID As Integer _
           , ByVal ServiceDate As Integer _
           , ByVal ServiceTime As Integer _
           , ByVal PersonID As Integer _
           , ByVal Smoke As Integer _
           , ByVal SmokeYear As Integer _
           , ByVal SmokeCigarette As Integer _
           , ByVal CigaretteType As Integer _
           , ByVal Alcohol As Integer _
           , ByVal AlcoholFQ As Integer _
           , ByVal MedicationUsed1 As String _
           , ByVal MedicationUsed2 As String _
           , ByVal MedicationUsed3 As String _
           , ByVal MedicationUsed4 As String _
           , ByVal MedicationUsed5 As String _
           , ByVal MedicationUsed6 As String _
           , ByVal MedicationUsed7 As String _
           , ByVal HospitalType As Integer _
           , ByVal HospitalName As String _
           , ByVal Status As Integer _
           , ByVal CreateBy As String, ByVal SEQ As Integer, MTMTYPE As String, PFrom As String, FromTXT As String, MTMService As String, ServiceRemark As String, ServiceRef As String _
           , TelepharmacyMethod As String _
           , RecordMethod As String _
           , RecordLocation As String, TelepharmRemark As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Master_Add", LocationID, PatientID, ServiceDate, ServiceTime, PersonID, Smoke, SmokeYear, SmokeCigarette, CigaretteType, Alcohol, AlcoholFQ, MedicationUsed1, MedicationUsed2, MedicationUsed3, MedicationUsed4, MedicationUsed5, MedicationUsed6, MedicationUsed7, HospitalType, HospitalName, Status, CreateBy, SEQ, MTMTYPE, PFrom, FromTXT, MTMService, ServiceRemark, ServiceRef, TelepharmacyMethod, RecordMethod, RecordLocation, TelepharmRemark)


    End Function
    Public Function MTM_Master_Update(ByVal UID As Integer,
             ByVal LocationID As String _
           , ByVal PatientID As Integer _
           , ByVal ServiceDate As Integer _
           , ByVal ServiceTime As Integer _
           , ByVal PersonID As Integer _
           , ByVal Smoke As Integer _
           , ByVal SmokeYear As Integer _
           , ByVal SmokeCigarette As Integer _
           , ByVal CigaretteType As Integer _
           , ByVal Alcohol As Integer _
           , ByVal AlcoholFQ As Integer _
           , ByVal MedicationUsed1 As String _
           , ByVal MedicationUsed2 As String _
           , ByVal MedicationUsed3 As String _
           , ByVal MedicationUsed4 As String _
           , ByVal MedicationUsed5 As String _
           , ByVal MedicationUsed6 As String _
           , ByVal MedicationUsed7 As String _
           , ByVal HospitalType As Integer _
           , ByVal HospitalName As String _
           , ByVal Status As Integer _
           , ByVal CreateBy As String, MTMTYPE As String, PFrom As String, FromTXT As String, MTMService As String, ServiceRemark As String, ServiceRef As String _
           , TelepharmacyMethod As String _
           , RecordMethod As String _
           , RecordLocation As String, TelepharmRemark As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Master_Update", UID, LocationID, PatientID, ServiceDate, ServiceTime, PersonID, Smoke, SmokeYear, SmokeCigarette, CigaretteType, Alcohol, AlcoholFQ, MedicationUsed1, MedicationUsed2, MedicationUsed3, MedicationUsed4, MedicationUsed5, MedicationUsed6, MedicationUsed7, HospitalType, HospitalName, Status, CreateBy, MTMTYPE, PFrom, FromTXT, MTMService, ServiceRemark, ServiceRef, TelepharmacyMethod, RecordMethod, RecordLocation, TelepharmRemark)


    End Function


    Public Function MTM_UpdatePE(ByVal UID As Integer,
             ByVal isPitting As String _
           , ByVal isWound As String _
           , ByVal isPeripheral As String _
           , ByVal Pitting As String _
           , ByVal Wound As String _
           , ByVal Peripheral As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_UpdatePE", UID, isPitting, isWound, isPeripheral, Pitting, Wound, Peripheral)

    End Function



#End Region

#Region "Drug"

    Public Function MTM_DrugProblem_Get(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_DrugProblem_Get", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function MTM_DrugProblem_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_DrugProblem_GetByUID", pUID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_DrugProblem_Save(
            ByVal MTMUID As Integer _
          , ByVal UID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal MedUID As Integer _
          , ByVal ProblemGroupUID As String _
          , ByVal ProblemUID As String _
          , ByVal ProblemOther As String _
          , ByVal Intervention As String _
          , ByVal ResultID As String _
          , ByVal ResultOther As String _
          , ByVal PatientID As Integer _
          , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DrugProblem_Save", MTMUID, UID, ServiceTypeID, MedUID, ProblemGroupUID, ProblemUID, ProblemOther, Intervention, ResultID, ResultOther, PatientID, CUser)

    End Function
    Public Function MTM_DrugProblem_Delete(ByVal MTMUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DrugProblem_Delete", MTMUID)
    End Function
#End Region
#Region "Desease Relate"
    Public Function MTM_DeseaseRelate_Get(MTMUID As Integer, PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "PatientDesease_Get", PatientID)
        Return ds.Tables(0)
    End Function

    'Public Function MTM_DeseaseRelate_Add(
    '        ByVal MTMUID As Integer, ServiceTypeID As String _
    '      , ByVal ICDCode As String _
    '      , ByVal DeseaseOther As String _
    '      , ByVal PatientID As Integer _
    '      , ByVal CreateBy As Integer) As Integer

    '    Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DeseaseRelate_Add", MTMUID, ServiceTypeID, ICDCode, DeseaseOther, PatientID, CreateBy)

    'End Function
    Public Function MTM_DeseaseRelate_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DeseaseRelate_Delete", UID)
    End Function

#End Region
#Region "Behavior Relate"
    Public Function MTM_BehaviorProblem_Get(UID As Integer, patientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_BehaviorProblem_Get", UID, patientID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_BehaviorProblem_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_BehaviorProblem_GetByUID", UID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_BehaviorProblem_Save(
            ByVal MTMUID As Integer _
          , ByVal UID As Integer _
          , ByVal BehaviorUID As Integer _
          , ByVal Other As String _
          , ByVal Intervention As String _
          , ByVal FinalResult As String _
          , ByVal FinalResultOther As String _
          , ByVal ResultBegin As String _
          , ByVal ResultEnd As String _
          , ByVal FatFollow As String _
          , ByVal Remark As String _
          , ByVal isFollow As String _
          , ByVal PatientID As Integer _
          , ByVal CreateBy As Integer, TasteFollow As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_BehaviorProblem_Save", MTMUID, UID, BehaviorUID, Other, Intervention, FinalResult, FinalResultOther, ResultBegin, ResultEnd, FatFollow, Remark, isFollow, PatientID, CreateBy, TasteFollow)

    End Function
    Public Function MTM_BehaviorProblem_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_BehaviorProblem_Delete", UID)
    End Function

#End Region
#Region "Hospital"
    Public Function MTM_Hospital_Get(MTMUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Hospital_Get", MTMUID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_Hospital_GetByPatient(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Hospital_GetByPatient", PatientID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_Hospital_Add(ByVal MTMUID As Integer, TypeID As String, ByVal Name As String, PatientID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Hospital_Add", MTMUID, TypeID, Name, PatientID)

    End Function
    Public Function MTM_Hospital_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Hospital_Delete", UID)
    End Function

#End Region

#Region "LAB"
    Public Function LabResult1_Get(HUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "LabResult1_Get", HUID)
        Return ds.Tables(0)
    End Function
    Public Function HP2P_LabResult_Get(HUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "HP2P_LabResult_Get", HUID)
        Return ds.Tables(0)
    End Function

    Public Function HP2P_LabResult_GetByLocation(HUID As Integer, LocationID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "HP2P_LabResult_GetByLocation", HUID, LocationID)
        Return ds.Tables(0)
    End Function
    Public Function HP2P_LabResult_GetByRefillHeaderUID(HUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "HP2P_LabResult_GetByRefillHeaderUID", HUID)
        Return ds.Tables(0)
    End Function

    Public Function LabResult2_Get(HUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "LabResult2_Get", HUID)
        Return ds.Tables(0)
    End Function

    Public Function LabResult_Save(
            ByVal RefUID As Integer _
          , ByVal LabUID As Integer _
          , ByVal ResultDate As Integer _
          , ByVal PatientID As Integer _
          , ByVal ResultValue As String _
          , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LabResult_Save", RefUID, LabUID, ResultDate, PatientID, ResultValue, CUser)

    End Function


#End Region

#Region "Drug Remain"

    Public Function MTM_DrugRemain_Get(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_DrugRemain_Get", PatientID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_DrugRemain_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_DrugRemain_GetByUID", UID)
        Return ds.Tables(0)
    End Function

    Public Function MTM_DrugRemain_Get(MTMUID As Integer, PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_DrugRemain_Get", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function MTM_DrugRemain_Save(
            ByVal MTMUID As Integer _
          , ByVal UID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal MedCode As String _
          , ByVal QTY As Double _
          , ByVal UOM As String _
          , ByVal Hospital As String _
          , ByVal ReasonUID As Integer _
          , ByVal ReasonRemark As String _
          , ByVal PatientID As Integer _
          , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DrugRemain_Save", MTMUID, UID, ServiceTypeID, MedCode, QTY, UOM, Hospital, ReasonUID, ReasonRemark, PatientID, CUser)

    End Function
    Public Function MTM_DrugRemain_Delete(ByVal MTMUID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DrugRemain_Delete", MTMUID)

    End Function

#End Region


#Region "Drug Order"
    Public Function DrugOrder_Get(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugOrder_Get", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function DrugOrder_Save(ServiceUID As Integer, SEQ As Integer, itemID As Integer, itemName As String, Frequency As String, QTY As Integer, MUser As String, ServiceTypeID As String, LocationID As String, StatusFlag As String, patientid As Long) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DrugOrder_Save", ServiceUID, SEQ, itemID, itemName, Frequency, QTY, MUser, ServiceTypeID, LocationID, StatusFlag, patientid)
    End Function

    Public Function DrugOrder_Save(
            ByVal ReferID As Integer _
          , ByVal MedCode As String _
          , ByVal QTY As Double _
          , ByVal UOM As String _
          , ByVal UsedRemark As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DrugOrder_Save", ReferID, MedCode, QTY, UOM, UsedRemark)

    End Function

    Public Function DrugOrder_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DrugOrder_Delete", UID)
    End Function
#End Region

#Region "Drug Refill"
    Public Function DrugRefill_Get(ByVal PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefill_GetByPatient", PatientID)
        Return ds.Tables(0)
    End Function
    Public Function DrugRefill_GetByLocation(ByVal LocationID As String, ByVal PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefill_GetByLocationID", LocationID, PatientID)
        Return ds.Tables(0)
    End Function
    Public Function DrugRefill_Get(ByVal MTMUID As Integer, ByVal PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefill_GetByMTM", MTMUID, PatientID)
        Return ds.Tables(0)
    End Function
    Public Function DrugRefill_GetByDate(RefillDate As String, PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefill_GetByDate", RefillDate, PatientID)
        Return ds.Tables(0)
    End Function

    Public Function HP2P_DrugRefill_GetByRefer(ReferID As Integer, PatientID As Integer, SEQ As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "HP2P_DrugRefill_GetByRefer", ReferID, PatientID, SEQ)
        Return ds.Tables(0)
    End Function

    Public Function HP2P_DrugRefill_GetByRefillUID(RefillUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "HP2P_DrugRefill_GetByRefillUID", RefillUID)
        Return ds.Tables(0)
    End Function

    Public Function MTM_DrugRefill_Save(
            ByVal MTMUID As Integer _
          , ByVal UID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal MedCode As String _
          , ByVal QTY As Double _
          , ByVal UOM As String _
          , ByVal UsedRemark As String _
          , ByVal PatientID As Integer _
          , ByVal CUser As String, ByVal RefillDate As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DrugRefill_Save", MTMUID, UID, ServiceTypeID, MedCode, QTY, UOM, UsedRemark, PatientID, CUser, RefillDate)

    End Function
    Public Function DrugRefillHeader_GetUID(ByVal RefillDate As String, ByVal PatientID As Integer, ByVal LocationID As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefillHeader_GetUID", RefillDate, PatientID, LocationID)
        Return DBNull2Lng(ds.Tables(0).Rows(0)(0))
    End Function


    Public Function DrugRefillHeader_Add(
            ByVal RefillDate As String, ByVal PatientID As Integer, ByVal LocationID As String _
          , ByVal Remark As String _
          , ByVal MTMUID As Integer _
          , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DrugRefillHeader_Add", RefillDate, PatientID, LocationID, Remark, MTMUID, CUser)

    End Function
    Public Function HP2P_DrugRefillHeader_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "HP2P_DrugRefillHeader_GetByUID", pUID)
        Return ds.Tables(0)
    End Function


    Public Function DrugRefill_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefill_GetByUID", pUID)
        Return ds.Tables(0)
    End Function
    Public Function DrugRefill_GetByRefillNo(RNO As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefill_GetByRefillNo", RNO)
        Return ds.Tables(0)
    End Function

    Public Function DrugRefill_GetLastSEQ(ByVal LocationID As String, ByVal PatientID As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefill_GetLastSEQ", LocationID, PatientID)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If

    End Function
    Public Function DrugRefill_GetByPatient(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefill_GetByPatient", PatientID)
        Return ds.Tables(0)
    End Function
    Public Function HP2P_DrugRefillHistory_GetByPatient(LocationID As String, PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "HP2P_DrugRefillHistory_GetByPatient", LocationID, PatientID)
        Return ds.Tables(0)
    End Function
    Public Function HP2P_DrugRefillHistory_GetByLocation(LocationID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "HP2P_DrugRefillHistory_GetByLocation", LocationID)
        Return ds.Tables(0)
    End Function

    Public Function HP2P_DrugRefillHistory_GetByLocation(LocationID As String, Search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "HP2P_DrugRefillHistory_GetSearchByLocation", LocationID, Search)
        Return ds.Tables(0)
    End Function

    Public Function DrugRefill_Delete(ByVal UID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DrugRefill_Delete", UID)

    End Function

    Public Function DrugRefill_Copy(HeadUID As Integer, ByVal ReferID As Integer, CUser As Integer, SEQ As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DrugRefill_Copy", HeadUID, ReferID, CUser, SEQ)
    End Function

    Public Function DrugRefillHeader_GetUID(ByVal RefillDate As String, ByVal PatientID As Integer, ByVal LocationID As String, SEQ As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "HP2P_DrugRefillHeader_GetUID", RefillDate, PatientID, LocationID, SEQ)
        Return DBNull2Lng(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function DrugRefillHeader_Delete(ByVal UID As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefillHeader_Delete", UID)
        Return DBNull2Lng(ds.Tables(0).Rows(0)(0))
    End Function


    Public Function DrugRefillHeader_Add(
            ByVal RefillDate As String, ByVal PatientID As Integer, ByVal LocationID As String _
          , ByVal SEQ As Integer _
          , ProblemDesc As String, isReback As String, PCU As String, ReasonDesc As String, Remark As String, ByVal CUser As String, ReferID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "HP2P_DrugRefillHeader_Add", RefillDate, PatientID, LocationID, SEQ, ProblemDesc, isReback, PCU, ReasonDesc, Remark, CUser, ReferID)

    End Function

    Public Function DrugRefillItem_Save(
            ByVal UID As Integer _
          , ByVal RefillHeaderUID As Long _
          , ByVal MedUID As Integer _
          , ByVal QTY As Double _
          , ByVal UOM As String _
          , ByVal UsedRemark As String _
          , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "HP2P_DrugRefillItem_Save", UID, RefillHeaderUID, MedUID, QTY, UOM, UsedRemark, CUser)

    End Function

    Public Function DrugRefillItem_SaveRefill(
            ByVal RefillHeaderUID As Long _
          , ByVal MedUID As Integer _
          , ByVal QTY As Double _
          , ByVal UOM As String _
          , ByVal UsedRemark As String _
          , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DrugRefillItem_SaveRefill", RefillHeaderUID, MedUID, QTY, UOM, UsedRemark, CUser)

    End Function
    Public Function DrugRefillItem_Delete(ByVal RefillHeaderUID As Long) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DrugRefillItem_Delete", RefillHeaderUID)
    End Function

    Public Function DrugRefillItem_GetByMTMUID(MTMUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefillItem_GetByMTMUID", MTMUID)
        Return ds.Tables(0)
    End Function

#End Region

#Region "Refer"
    Public Function Refer_Get(MTMUID As Integer, PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Refer_Get", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function Refer_Save(
          ByVal MTMUID As Integer _
        , ByVal UID As Integer _
        , ByVal HospitalUID As Integer _
        , ByVal Reason As String _
        , ByVal PatientID As Integer _
        , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Refer_Save", MTMUID, UID, HospitalUID, Reason, PatientID, CUser)

    End Function

    Public Function Refer_Delete(
       ByVal MTMUID As Integer _
     , ByVal PatientID As Integer _
     , ByVal MUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Refer_Delete", MTMUID, PatientID, MUser)

    End Function

#End Region

#Region "Document Order"
    Public Function DocumentUpload_Get(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DocumentUpload_Get", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function DocumentUpload_GetByLocation(LocationID As String, PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DocumentUpload_GetByLocation", LocationID, PatientID)
        Return ds.Tables(0)
    End Function

    Public Function DocumentUpload_Save(ByVal PatientID As Integer, ByVal Desc As String, ByVal FilePath As String, ByVal Cuser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DocumentUpload_Save", PatientID, Desc, FilePath, Cuser)
    End Function
    Public Function DocumentUpload_SaveByLocation(ByVal PatientID As Integer, ByVal Desc As String, ByVal FilePath As String, ByVal Cuser As Integer, LocationID As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DocumentUpload_SaveByLocation", PatientID, Desc, FilePath, Cuser, LocationID)
    End Function

    Public Function DocumentUpload_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DocumentUpload_Delete", UID)
    End Function
#End Region


#Region "Document Order"
    Public Function DocumentOrder_Get(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DocumentOrder_Get", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function DocumentOrder_Save(ServiceUID As Integer, SEQ As Integer, itemID As Integer, itemName As String, Frequency As String, QTY As Integer, MUser As String, ServiceTypeID As String, LocationID As String, StatusFlag As String, patientid As Long) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DocumentOrder_Save", ServiceUID, SEQ, itemID, itemName, Frequency, QTY, MUser, ServiceTypeID, LocationID, StatusFlag, patientid)
    End Function

    Public Function DocumentOrder_Save(
            ByVal ReferID As Integer _
          , ByVal MedCode As String _
          , ByVal QTY As Double _
          , ByVal UsedRemark As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DocumentOrder_Save", ReferID, MedCode, QTY, UsedRemark)

    End Function

    Public Function DocumentOrder_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DocumentOrder_Delete", UID)
    End Function
#End Region

#Region "LAB"
    Public Function HP2P_LabResult_GetLastResult(RUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "HP2P_LabResult_GetLastResult", RUID)
        Return ds.Tables(0)
    End Function

    Public Function LabResult_Save(
            ByVal RefUID As Integer _
          , ByVal LabUID As Integer _
          , ByVal ResultDate As Integer _
          , ByVal PatientID As Integer _
          , ByVal ResultValue As String _
          , ByVal isSendBack As String _
          , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LabResult_Save", RefUID, LabUID, ResultDate, PatientID, ResultValue, isSendBack, CUser)

    End Function

    Public Function HP2P_LabResult_Save(
              ByVal RefUID As Integer _
            , ByVal LabUID As Integer _
            , ByVal ResultDate As Integer _
            , ByVal PatientID As Integer _
            , ByVal ResultValue As String _
            , ByVal isSendBack As String _
            , ByVal CUser As String _
            , ByVal RefillHUID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "HP2P_LabResult_Save", RefUID, LabUID, ResultDate, PatientID, ResultValue, isSendBack, CUser, RefillHUID)

    End Function

#End Region



End Class
