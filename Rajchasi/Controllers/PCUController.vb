﻿Imports Microsoft.ApplicationBlocks.Data
Public Class PCUController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function PCU_GetAll() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("PCU_GetAll"))
        Return ds.Tables(0)
    End Function

    Public Function PCU_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("PCU_GetActive"))
        Return ds.Tables(0)
    End Function
    Public Function PCU_GetActive() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("PCU_GetActive"))
        Return ds.Tables(0)
    End Function


    Public Function PCU_GetReport(pType As String, pProvince As String, ProjID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "RPT_PCUProject", pType, pProvince, ProjID)
        Return ds.Tables(0)
    End Function

    Public Function PCU_GetByID(id As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("PCU_GetByID"), id)
        Return ds.Tables(0)
    End Function

    Public Function PCU_GetHospital(lid As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("PCU_GetHospital"), lid)
        Return ds.Tables(0)
    End Function

    Public Function PCU_GetBySearchAll(provid As String, id As String) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("PCU_GetBySearchAll"), id)
        Return ds.Tables(0)
    End Function

    Public Function PCU_GetBySearch(provid As String, id As String) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("PCU_GetByProvinceID"), provid, id)
        Return ds.Tables(0)
    End Function
    Public Function PCU_Save(ByVal UID As Integer, ByVal Code As String, ByVal Name As String, ByVal TypeID As String, DivisionID As String, DepartmentID As String, Bed As Integer, ByVal Address As String, ByVal ProvinceID As String, ByVal ZipCode As String, ByVal Tel As String, ByVal Fax As String, ByVal EMail As String, ByVal AreaCode As String, ByVal StatusFlag As String, ByVal CUser As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("PCU_Save"), UID, Code, Name, TypeID, DivisionID, DepartmentID, Bed, Address, ProvinceID, ZipCode, Tel, Fax, EMail, AreaCode, StatusFlag, CUser)
    End Function

    Public Function PCU_Delete(ByVal UID As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("PCU_Delete"), UID)
    End Function
End Class

