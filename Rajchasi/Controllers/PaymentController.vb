﻿Imports Microsoft.ApplicationBlocks.Data
Public Class PaymentController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function Payment_GetAllMethod() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Payment_GetAllMethod"))
        Return ds.Tables(0)
    End Function
    Public Function Payment_MethodByForm(pForm As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Payment_MethodByForm"), pForm)
        Return ds.Tables(0)
    End Function
    Public Function Payment_GetConfigByID(PID As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Payment_GetConfigByID"), PID)
        Return ds.Tables(0)
    End Function
    Public Function Payment_GetMethodByID(PID As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Payment_GetMethodByID"), PID)
        Return ds.Tables(0)
    End Function

    Public Function Payment_GetConfig() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Payment_GetConfig"))
        Return ds.Tables(0)
    End Function
    Public Function Payment_GetConfigBySearch(PID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Payment_GetConfigBySearch"), PID)
        Return ds.Tables(0)
    End Function
    Public Function Payment_GetAmount(pProvID As String, pEffDate As Long, ProjID As Integer) As Double
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Payment_GetAmount"), pProvID, pEffDate, ProjID)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Dbl(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If

    End Function
    Public Function Payment_GetAmountByLocation(pLocationID As String, pEffDate As Long, ProjID As Integer) As Double
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Payment_GetAmountByLocation"), pLocationID, pEffDate, ProjID)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Dbl(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If

    End Function

    'Public Function Payment_ChkDupPayment(FName As String, LName As String) As Boolean
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Payment_ChkDupPaymentByName"), FName, LName)
    '    If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function
    'Public Function Payment_ChkDupPayment(CardID As String) As Boolean
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Payment_ChkDupPaymentByCardID"), CardID)
    '    If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function
    Public Function Payment_Add(ByVal ProvinceID As String _
    , ByVal PayID As Integer _
    , ByVal EffDate As Long _
    , ByVal StatusFlag As String, ByVal ProjectID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Payment_Add", ProvinceID _
       , PayID _
       , EffDate _
       , StatusFlag, ProjectID)

    End Function

    Public Function Payment_Update(ByVal ConfigID As Long, ByVal ProvinceID As String _
       , ByVal PayID As Integer _
       , ByVal EffDate As Long _
       , ByVal StatusFlag As String, ByVal ProjectID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Payment_Update", ConfigID, ProvinceID _
       , PayID _
       , EffDate _
       , StatusFlag, ProjectID)

    End Function

    Public Function Payment_Add2(ByVal PayName As String, ByVal FormID As String _
       , ByVal PayID As Integer _
       , ByVal EffDate As Long _
       , ByVal StatusFlag As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Payment_Add2", PayName, FormID _
       , PayID _
       , EffDate _
       , StatusFlag)

    End Function

    Public Function Payment_Update2(ByVal ConfigID As Long, ByVal PayName As String, ByVal FormID As String _
       , ByVal PayID As Integer _
       , ByVal EffDate As Long _
       , ByVal StatusFlag As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Payment_Update2", ConfigID, PayName, FormID _
       , PayID _
       , EffDate _
       , StatusFlag)

    End Function
    Public Function Payment_Delete(ByVal PaymentID As Long) As Boolean
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Payment_Delete", PaymentID)
    End Function
    Public Function PaymentMethod_Delete(ByVal PaymentID As Long) As Boolean
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "PaymentMethod_Delete", PaymentID)
    End Function
    Public Function Payment_GetMethod() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Payment_GetMethod"))
        Return ds.Tables(0)
    End Function
    Public Function Payment_GetMethodBySearch(PID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Payment_GetMethodBySearch"), PID)
        Return ds.Tables(0)
    End Function
End Class
