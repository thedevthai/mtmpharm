﻿Imports Microsoft.ApplicationBlocks.Data
Public Class PatientController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet
    Public Function Patient_GetByHN(HN As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Patient_GetByHN"), HN)
        Return ds.Tables(0)
    End Function
    Public Function Patient_GetPatientID(Fname As String, Lname As String) As Long
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Patient_GetPatientID"), Fname, Lname)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Lng(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function
    Public Function Patient_GetBySearch(LID As String, pKey As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Patient_GetBySearch"), LID, pKey)
        Return ds.Tables(0)
    End Function
    Public Function Patient_GetByLocation(LID As String, pKey As String, Optional isLocation As Integer = 0) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Patient_GetByLocation"), LID, pKey, isLocation)
        Return ds.Tables(0)
    End Function
    Public Function Patient_GetAllergy(PatientID As Integer, SoundexName As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("PatientAllergy_Get"), PatientID, SoundexName)
        Return ds.Tables(0)
    End Function
    Public Function PatientAllergy_Delete(UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("PatientAllergy_Delete"), UID)
    End Function

    Public Function PatientAllergy_Save(PatientID As Integer, SoundexName As String, Drug As String, Symptom As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("PatientAllergy_Save"), PatientID, SoundexName, Drug, Symptom)
    End Function

    Public Function PatientMedical_Save(
          ByVal PatientID As Integer _
        , ByVal Smoke As Integer _
        , ByVal SmokeYear As Integer _
        , ByVal SmokeCigarette As Integer _
        , ByVal CigaretteType As Integer _
        , ByVal Alcohol As Integer _
        , ByVal AlcoholFQ As Integer)

        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("PatientMedical_Save"), PatientID, Smoke, SmokeYear, SmokeCigarette, CigaretteType, Alcohol, AlcoholFQ)
    End Function
    Public Function Patient_GetMedicalHistory(PID As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Patient_GetMedicalHistory"), PID)
        Return ds.Tables(0)
    End Function
    Public Function Patient_GetByID(PID As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Patient_GetByID"), PID)
        Return ds.Tables(0)
    End Function
    Public Function Patient_GetAllByID(PID As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Patient_GetAllByID"), PID)
        Return ds.Tables(0)
    End Function
    Public Function Patient_GetBirthDate(PID As Long) As Long
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Patient_GetBirthDate"), PID)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Lng(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function
    Public Function Patient_CheckIsService(pid As Long) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Patient_CheckIsService"), pid)
        If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function Patient_ChkDupPatient(FName As String, LName As String) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Patient_ChkDupPatientByName"), FName, LName)
        If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function Patient_ChkDupPatientByIDCard(cardid As String) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Patient_ChkDupPatientByCardID"), cardid)
        If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function Patient_ChkDupPatientByHN(HN As String, HospitalUID As String) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Patient_ChkDupPatientByHN"), HN, HospitalUID)
        If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function


    Public Function Patient_ChkDupPatient(CardID As String) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Patient_ChkDupPatientByCardID"), CardID)
        If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function


    Public Function Patient_Add(ByVal Forename As String _
       , ByVal Surname As String _
       , ByVal Gender As String _
       , ByVal BirthDate As String _
       , ByVal Ages As String _
       , ByVal CardID As String _
       , ByVal Telephone As String _
       , ByVal Mobile As String _
       , ByVal TimeContact As String _
       , ByVal AddressType As String _
       , ByVal AddressNo As String _
       , ByVal Road As String _
       , ByVal District As String _
       , ByVal City As String _
       , ByVal ProvinceID As String _
       , ByVal ProvinceName As String _
       , ByVal ZipCode As String _
       , ByVal Education As String _
       , ByVal Occupation As String _
       , ByVal MainClaim As String _
       , ByVal Status As String _
       , ByVal UpdBy As String _
       , ByVal UpdDate As Long _
       , ByVal CreateBy As String _
       , ByVal Homesss As String _
       , ByVal isAllergy As String _
       , ByVal HN As String _
       , ByVal Hospital As String _
       , ByVal Smoke As Integer _
       , ByVal SmokeYear As Integer _
       , ByVal SmokeCigarette As Integer _
       , ByVal CigaretteType As Integer _
       , ByVal Alcohol As Integer _
       , ByVal AlcoholFQ As Integer _
       , ByVal MedicationUsed1 As String, ByVal MedicationUsed2 As String, ByVal MedicationUsed3 As String, ByVal MedicationUsed4 As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Patient_Add", Forename _
       , Surname _
       , Gender _
       , BirthDate _
       , Ages _
       , CardID _
       , Telephone _
       , Mobile _
       , TimeContact _
       , AddressType _
       , AddressNo _
       , Road _
       , District _
       , City _
       , ProvinceID _
       , ProvinceName _
       , ZipCode _
       , Education _
       , Occupation _
       , MainClaim _
       , Status _
       , UpdBy, UpdDate, CreateBy, Homesss, isAllergy _
       , HN _
       , Hospital _
       , Smoke, SmokeYear, SmokeCigarette, CigaretteType, Alcohol, AlcoholFQ _
       , MedicationUsed1, MedicationUsed2, MedicationUsed3, MedicationUsed4)

    End Function

    Public Function Patient_Update(ByVal PatientID As Long, ByVal Forename As String _
      , ByVal Surname As String _
      , ByVal Gender As String _
      , ByVal BirthDate As String _
      , ByVal Ages As String _
      , ByVal CardID As String _
      , ByVal Telephone As String _
      , ByVal Mobile As String _
      , ByVal TimeContact As String _
      , ByVal AddressType As String _
      , ByVal AddressNo As String _
      , ByVal Road As String _
      , ByVal District As String _
      , ByVal City As String _
      , ByVal ProvinceID As String _
      , ByVal ProvinceName As String _
      , ByVal ZipCode As String _
      , ByVal Education As String _
      , ByVal Occupation As String _
      , ByVal MainClaim As String _
      , ByVal Status As String _
      , ByVal UpdBy As String, ByVal Homesss As String, ByVal isAllergy As String _
      , ByVal HN As String _
      , ByVal Hospital As String _
      , ByVal Smoke As Integer _
      , ByVal SmokeYear As Integer _
      , ByVal SmokeCigarette As Integer _
      , ByVal CigaretteType As Integer _
      , ByVal Alcohol As Integer _
      , ByVal AlcoholFQ As Integer _
      , ByVal MedicationUsed1 As String, ByVal MedicationUsed2 As String, ByVal MedicationUsed3 As String, ByVal MedicationUsed4 As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Patient_Update", PatientID, Forename _
       , Surname _
       , Gender _
       , BirthDate _
       , Ages _
       , CardID _
       , Telephone _
       , Mobile _
       , TimeContact _
       , AddressType _
       , AddressNo _
       , Road _
       , District _
       , City _
       , ProvinceID _
       , ProvinceName _
       , ZipCode _
       , Education _
       , Occupation _
       , MainClaim _
       , Status _
       , UpdBy _
       , Homesss _
       , isAllergy _
       , HN _
       , Hospital _
       , Smoke, SmokeYear, SmokeCigarette, CigaretteType, Alcohol, AlcoholFQ, MedicationUsed1, MedicationUsed2, MedicationUsed3, MedicationUsed4)

    End Function
    Public Function Patient_UpdateHomesss(ByVal PatientID As Long, ByVal Description As String, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Patient_UpdateHomesss", PatientID, Description, UpdBy)
    End Function
    Public Function Patient_Delete(ByVal PatientID As Long) As Boolean
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Patient_Delete", PatientID)
    End Function

#Region "Drug Use"
    Public Function DrugUse_Get(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugUse_Get", PatientID)
        Return ds.Tables(0)
    End Function
    Public Function DrugUse_Get(PatientID As Integer, LocationID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugUse_GetByLocation", PatientID, LocationID)
        Return ds.Tables(0)
    End Function
    Public Function DrugUse_Get4Refill(PatientID As Integer, LocationID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugUse_Get4Refill", PatientID, LocationID)
        Return ds.Tables(0)
    End Function

    Public Function DrugUse_GetByName(SoundexName As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugUse_GetByName", SoundexName)
        Return ds.Tables(0)
    End Function


    Public Function DrugUse_Save(PatientID As Integer, DrugUID As Integer, Frequency As String, MUser As String, SoundexName As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DrugUse_Save", PatientID, DrugUID, Frequency, MUser, SoundexName)
    End Function

    Public Function DrugUse_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DrugUse_Delete", UID)
    End Function

    Public Function Patient_GetHerb(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Patient_GetHerb", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function Patient_UpdateHerb(ByVal PatientID As Integer _
           , ByVal MedicationUsed1 As String _
           , ByVal MedicationUsed2 As String _
           , ByVal MedicationUsed3 As String _
           , ByVal MedicationUsed4 As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Patient_UpdateHerb", PatientID, MedicationUsed1, MedicationUsed2, MedicationUsed3, MedicationUsed4)

    End Function

    Public Function Patient_UpdateHerbByName(ByVal SoundexName As String _
           , ByVal MedicationUsed1 As String _
           , ByVal MedicationUsed2 As String _
           , ByVal MedicationUsed3 As String _
           , ByVal MedicationUsed4 As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Patient_UpdateHerbByName", SoundexName, MedicationUsed1, MedicationUsed2, MedicationUsed3, MedicationUsed4)

    End Function


#End Region


#Region "Patient Desease"
    'Public Function PatientDesease_Add(ByVal PatientID As Integer _
    '     , ByVal ICDCode As String _
    '     , ByVal Descriptions As String _
    '     , ByVal SoundexName As String _
    '     , ByVal CreateBy As Integer) As Integer

    '    Return SqlHelper.ExecuteNonQuery(ConnectionString, "PatientDesease_Add", PatientID, ICDCode, Descriptions, SoundexName, CreateBy)

    'End Function
    Public Function PatientDesease_Add(ByVal PatientID As Integer, DeseaseName As String, ByVal Descriptions As String, ByVal SoundexName As String, ByVal CreateBy As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "PatientDesease_Add", PatientID, DeseaseName, Descriptions, SoundexName, CreateBy)

    End Function

    'Public Function PatientDesease_Add2(ByVal PatientID As Integer _
    '     , ByVal DeseaseName As String _
    '     , ByVal Descriptions As String _
    '     , ByVal SoundexName As String _
    '     , ByVal CreateBy As Integer) As Integer

    '    Return SqlHelper.ExecuteNonQuery(ConnectionString, "PatientDesease_Add2", PatientID, DeseaseName, Descriptions, SoundexName, CreateBy)

    'End Function

    Public Function PatientDesease_AddFromMTM(ByVal PatientID As Integer _
         , ByVal DeseaseName As String _
         , ByVal Descriptions As String _
         , ByVal CreateBy As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "PatientDesease_Add2", PatientID, DeseaseName, Descriptions, CreateBy)

    End Function



    Public Function PatientDesease_Get(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "PatientDesease_Get", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function PatientDesease_Get2(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "PatientDesease_Get2", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function PatientDesease_GetByName(PatientName As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "PatientDesease_GetByName", PatientName)
        Return ds.Tables(0)
    End Function

    Public Function PatientDesease_GetByName2(PatientName As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "PatientDesease_GetByName2", PatientName)
        Return ds.Tables(0)
    End Function

    Public Function PatientDesease_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "PatientDesease_Delete", UID)
    End Function

#End Region
End Class
