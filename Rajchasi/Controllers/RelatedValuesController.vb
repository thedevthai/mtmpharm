﻿Imports Microsoft.ApplicationBlocks.Data

Public Class RelatedValuesController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet
    Public Function RelatedValues_GetByCustomer(ServiceTypeID As String, SID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RelatedValues_GetByCustomer"), ServiceTypeID, SID)
        Return ds.Tables(0)
    End Function

    Public Function RelatedValues_Save(ByVal ServiceOrderID As Integer _
              , ByVal ServiceTypeID As String, ByVal RelatedValuesCode As String, ByVal RelatedValuesValue As Integer, ByVal RelatedValuesDescription As String, ByVal KNTYPEID As Integer, ByVal UpdBy As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "RelatedValues_Save", ServiceOrderID, ServiceTypeID, RelatedValuesCode, RelatedValuesValue, RelatedValuesDescription, KNTYPEID, UpdBy)

    End Function

    Public Function RelatedValues_GetByID(SID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RelatedValues_GetByID"), SID)
        Return ds.Tables(0)
    End Function

    Public Function RelatedValues_Delete(ByVal pID As Integer, tID As String) As Integer
        SQL = "delete from RelatedValues  where ServiceOrderID =" & pID & " AND ServiceTypeID='" & tID & "'"
        Return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, SQL)
    End Function

End Class
