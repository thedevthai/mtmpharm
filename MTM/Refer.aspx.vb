﻿Imports Rajchasi
Public Class Refer
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim ctlR As New ReferController
    Dim acc As New UserController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadHospitalRefer()
            txtDate.Text = DisplayShortDateTH(ctlR.GET_DATE_SERVER)
            pnRefer.Visible = False
            LoadReferData()
        End If

        cmdPrint.Attributes.Add("onclick", "javascript:void(window.open('printRefer.aspx',null,'scrollbars=1,width=650,HEIGHT=550'));")


    End Sub
    Private Sub LoadReferData()
        dt = ctlR.Refer_GetByPatient(Session("PatientID"))
        If dt.Rows.Count > 0 Then
            txtDate.Text = ConvertDateToString(dt.Rows(0)("ReferDate"))
            txtReason.Text = dt.Rows(0)("ReasonRefer")
            cboHospital.Value = DBNull2Str(dt.Rows(0)("HospitalUID"))
            cmdPrint.Visible = True
        Else
            cmdPrint.Visible = False
        End If
    End Sub

    Private Sub LoadHospitalRefer()
        Dim ctlH As New HospitalController
        dt = ctlH.Hospital_GetByStatus("A")
        cboHospital.DataSource = dt
        cboHospital.ValueField = "HospitalUID"
        cboHospital.TextField = "HospitalName"
        cboHospital.DataBind()
    End Sub
    Private Sub ClearData()
        Me.txtDate.Text = ""
        txtReason.Text = ""
        cboHospital.Text = ""
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdRefer.Click

        If txtDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','กรุณาระบุวันที่ Refer ให้ถูกต้อง');", True)
            Exit Sub
        End If

        If cboHospital.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','กรุณาระบุสถานพยาบาลที่ต้องการส่งต่อ');", True)
            Exit Sub
        End If

        If txtReason.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','กรุณาระบุเหตุผล');", True)
            Exit Sub
        End If

        If Not ctlR.Refer_CheckDup(Session("PatientID")) Then
            ctlR.Refer_Add(txtDate.Text, Session("PatientID"), cboHospital.Value, Session("LocationID"), txtReason.Text, Session("Userid"))
        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','ผู้รับบริการรายนี้ได้ทำการส่งต่อแล้ว');", True)
            Exit Sub
        End If

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','บันทึกข้อมูลการส่งต่อเรียบร้อย');", True)
        cmdPrint.Visible = True
    End Sub

    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click
        Response.Write("<script>javascript:void(window.open('Reports/printRefer.aspx?pid=" & Session("patientid") & "',null,'scrollbars=1,width=650,HEIGHT=550'));</script>")
    End Sub
End Class

