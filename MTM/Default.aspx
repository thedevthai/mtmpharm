<%@ Page Title="Home Page" Language="vb"  AutoEventWireup="false"  CodeBehind="Default.aspx.vb" Inherits=".Default2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-874" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title> Medication Therapy Management by Community Pharmacy</title>
  
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">  

  <link rel="stylesheet" type="text/css" href="css/mtmstyles.css"> 
  <link rel="stylesheet" type="text/css" href="css/uidialog.css">
  <link rel="stylesheet" type="text/css" href="css/pagestyles.css"> 
  <link rel="stylesheet" type="text/css" href="css/button.css"> 

  <link rel="stylesheet" href="components/bootstrap/dist/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">  

      <link rel="icon" href="favicon.ico" type="image/x-icon"/>
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
     
<!-- JS -->
<script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
    
<script src="js/jquery-1.10.2.js"></script> 

<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
   <script src="js/jquery-1.9.1.js"></script>
  <script src="js/jquery-ui.js"></script>
  
  <script> $(function() {
       $( "#dialog-message" ).dialog({
      autoOpen: false,
      height: 600,
      width: 600,
      modal: true,
      buttons: {
        Close: function() {
          $( this ).dialog( "close" );
        }
      },
      close: function() {
        
      }
    });
 
    $( "#user-login" )
      .button()
      .click(function() {
        $( "#dialog-message" ).dialog( "open" );
      });
  });
  </script>


</head>

<body class="hold-transition skin-blue sidebar-mini">
     <!-- cookies start -->

<div class="cookie_box" id="cookie_box"> 
	<h3>Cookie Policy</h3>
	<p>�����ء��� ���ͨѴ��â�������ǹ�ؤ��������͡�û����żŷ��շ���ش �١��ͧ�������� 㹡������ԡ�õ�ҧ���к��ͧ��� 
        <br /> �ҡ��ҹ��ҹ���䫵��������������¹�ŧ��õ�駤���� �����ҷ�ҹ�Թ�����������ء������෤��������� ������¤�֧ ��й�º���Է����ǹ�ؤ�Ţͧ��� <a href="Docs/privacy.pdf" target="_blank"> ��ҹ��������´�������</a></p>
	<button id="activeBtn">����Ѻ</button>
</div>

<!-- cookies end -->
<form id="form1" name="form1"  runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<div class="wrapper">
 <!--
  <header class="main-header">    
    <a href="#" class="logo">
    <span class="logo-mini"><b>MTM</b></span>
    <span class="logo-lg"><b>MTM</b>PHARM</span>
    </a>
    <nav class="navbar navbar-static-top">    
  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
        <div class="slk-header" >
            <span  class="header-full">Medication Therapy Management </span>
            <span  class="header-mid"> by Community Pharmacy </span>
            <span  class="header-mini"></span>
        </div>
    </nav>
  </header>
-->
  <div class="contentCPA">   
    <!-- Main content -->
<section class="content">
 <!-- Small boxes (Stat box) -->
      <!-- Main row -->
  <!--     <div class="row">
 right col (We are only adding the ID to make the widgets sortable)
        <section class="col-lg-3 connectedSortable">
-->
<div class="login-box">
  <div class="login-logo">
   <b>MTM</b>-Pharm
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">  
       <p class="login-box-msg">˹��º�ԡ�� Login</p>

      <div class="form-group has-feedback">
         <span class="glyphicon glyphicon-user form-control-feedback"></span> 
          <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control"  ToolTip="Username" type="login" Width="100%" placeholder="Username"></asp:TextBox>                
      </div>
      <div class="form-group has-feedback">  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
       <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control"   TextMode="password"  placeholder="Password" ToolTip="Password" type="login" Width="100%"></asp:TextBox>
      
      </div>
      <div class="row" align="center">
        <asp:Button ID="cmdLogin" runat="server" text="Login" CssClass="buttonLogin" Width="100px" />
      </div>   
         
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
 
 
                

          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-heart"></i>

              <h3 class="box-title">ʹѺʹع��</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
                 <table  align="center">
                        <tr>

                          <td align="center"><img src="images/sss.jpg"  height="100" border="0" /></td>
                            <td>&nbsp;</td>
                          <td align="center"><img src="images/rxlogo.png"  height="100" /></td>
                      </tr> 
                    
                      </table>

            </div>
            <div class="box-footer clearfix no-border">
           
            </div>
          </div>
      
 
   <!--
        </section> -->
        <!-- right col -->
        <!-- Left col -->
  <!--        <section class="col-lg-9 connectedSortable">     
        
 <div class="box box-danger">
            <div class="box-header">
              <i class="fa fa-users"></i>

              <h3 class="box-title"> Admin'Message : ��С�Ȩҡ�������к�</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body text-center">
            <h3 class="text-red">��С�ȻԴ�к����ͻ�Ѻ��ا<br />
         ���駻Դ�к����ͻ�Ѻ��ا Server �ѹ��� 10 �.�. 2561 (�ѹ���) ����� ���� 21.00 �. �֧ 8.00 �. (����ѹ���觹��)
           <br />������㹤�������дǡ
            </h3><br />    
         

            </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
       
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-heart"></i>

              <h3 class="box-title">ʹѺʹع��</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body text-center">
                <div class="inline"><img src="images/sss.jpg"  border="0" /></div>
                <div class="inline"><img src="images/rx.JPG"  border="0" /></div>
                 <div class="inline"><img src="images/cpalogo.png" width="160" height="100" /></div>
 
            </div>
            <div class="box-footer clearfix no-border">
           
            </div>
          </div>
  --> 
 <!-- 

          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-newspaper-o"></i>

              <h3 class="box-title">���ǻ�С��</h3>
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chat" id="chat-box">
             
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" DataKeyNames="NewsID" Font-Bold="False" ShowHeader="False" CellSpacing="4">
            <RowStyle HorizontalAlign="Center" />
            <columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Image ID="imgNews" runat="server"  Width="20px" />                    </ItemTemplate>
                    <ItemStyle Width="50px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="��Ǣ�͢���">
                    <ItemTemplate>
                        <asp:HyperLink ID="hlnkNews" runat="server" Target="_blank" Font-Size="14px">[hlnkNews]</asp:HyperLink>
                        <asp:Image ID="imgNew" runat="server" ImageUrl="images/new_icon.gif" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:BoundField HeaderText="�Ѿഷ�����">
                <ItemStyle HorizontalAlign="Right" Width="150px" CssClass="NewsUpdate" />                </asp:BoundField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle ForeColor="White" HorizontalAlign="Center" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle Font-Bold="False" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>
             
            </div>

               <div class="box-footer clearfix no-border">
                <asp:HyperLink ID="HyperLink2" class="buttonSave pull-right" runat="server" NavigateUrl="NewsAll.aspx"><i class="fa fa-plus"></i> ��ҹ���Ƿ�����</asp:HyperLink>    
            </div>
          </div>
         
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-users"></i>

              <h3 class="box-title">User Online : <asp:Label ID="lblUserOnlineCount" runat="server" Text=""></asp:Label></h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
            <asp:Label ID="lblOnline" runat="server" Width="99%"></asp:Label>
            </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>  
       
        </section>
     -->       <!-- /.Left col 

       
      </div>
         -->
      <!-- /.row (main row) -->
  
      
    </section>
    <!-- /.content -->

  <table align="center" border="0" cellpadding="0" cellspacing="2" >   
                  <tr>
                      <td>
                      <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" 
                              BackgroundCssClass="dialog_bg" Drag="True" DropShadow="false" 
                              PopupControlID="Panel1" PopupDragHandleControlID="headbox" 
                              TargetControlID="Panel1">                          </cc1:ModalPopupExtender>
                          <asp:Panel ID="Panel1" runat="server" DefaultButton="btnOk" 
                              Style="display: none;">

                                <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-expeditedssl"></i>

              <h3 class="box-title">�š�÷ӧҹ</h3>
                 <div class="box-tools pull-right">               
              </div>
            </div>
            <div class="box-body chat" id="alert-box">
                  <asp:Label ID="lblAlert" runat="server" CssClass="text-maroon">Username  ���� Password ���١��ͧ</asp:Label>                                                 
              </div>
            <!-- /.chat -->
               <div class="box-footer clearfix no-border" align="center"> 
                   <asp:Button ID="btnOK" runat="server" CssClass="buttonSave" Text="Close" Width="100px" />  
            </div>
          </div>            
                  </asp:Panel>

<script type="text/javascript">

    function fnClickOK(sender, e) {
        __doPostBack(sender, e);
    }
                </script></td>
                </tr>               
               
              </table>    

</div>
  <!-- /.content-wrapper -->
  <footer class="main-footerCPA">
       <div class="pull-right hidden-xs">
         <b>Version</b> <asp:Label ID="lblVersion" runat="server" Text="1.0.0"></asp:Label>
    </div>
       

      <strong>�Ԣ�Է��� &copy; �� ��.�筷Ծ� ���ࡵطͧ ��� ��.��.�س� �����Թ�ش�</strong> 
    
  </footer>
  
</div>
<!-- ./wrapper --> 



</form>
    
<script src="components/jquery/dist/jquery.min.js"></script>
<script src="components/jquery-ui/jquery-ui.min.js"></script>
<script src="components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="dist/js/adminlte.min.js"></script>
  <script>
      var cookie_box = document.getElementById('cookie_box');
      var activeBtn = document.getElementById('activeBtn');

      activeBtn.addEventListener('click', function () {
          document.cookie = "CookieBy=mtmpharm; expires=" + 60 * 60 * 24 * 30;

          if (document.cookie) {
              //Hide the popup box
              cookie_box.classList.add('hide');
          } else {
              //If we block cookie setting then show this massege
              alert("cookie not set! Please allow this site from your browser cookie setting");
          }

      })

      function getCookieName(name) {
          var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
          return r ? r[1] : '';
      }
      var getCookieName = getCookieName('CookieBy');
      //alert(getCookieName)
      if (getCookieName === 'mtmpharm') {
          //All time hide the popup box
          cookie_box.classList.add('hide');
      }
  </script>

</body>
</html>
