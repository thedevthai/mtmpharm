﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteNoAjax.Master" CodeBehind="FormList.aspx.vb" Inherits=".FormList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
     
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>รายการกิจกรรม
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">รายการกิจกรรม MTM</li>
      </ol>
    </section>

<section class="content">  


       <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search"></i>

              <h3 class="box-title">ค้นหา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">  
                <table border="0" align="center" cellPadding="1" cellSpacing="1">
<tr>
  <td align="left" class="texttopic">ปี :</td>
  <td align="left" class="texttopic">
                                                      <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" 
                                                          CssClass="OptionControl">
                                                          <asp:ListItem Selected="True" Value="0">-- ทั้งหมด --</asp:ListItem>  
                                                          <asp:ListItem>2561</asp:ListItem>
                                                          <asp:ListItem>2562</asp:ListItem>
                                                          <asp:ListItem>2563</asp:ListItem>
                                                          <asp:ListItem>2564</asp:ListItem>
                                                      </asp:DropDownList>                                                    </td>
</tr>
<tr>
  <td align="left" class="texttopic"><asp:Label ID="lblProv" runat="server" 
          Text="จังหวัด :"></asp:Label>
    </td>
  <td align="left" class="texttopic">
                                                      <asp:DropDownList ID="ddlProvinceID" runat="server" AutoPostBack="True" 
                                                          CssClass="OptionControl">                                                      </asp:DropDownList>                                                    </td>
</tr>

<tr>
  <td align="left" class="texttopic">ค้นหา :</td>
  <td align="left" class="texttopic">
                  <asp:TextBox ID="txtSearch" runat="server" Width="150px"></asp:TextBox>                                                    </td>
</tr>

<tr>
  <td align="left" class="texttopic">&nbsp;</td>
  <td align="left" class="texttopic">
                                                      <asp:Button ID="cmdSearch" runat="server" CssClass="buttonLogin" Text="ค้นหา" Width="100px" />
    </td>
</tr>

  </table>  
 </div>
          
          </div>
     <h3>รายการกิจกรรมที่พบทั้งหมด&nbsp;<asp:Label ID="lblCount" runat="server"></asp:Label>
           &nbsp;&nbsp;รายการ</h3> 
    <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-edit"></i>

              <h3 class="box-title">1.ร้านยาบันทึกกิจกรรม ( <asp:Label 
                  ID="lblCountForm" runat="server"></asp:Label>
              &nbsp;)</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">


              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="4" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" >
            <RowStyle BackColor="#F7F7F7" />
            <columns>
                <asp:BoundField HeaderText="Seq No." DataField="ItemID" >
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" Width="70px" />                </asp:BoundField>
                <asp:BoundField HeaderText="วันที่บริการ" DataField="ServiceDateTXT" >
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" Width="90px" />                </asp:BoundField>
                <asp:TemplateField HeaderText="ร้านยา">
                 <ItemTemplate>
                        <asp:HyperLink ID="Hyper1" runat="server" 
                            NavigateUrl='<%# NavigateURL("FormDirect.aspx", "acttype", "view", "ServiceTypeID", DataBinder.Eval(Container.DataItem, "ServiceTypeID"), "ItemId", DataBinder.Eval(Container.DataItem, "ItemID"), "ProjID", "3")%>' 
                            Target="_blank" 
                            Text='<%# DataBinder.Eval(Container.DataItem, "LocationName") %>' 
                            CssClass="grd_item"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle Width="200px" CssClass="grd_item" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ชื่อ-สกุล">
                 <ItemTemplate>
                        <asp:HyperLink ID="Hyper2" runat="server" 
                            NavigateUrl='<%# NavigateURL("FormDirect.aspx", "acttype", "view", "ServiceTypeID", DataBinder.Eval(Container.DataItem, "ServiceTypeID"), "ItemId", DataBinder.Eval(Container.DataItem, "ItemID"), "ProjID", "3")%>' 
                            Target="_blank" 
                            Text='<%# DataBinder.Eval(Container.DataItem, "CustName") %>' 
                            CssClass="grd_item"></asp:HyperLink>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle Width="200px" HorizontalAlign="Left" CssClass="grd_item" />
                </asp:TemplateField>
                <asp:BoundField DataField="Ages" HeaderText="อายุ" >
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" Width="30px" />                </asp:BoundField>
            <asp:BoundField HeaderText="เพศ" DataField="Sexx">                      
                <HeaderStyle HorizontalAlign="Center" />
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />                      </asp:BoundField>
                <asp:BoundField DataField="MTMTYPETXT" HeaderText="ฟอร์ม" >
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />                </asp:BoundField>
                <asp:BoundField DataField="SEQ" HeaderText="ครั้งที่" >
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
            <asp:TemplateField  HeaderText="" >
                <ItemTemplate>
                     
                    <asp:LinkButton ID="lnkAdd1" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|3" & "|" & DataBinder.Eval(Container.DataItem, "ServiceTypeID") %>' CssClass="buttonPlus" Width="25px">+</asp:LinkButton>
                </ItemTemplate>
              <itemstyle HorizontalAlign="center" VerticalAlign="Middle"  Width="40px"/>          
            </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|3" & "|" & DataBinder.Eval(Container.DataItem, "ServiceTypeID") %>' 
                                        ImageUrl="images/icon-edit.png" />
                                 
                                     <asp:ImageButton ID="imgDel"  cssclass="gridbutton"  runat="server" 
                                       CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|3" & "|" & DataBinder.Eval(Container.DataItem, "ServiceTypeID") %>'
                                        ImageUrl="images/delete.png" />                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>


 </div>
            <div class="box-footer clearfix">
           
              <asp:Label ID="lblNo" runat="server" CssClass="text_red" 
                  
                  Text="ยังไม่มีรายการกิจกรรม"></asp:Label>           
           
            </div>
          </div>
      <asp:Panel ID="Panel2" runat="server"> 
  <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-dollar"></i>

              <h3 class="box-title">จ่ายเงินแล้ว ( <asp:Label            ID="lblPayCount" runat="server"></asp:Label> &nbsp;) </h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
               

                 
            </div>
            <div class="box-body">          

   
  
 
              <asp:GridView ID="grdDataPaymented" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True">
            <RowStyle BackColor="#F7F7F7" />
            <columns>
                <asp:BoundField HeaderText="Seq No." DataField="itemID" >
                <ItemStyle HorizontalAlign="Center" Width="70px" />                </asp:BoundField>
                <asp:BoundField DataField="ServiceDateTXT" HeaderText="วันที่บริการ">
                <ItemStyle Width="80px" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="ร้านยา">
                 <ItemTemplate>
                        <asp:HyperLink ID="Hyper5" runat="server" 
                            NavigateUrl='<%# NavigateURL("FormDirect.aspx", "acttype", "view", "ServiceTypeID", DataBinder.Eval(Container.DataItem, "ServiceTypeID"), "ItemId", DataBinder.Eval(Container.DataItem, "ItemID"), "ProjID", "3")%>' 
                            Target="_blank" 
                            Text='<%# DataBinder.Eval(Container.DataItem,"LocationName") %>' 
                            CssClass="grd_item"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle Width="200px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ชื่อ-สกุล">
                 <ItemTemplate>
                        <asp:HyperLink ID="Hyper6" runat="server" 
                            NavigateUrl='<%# NavigateURL("FormDirect.aspx", "acttype", "view", "ServiceTypeID", DataBinder.Eval(Container.DataItem, "ServiceTypeID"), "ItemId", DataBinder.Eval(Container.DataItem, "ItemID"), "ProjID", "3")%>' 
                            Target="_blank" 
                            Text='<%# DataBinder.Eval(Container.DataItem,"CustName") %>' 
                            CssClass="grd_item"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle Width="200px" HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:BoundField DataField="Ages" HeaderText="อายุ" >
                <ItemStyle HorizontalAlign="Center" Width="30px" />                </asp:BoundField>
            <asp:BoundField HeaderText="เพศ" DataField="Sexx">                      
                <HeaderStyle HorizontalAlign="Center" />
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />                      </asp:BoundField>
                <asp:BoundField DataField="MTMTYPETXT" HeaderText="ฟอร์ม" >
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" />                </asp:BoundField>
                <asp:BoundField DataField="SEQ" HeaderText="ครั้งที่" >
                 <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center" Width="50px" />
                </asp:BoundField>
                 <asp:TemplateField  HeaderText="" >
                <ItemTemplate>
                     
                    <asp:LinkButton ID="lnkAdd2" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|3" & "|" & DataBinder.Eval(Container.DataItem, "ServiceTypeID") %>' CssClass="buttonPlus" Width="25px">+</asp:LinkButton>
                </ItemTemplate>
              <itemstyle HorizontalAlign="center" VerticalAlign="Middle"  Width="40px" />          
            </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit2" runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|3" & "|" & DataBinder.Eval(Container.DataItem, "ServiceTypeID") %>' 
                                        ImageUrl="images/icon-edit.png" />
                                    <asp:ImageButton ID="imgDel2" runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|3" & "|" & DataBinder.Eval(Container.DataItem, "ServiceTypeID") %>' 
                                        ImageUrl="images/delete.png" />                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                </asp:TemplateField>
                <asp:BoundField DataField="InvoiceNo" HeaderText="Invoice No." />
                <asp:BoundField DataField="PayDateTXT" HeaderText="Pay Date" />
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>
                  
 

                 </div>
            <div class="box-footer clearfix">
            <asp:Label ID="lblNo2" runat="server" CssClass="text_red" 
                  
                  Text="ยังไม่มีรายการกิจกรรม"></asp:Label> 
            </div>
          </div>

     </asp:Panel>

    </section>
</asp:Content>
