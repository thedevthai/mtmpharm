﻿Imports System.IO
Imports Rajchasi
Public Class SitePatient
    Inherits System.Web.UI.MasterPage
    Dim dt As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not Request("PatientID") Is Nothing Then
            Session("patientid") = Request("PatientID")
        End If
        If Session("patientid") Is Nothing Then
            Response.Redirect("PatientSearch.aspx")
        End If
        If Not IsPostBack Then
            Dim ctlS As New SystemConfigController
            lblVersion.Text = ctlS.SystemConfig_GetByCode("Version")

            'Dim ctlStd As New StudentController
            'Dim ctlPsn As New PersonController

            'Dim picPaths As String


            'If Session("UserProfileID") = 1 Then
            '    dt = ctlStd.GetStudent_ByID(Session("ProfileID"))
            '    picPaths = stdPic
            'Else
            '    dt = ctlPsn.GetPerson_ByID(Session("ProfileID"))
            '    picPaths = personPic
            'End If

            'If dt.Rows.Count > 0 Then
            '    If DBNull2Str(dt.Rows(0).Item("PicturePath")) <> "" Then

            '        Dim objfile As FileInfo = New FileInfo(Server.MapPath("~/" & picPaths & "/" & dt.Rows(0).Item("PicturePath")))


            '    End If


            'End If

            'dt = Nothing
        End If
        hlnkUserName.Text = Session("NameOfUser")
        If Session("RoleID") = isShopAccess Then
            hlnkUserName.NavigateUrl = "LocationsEdit.aspx?id=" & Session("LocationID")
        Else
            hlnkUserName.NavigateUrl = "#"
        End If

    End Sub
End Class