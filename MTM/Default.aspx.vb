﻿Imports Rajchasi
Imports System.Web
Public Class Default2
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim acc As New UserController
    Dim enc As New CryptographyEngine
    Dim ctlNews As New NewsController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Response.Redirect("index.html")
        Try

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "reconn key", KeepAlive())

            If Not Request("logout") Is Nothing Then
                If Request("logout") = "YES" Or Request("logout").ToLower() = "y" Then
                    acc.User_GenLogfile(Session("Username"), ACTTYPE_LOGOUT, "Users", Session("Username"), "")
                    Session.Contents.RemoveAll()
                    Session.Abandon()
                    Session("username") = Nothing
                Else
                    RenewSessionFromCookie()
                End If
            Else
                RenewSessionFromCookie()
            End If
            Session.Timeout = 60

            If Not IsPostBack Then
                Dim ctlS As New SystemConfigController
                lblVersion.Text = ctlS.SystemConfig_GetByCode("Version")
                'LoadNewsToGrid()
                'LoadUserOnline()
                txtUserName.Focus()
            End If

            btnOK.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnOK.UniqueID, "")
        Catch eX As Exception

        End Try


        Dim jsString As String = "if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {document.forms[0].all['" + cmdLogin.ClientID + "'].click();return false;} else return true; "
        txtPassword.Attributes.Add("onkeydown", jsString)


    End Sub

    Private Sub RenewSessionFromCookie()
        If Request.Cookies("userid") Is Nothing Then
            Exit Sub
        End If

        dt = acc.User_GetByUserID(StrNull2Zero(Request.Cookies("userid").Value))

        If dt.Rows.Count > 0 Then
            Session("UserID") = dt.Rows(0).Item("UserID")
            Session("Username") = dt.Rows(0).Item("USERNAME")
            Session("LastLog") = String.Concat(dt.Rows(0).Item("LastLogin"))
            Session("NameOfUser") = String.Concat(dt.Rows(0).Item("FirstName")) & " " & String.Concat(dt.Rows(0).Item("LastName"))
            Session("LocationID") = String.Concat(dt.Rows(0).Item("LocationID"))
            Session("RoleID") = String.Concat(dt.Rows(0).Item("RoleID"))
            Session("RPTGRP") = String.Concat(dt.Rows(0).Item("ReportGroup"))
            Session("PRJMNG") = DBNull2Zero(dt.Rows(0).Item("ProjectManager"))

            Dim dtP As New DataTable
            dtP = acc.User_GetProjectRole(Session("UserID"))
            If dtP.Rows.Count > 0 Then
                Session("ProjectF") = Decimal2Boolean(dtP.Rows(0)("ProjectF"))
                Session("ProjectA") = Decimal2Boolean(dtP.Rows(0)("ProjectA"))

            End If

            Response.Redirect("Home.aspx")
            Exit Sub
        End If

    End Sub



    Private Sub LoadNewsToGrid()

        dt = ctlNews.News_GetFirstPage

        With grdData
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkN As HyperLink = .Rows(i).Cells(1).FindControl("hlnkNews")
                hlnkN.Text = dt.Rows(i)("Title")

                Select Case dt.Rows(i)("NewsType")
                    Case "URL"
                        img1.ImageUrl = "images/www.png"
                        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                    Case "UPL"
                        img1.ImageUrl = "images/pdf.png"
                        hlnkN.NavigateUrl = tmpUpload & "/" & String.Concat(dt.Rows(i)("LinkPath"))
                    Case "CON"
                        img1.ImageUrl = "images/comms.png"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    Case Else
                        img1.ImageUrl = "images/comms.png"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                End Select

                Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                img2.Visible = False
                If DateDiff(DateInterval.Day, CDate(dt.Rows(i)("NewsDate")), Today.Date) <= 5 Then
                    img2.Visible = True
                End If

                .Rows(i).Cells(2).Text = "<span class='Newsdate'>" & DisplayDateTH(dt.Rows(i)("NewsDate")) & "</span>"

                'Select Case i Mod 4
                '    Case 0
                '        .Rows(i).Cells(2).Text = "<span class='label label-success'>" & DisplayDateTH(dt.Rows(i)("NewsDate")) & "</span>"
                '    Case 1
                '        .Rows(i).Cells(2).Text = "<span class='label label-info'>" & DisplayDateTH(dt.Rows(i)("NewsDate")) & "</span>"
                '    Case 2
                '        .Rows(i).Cells(2).Text = "<span class='label label-danger'>" & DisplayDateTH(dt.Rows(i)("NewsDate")) & "</span>"
                '    Case 3
                '        .Rows(i).Cells(2).Text = "<span class='label label-warning'>" & DisplayDateTH(dt.Rows(i)("NewsDate")) & "</span>"
                '    Case Else
                '        .Rows(i).Cells(2).Text = "<span class='label label-success'>" & DisplayDateTH(dt.Rows(i)("NewsDate")) & "</span>"
                'End Select




            Next

        End With

    End Sub


    Private Sub CheckUser()
        dt = acc.User_CheckLogin(txtUserName.Text, enc.EncryptString(txtPassword.Text, True))

        If dt.Rows.Count > 0 Then

            If dt.Rows(0).Item("isPublic") = 0 Then

                lblAlert.Text = "ID ของท่านไม่สามารถใช้งานได้ชั่วคราว"
                ModalPopupExtender1.Show()

                ' DisplayMessage(Me.Page, "ID ของท่านไม่สามารถใช้งานได้ชั่วคราว")
                Exit Sub
            End If



            Dim ckUserID As New HttpCookie("userid")
            Dim ckUsername As New HttpCookie("username")

            ckUserID.Value = dt.Rows(0).Item("userid")
            ckUsername.Value = dt.Rows(0).Item("username")

            Response.Cookies.Add(ckUserID)
            Response.Cookies.Add(ckUsername)


            Session("UserID") = dt.Rows(0).Item("UserID")
            Session("Username") = dt.Rows(0).Item("USERNAME")
            Session("Password") = enc.DecryptString(dt.Rows(0).Item("PASSWORD"), True)
            'Session("UserRole") = dt.Rows(0).Item("USERROLE")
            Session("LastLog") = String.Concat(dt.Rows(0).Item("LastLogin"))
            Session("NameOfUser") = String.Concat(dt.Rows(0).Item("FirstName")) & " " & String.Concat(dt.Rows(0).Item("LastName"))
            Session("LocationID") = String.Concat(dt.Rows(0).Item("LocationID"))
            Session("RoleID") = String.Concat(dt.Rows(0).Item("RoleID"))
            Session("RPTGRP") = String.Concat(dt.Rows(0).Item("ReportGroup"))
            Session("PRJMNG") = DBNull2Zero(dt.Rows(0).Item("ProjectManager"))

            Dim dtP As New DataTable
            dtP = acc.User_GetProjectRole(Session("UserID"))
            If dtP.Rows.Count > 0 Then
                Session("ProjectF") = Decimal2Boolean(dtP.Rows(0)("ProjectF"))
                Session("ProjectA") = Decimal2Boolean(dtP.Rows(0)("ProjectA"))
            End If


            genLastLog()

            acc.User_GenLogfile(txtUserName.Text, ACTTYPE_LOG, "Users", Session("Username"), "MTM")

            Response.Redirect("Home.aspx")

        Else

            lblAlert.Text = "Username  หรือ Password ไม่ถูกต้อง"
            ModalPopupExtender1.Show()


            ' DisplayMessage(Me.Page, "Username  หรือ Password ไม่ถูกต้อง")
            Exit Sub
        End If
    End Sub

    Private Sub genLastLog() ' เก็บวันเวลาที่ User เข้าใช้ระบบครั้งล่าสุด
        acc.User_LastLog_Update(txtUserName.Text)
    End Sub

    Protected Sub cmdLogin_Click(sender As Object, e As EventArgs) Handles cmdLogin.Click

        'txtUserName.Text = "admin"
        'txtPassword.Text = "112233"

        If txtUserName.Text = "" Or txtPassword.Text = "" Then

            lblAlert.Text = "กรุณาป้อน Username  หรือ Password ให้ครบถ้วนก่อน"
            ModalPopupExtender1.Show()
            ' DisplayMessage(Me.Page, "กรุณาป้อน Username  หรือ Password ให้ครบถ้วนก่อน ")
            txtUserName.Focus()
            Exit Sub
        End If

        Session.Contents.RemoveAll()

        CheckUser()

    End Sub

    Private Sub grdData_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Private Sub LoadUserOnline()

        lblUserOnlineCount.Text = Application("OnlineNow")

        dt = acc.GetUsers_Online("MTM")
        lblOnline.Text = "ผู้ใช้งานล่าสุด : "
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1

                Select Case i Mod 4
                    Case 0
                        lblOnline.Text &= " <span class='label label-success'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & "</span>"
                    Case 1
                        lblOnline.Text &= " <span class='label label-info'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & "</span>"
                    Case 2
                        lblOnline.Text &= " <span class='label label-danger'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & "</span>"
                    Case 3
                        lblOnline.Text &= " <span class='label label-warning'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & "</span>"
                    Case Else
                        lblOnline.Text &= " <span class='label label-success'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & "</span>"
                End Select
                'If i Mod 2 = 0 Then
                '    lblOnline.Text &= " <span class='label label-success'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("Name") & "</span>"
                '    'lblOnline.Text &= "<font color=#1878ca>" & "[" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("Name") & "]" & "</font>"
                'Else
                '    lblOnline.Text &= " <span class='label label-warning'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("Name") & "</span>"
                'End If

            Next

        Else
            lblOnline.Text = "ไม่มี User Online ในขณะนี้"
        End If

    End Sub


End Class
