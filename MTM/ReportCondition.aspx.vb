﻿Imports Rajchasi
Public Class ReportCondition
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlType As New ServiceTypeController
    Dim objLct As New LocationInfo
    Dim ctlLct As New LocationController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim m, y As Integer
            y = Year(Date.Now)
            m = Month(Date.Now)
            If y < 2500 Then
                y = y + 543
            End If

            txtStartDate.Text = "01/" & m & "/" & y 'Date.Now.AddDays(-30).ToString("dd/MM/yyyy", DateFormat_TH)
            txtEndDate.Text = Date.DaysInMonth(y, m) & "/" & m & "/" & y  'Date.Now.ToString("dd/MM/yyyy", DateFormat_TH)

            'txtStartDate.Text = Date.Now.AddDays(-30).ToString("dd/MM/yyyy", DateFormat_EN)
            'txtEndDate.Text = Date.Now.ToString("dd/MM/yyyy", DateFormat_EN)

            Select Case Request("ItemType")
                Case "fnc"
                    lblReportHeader.Text = "รายงานสรุปจำนวนผู้รับบริการ"
                    ReportsName = "MTMFinanceServiceCountByLocation"
                    cmdExcel.Visible = True
                Case "cus"
                    ReportsName = "MTMCustomerServiceCountByLocation"
                    lblReportHeader.Text = "รายงานรายชื่อผู้เข้ารับบริการแยกตามกิจกรรม"
                    cmdExcel.Visible = True
            End Select
        End If

    End Sub
    Protected Sub cmdExcel_Click(sender As Object, e As EventArgs) Handles cmdExcel.Click

        Dim dStart As String = ConvertStrDate2DBString(txtStartDate.Text)
        Dim dEnd As String = ConvertStrDate2DBString(txtStartDate.Text)
        Reportskey = "XLS"
        Dim rptGRP As String = "ALL"
        Dim LID As String = "0"
        If Session("RoleID") = isShopAccess Then
            LID = Session("LocationID")
        End If
        FagRPT = Request("ItemType")
        Response.Redirect("ReportViewer.aspx?rpt=" & Request("ItemType") & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)

    End Sub
End Class