﻿Imports Rajchasi
Public Class PatientSearch
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim objUser As New UserController
    Dim ctlP As New PatientController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            pnNew.Visible = False
            grdData.PageIndex = 0
            LoadPatientListToGrid()

        End If
    End Sub
    Private Sub LoadPatientListToGrid()

        If Session("RoleID") = isShopAccess Then
            dt = ctlP.Patient_GetBySearch(Session("LocationID"), Trim(txtSearch.Text))
        ElseIf Session("RoleID") = isReportViewerAccess Then
            dt = ctlP.Patient_GetBySearch(Session("RPTGRP"), Trim(txtSearch.Text))
        Else
            dt = ctlP.Patient_GetBySearch("", Trim(txtSearch.Text))
        End If

        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                Try
                    For i = 0 To .PageSize - 1

                        Dim btnD As ImageButton = .Rows(i).Cells(6).FindControl("imgDel")

                        If Session("RoleID") = isAdminAccess Then
                            btnD.Visible = True
                        Else
                            btnD.Visible = False
                        End If

                    Next

                Catch ex As Exception

                End Try
                pnNew.Visible = False
            End With
        Else
            grdData.Visible = False
            grdData.DataSource = Nothing
            pnNew.Visible = True
        End If

        dt = Nothing
    End Sub

    Protected Function DateText(ByVal input As String) As String
        Dim dStr As String
        dStr = Replace(DisplayStr2ShortDateTH(input), "/", "-")
        Return dStr
    End Function
    Protected Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        grdData.PageIndex = 1
        LoadPatientListToGrid()
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadPatientListToGrid()
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdData.RowCommand
        Session("patientid") = e.CommandArgument
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgNew"
                    Response.Redirect("MTM.aspx?ActionType=mtm&PatientID=" & e.CommandArgument)
                Case "imgPt"
                    Response.Redirect("EMR.aspx?ActionType=emr&PatientID=" & e.CommandArgument)
                Case "imgEdit"
                    Response.Redirect("Patient.aspx?ActionType=pt&PatientID=" & e.CommandArgument)
                Case "imgDel"
                    If ctlP.Patient_Delete(e.CommandArgument) Then

                        objUser.User_GenLogfile(Session("Username"), ACTTYPE_DEL, "Patient", "ลบ Patient:" & e.CommandArgument, "")

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบข้อมูลเรียบร้อย');", True)
                        LoadPatientListToGrid()
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If


            End Select


        End If

        'If TypeOf e.CommandSource Is WebControls.HyperLink Then
        '    Dim ButtonPressed As WebControls.HyperLink = e.CommandSource
        '    Select Case ButtonPressed.ID
        '        Case "lnkPatientID", "lnkCardID", "lnkName"
        '            Session("patientid") = e.CommandArgument

        '            'Case "lnkEdu2"
        '            '    Response.Redirect("F03.aspx?t=new&seq=2&fid=" & DBNull2Zero(e.CommandArgument))
        '            'Case "lnkFollow"
        '            '    Response.Redirect("F11_Follow.aspx?t=new&fid=" & DBNull2Zero(e.CommandArgument))
        '    End Select
        'End If
    End Sub

    Protected Sub cmdAddNew_Click(sender As Object, e As EventArgs) Handles cmdAddNew.Click
        Response.Redirect("PatientRegister.aspx")
    End Sub

    Protected Sub grdData_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub
End Class