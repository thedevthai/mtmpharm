﻿Imports Rajchasi
Public Class Hospital
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim acc As New UserController
    Dim ctlP As New ApplicationBaseClass

    Dim ctlH As New HospitalController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            grdData.PageIndex = 0
            lblUID.Text = ""
            LoadProvince()
            LoadHospital()
        End If

    End Sub
    Private Sub LoadProvince()
        dt = ctlP.LoadProvince
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadHospital()
        If Trim(txtSearch.Text) <> "" Then
            dt = ctlH.Hospital_GetBySearch(txtSearch.Text)
        Else
            dt = ctlH.Hospital_Get
        End If

        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With
        Else
            grdData.Visible = False
        End If
        dt = Nothing
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadHospital()
    End Sub
    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    ctlH.Hospital_Delete(e.CommandArgument)
                    acc.User_GenLogfile(Session("Username"), ACTTYPE_DEL, "Hospital", "Delete Hospital:" & lblUID.Text & ">>" & txtName.Text, "")
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบข้อมูลเรียบร้อย');", True)
                    LoadHospital()
            End Select


        End If
    End Sub

    Private Sub EditData(ByVal pID As String)
        dt = ctlH.Hospital_GetByUID(pID)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                Me.lblUID.Text = DBNull2Str(dt.Rows(0)("HospitalUID"))
                txtCode.Text = String.Concat(dt.Rows(0)("Code"))
                txtName.Text = DBNull2Str(dt.Rows(0)("HospitalName"))
                ddlProvince.SelectedValue = DBNull2Str(dt.Rows(0)("ProvinceID"))
                chkActive.Checked = ConvertStatusFlag2Boolean(DBNull2Str(dt.Rows(0)("StatusFlag")))
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub ClearData()
        Me.lblUID.Text = ""
        txtCode.Text = ""
        txtName.Text = ""
        ddlProvince.SelectedIndex = 0
    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(5).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub


    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtName.Text = "" Then

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','กรุณากรอกข้อมูลให้ครบถ้วน');", True)
            Exit Sub
        End If


        If lblUID.Text = "" Then

            ctlH.Hospital_Add(txtCode.Text, txtName.Text, ddlProvince.SelectedValue.ToString(), Boolean2StatusFlag(chkActive.Checked), Session("userid"))

            acc.User_GenLogfile(Session("Username"), ACTTYPE_ADD, "Hospital", "Add new Hospital:" & txtName.Text, "")

        Else
            ctlH.Hospital_Update(StrNull2Zero(lblUID.Text), txtCode.Text, txtName.Text, ddlProvince.SelectedValue.ToString(), Boolean2StatusFlag(chkActive.Checked), Session("userid"))

            acc.User_GenLogfile(Session("Username"), ACTTYPE_UPD, "Hospital", "Update Hospital:" & txtName.Text, "")

        End If

        LoadHospital()
        ClearData()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','บันทึกข้อมูลเรียบร้อย');", True)

    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadHospital()
    End Sub
End Class

