﻿Imports Rajchasi
Public Class ReportConditionByProvince
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlType As New ServiceTypeController
    Dim objLct As New LocationInfo
    Dim ctlLct As New LocationController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then

            LoadProvinceToDDL()

            Dim m, y As Integer
            y = Year(Date.Now)
            m = Month(Date.Now)
            If y < 2300 Then
                y = y + 543
            End If

            txtStartDate.Text = "01/" & m & "/" & y 'Date.Now.AddDays(-30).ToString("dd/MM/yyyy", DateFormat_TH)
            txtEndDate.Text = Date.DaysInMonth(y, m) & "/" & m & "/" & y  'Date.Now.ToString("dd/MM/yyyy", DateFormat_TH)

            Select Case Request("ItemType")
                Case "fncp"
                    lblReportHeader.Text = "รายงานสรุปจำนวนกิจกรรม"

                    lblLocation.Visible = False
                    ddlLocation.Visible = False
                Case "cusp"
                    lblLocation.Visible = True
                    ddlLocation.Visible = True
                    LoadLocationToDDL()
                    lblReportHeader.Text = "รายงานรายชื่อผู้เข้ารับบริการแยกตามกิจกรรม"

                Case "mtm"
                    lblReportHeader.Text = "รายงาน MTM "
                    ReportsName = "MTMFinanceServiceCount"

                    lblLocation.Visible = False
                    ddlLocation.Visible = False
                    ddlProvince.Visible = False
                    lblProv.Visible = False
                    FagRPT = "MTM"
                Case "mcount"
                    lblReportHeader.Text = "รายงานสรุปจำนวนการบริการ MTM "
                    ReportsName = "MTMCustomerServiceCount"

                    ddlProvince.Visible = False
                    lblProv.Visible = False
                    lblLocation.Visible = False
                    ddlLocation.Visible = False
                    FagRPT = "MTM"
                Case "pcount"
                    lblReportHeader.Text = "รายงานสรุปปัญหา MTM  รายบุคคล"
                    ReportsName = "MTMCustomerProblemMatrix"

                    ddlProvince.Visible = False
                    lblProv.Visible = False
                    lblLocation.Visible = False
                    ddlLocation.Visible = False
                    FagRPT = "MTM"
                Case "mtmcount", "hvcount"
                    lblReportHeader.Text = "รายงานสรุปจำนวนการบริการ MTM "
                    ReportsName = "MTMCustomerServiceCountByType"

                    ddlProvince.Visible = False
                    lblProv.Visible = False
                    lblLocation.Visible = False
                    ddlLocation.Visible = False
                    FagRPT = "HV"
                Case "dr"
                    lblReportHeader.Text = "รายงานยาเหลือใช้ "
                    ReportsName = "DrugRemain"

                    lblLocation.Visible = False
                    ddlLocation.Visible = False
                    ddlProvince.Visible = True
                    lblProv.Visible = True
                    FagRPT = "DR"
            End Select

            If Session("RoleID") = isShopAccess Then
                lblLocation.Visible = False
                ddlLocation.Visible = False
            End If

        End If
    End Sub
    Private Sub LoadProvinceToDDL()
        If Session("RoleID") = isReportViewerAccess Then
            dt = ctlLct.Province_GetInGroup(Session("RPTGRP"))
        Else
            dt = ctlLct.Province_GetInLocation
        End If

        ddlProvince.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                If Session("RoleID") = isReportViewerAccess Then
                    .Items(0).Value = Session("RPTGRP")
                Else
                    .Items(0).Value = "0"
                End If

                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("ProvinceName"))
                    .Items(i + 1).Value = dt.Rows(i)("ProvinceID")
                Next
                .SelectedIndex = 0

            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadLocationToDDL()
        If Session("RoleID") = isReportViewerAccess Then
            If ddlProvince.SelectedValue <> Session("RPTGRP") Then
                dt = ctlLct.Location_GetByProvince(ddlProvince.SelectedValue)
            Else
                dt = ctlLct.Location_GetByProvinceGroup(Session("RPTGRP"))
            End If
        Else
            dt = ctlLct.Location_GetByProvince(ddlProvince.SelectedValue)
        End If

        ddlLocation.Items.Clear()

        If dt.Rows.Count > 0 Then
            'ddlLocation.DataSource = dt
            'ddlLocation.ValueField = "HospitalUID"
            'ddlLocation.TextField = "HospitalName"
            'ddlLocation.DataBind()

            With ddlLocation
                .Items.Add("---ทั้งหมด---")
                .Items(0).Value = "0"
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("LocationName"))
                    .Items(i + 1).Value = dt.Rows(i)("LocationID")
                Next
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click
        Dim dStart As String = ConvertStrDate2DBString(txtStartDate.Text)
        Dim dEnd As String = ConvertStrDate2DBString(txtStartDate.Text)

        Dim LID As String = "0"
        Dim rptGRP As String = "ALL"
        If Session("RoleID") = isShopAccess Then
            LID = Session("LocationID")
        ElseIf Session("RoleID") = isReportViewerAccess Then
            rptGRP = Session("RPTGRP")
            LID = ddlLocation.Value
        Else
            LID = ddlLocation.Value
        End If
        Select Case Request("ItemType")
            Case "fncp"
                If Session("RoleID") = isShopAccess Then
                    Response.Redirect("rptFinanceCustomerSummarize.aspx?lid=" & LID & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
                Else
                    Response.Redirect("rptFinanceSummarizeByProvince.aspx?grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
                End If
            Case "mcount", "mtmcount", "hvcount", "pcount"
                Response.Redirect("ReportViewer.aspx?rpt=" & Request("ItemType") & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            Case "mtm"
                Response.Redirect("ReportViewer.aspx?rpt=mtm&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            Case "dr"
                Response.Redirect("ReportViewer.aspx?rpt=mtm&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            Case Else
                Response.Redirect("rptCustomerByActivityAndProvince.aspx?grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&lid=" & LID & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
        End Select
    End Sub
    Protected Sub ddlProvince_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvince.SelectedIndexChanged
        LoadLocationToDDL()
    End Sub
End Class