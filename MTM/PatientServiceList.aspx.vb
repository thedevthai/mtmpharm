﻿Imports System.Data
Imports System.Data.SqlClient
Imports Rajchasi
Public Class PatientServiceList
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlSmk As New SmokingController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then

            pnNo.Visible = False

            ddlYear.SelectedValue = DisplayYear(Today.Date)

            LoadProvinceToDDL()

            grdData.PageIndex = 0
            LoadActivityListToGrid()

            If Session("RoleID") = isShopAccess Then
                grdData.Columns(2).Visible = False
                lblProv.Visible = False
                ddlProvinceID.Visible = False
            Else
                grdData.Columns(2).Visible = True
                lblProv.Visible = True
                ddlProvinceID.Visible = True

            End If

            If Request("r") = "y" Then
                pnResult.Visible = True
            Else
                pnResult.Visible = False
            End If

        End If
    End Sub
    Protected Function DateText(ByVal input As Date) As String
        Dim dStr As String
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/0544", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/0001", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/0544", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/2443", "", input.ToString("dd/MM/yyyy"))
        Return dStr
    End Function

    Protected Function ConvertValue(ByVal pValue As Integer) As Integer

        If pValue = 0 Then
            Return 1
        Else
            Return 0
        End If
    End Function

    Private Sub LoadProvinceToDDL()
        Dim ctlOrder As New OrderController
        If Session("RoleID") = isReportViewerAccess Then
            dt = ctlOrder.Province_GetInGroup(Session("RPTGRP"))
        Else
            dt = ctlOrder.Province_GetInLocation
        End If


        ddlProvinceID.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlProvinceID
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                If Session("RoleID") = isReportViewerAccess Then
                    .Items(0).Value = Session("RPTGRP")
                Else
                    .Items(0).Value = "0"
                End If

                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("ProvinceName"))
                    .Items(i + 1).Value = dt.Rows(i)("ProvinceID")
                Next
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadActivityListToGrid()

        If Session("RoleID") = isShopAccess Then
            dt = ctlSmk.PatientSmoking_GetByLocation(Session("LocationID"), Trim(txtSearch.Text), "0", CInt(ddlYear.SelectedValue))
            'ElseIf Session("RoleID") = isReportViewerAccess  Then
            '    dt = ctlSmk.Smoking_GetByProvinceGroup(Session("RPTGRP"),FORM_TYPE_ID_MTM, Trim(txtSearch.Text), 1, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
            'ElseIf Session("RoleID") = isProjectManager Then
            '    dt = ctlSmk.Smoking_GetByProjectID(Session("PRJMNG"), ddlForm.SelectedValue, Trim(txtSearch.Text), 1, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        Else
            dt = ctlSmk.PatientSmoking_Get(Trim(txtSearch.Text), ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        End If

        If dt.Rows.Count > 0 Then
            lblCount.Text = dt.Rows.Count
            pnNo.Visible = False
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()


            End With
        Else
            lblCount.Text = "0"
            pnNo.Visible = True
            grdData.Visible = False
            grdData.DataSource = Nothing
        End If

        dt = Nothing
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadActivityListToGrid()
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand

        If TypeOf e.CommandSource Is WebControls.Button Then
            Dim ButtonPressed As WebControls.Button = e.CommandSource
            Select Case ButtonPressed.ID
                Case "btnRefer"
                    Session("PatientID") = e.CommandArgument()
                    Response.Redirect("ReferReg.aspx?pid=" & e.CommandArgument())
            End Select

        End If

    End Sub

    Private Sub grdData_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub


    Protected Sub ddlProvinceID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvinceID.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadActivityListToGrid()

    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadActivityListToGrid()

    End Sub

    Protected Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        grdData.PageIndex = 0
        LoadActivityListToGrid()
    End Sub
End Class

