﻿Imports Rajchasi
Public Class PatientMedical
    Inherits System.Web.UI.Page
    'Public Shared pageName As String
    'Public Shared ItemCode As String
    Dim ctlP As New PatientController
    Dim dtP As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            txtCGNo.Visible = False
            lblCGday.Visible = False
            txtCGYear.Visible = False
            lblCGYear.Visible = False
            lblCgType.Visible = False
            optCigarette.Visible = False
            lblAlcohol.Visible = False
            txtAlcoholFQ.Visible = False

            LoadPatientMedical()
            LoadAllergy()
        End If

    End Sub
    Private Sub LoadPatientMedical()
        dtP = ctlP.Patient_GetByID(Session("patientid"))
        If dtP.Rows.Count > 0 Then
            With dtP.Rows(0)

                If String.Concat(.Item("Smoke")) <> "" Then
                    optSmoke.SelectedValue = String.Concat(.Item("Smoke"))
                End If
                txtCGYear.Text = String.Concat(.Item("SmokeYear"))
                txtCGNo.Text = String.Concat(.Item("SmokeCigarette"))

                If String.Concat(.Item("CigaretteType")) <> "" Then
                    optCigarette.SelectedValue = String.Concat(.Item("CigaretteType"))
                End If

                If String.Concat(.Item("Alcohol")) <> "" Then
                    optAlcohol.SelectedValue = String.Concat(.Item("Alcohol"))
                End If

                txtAlcoholFQ.Text = String.Concat(.Item("AlcoholFQ"))
            End With

            dtP = Nothing
        End If
    End Sub
    Private Sub LoadAllergy()
        dtP = ctlP.Patient_GetAllergy(StrNull2Zero(Session("patientid")), "")
        If dtP.Rows.Count > 0 Then
            optAllergy.SelectedValue = "Y"
            grdAllergy.DataSource = dtP
            grdAllergy.DataBind()
        End If
        dtP = Nothing
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        ctlP.PatientMedical_Save(Session("patientid"), StrNull2Zero(optSmoke.SelectedValue), StrNull2Zero(txtCGYear.Text), StrNull2Zero(txtCGNo.Text), StrNull2Zero(optCigarette.SelectedValue), StrNull2Zero(optAlcohol.SelectedValue), StrNull2Zero(txtAlcoholFQ.Text))

        DisplayMessage(Me.Page, "บันทึกเรียบร้อย")

    End Sub

    Protected Sub grdAllergy_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdAllergy.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(3).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub

    Protected Sub grdAllergy_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdAllergy.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel"
                    ctlP.PatientAllergy_Delete(StrNull2Zero(e.CommandArgument()))
                    LoadAllergy()
            End Select
        End If
    End Sub

    Protected Sub imgAddAllergy_Click(sender As Object, e As ImageClickEventArgs) Handles imgAddAllergy.Click
        If txtDrugAllergy.Text = "" Or txtSymptom.Text = "" Then
            DisplayMessage(Me.Page, "ระบุยาที่แพ้และอาการแสดงก่อน")
            Exit Sub
        End If
        ctlP.PatientAllergy_Save(Session("PatienID"), "", txtDrugAllergy.Text, txtSymptom.Text)
        LoadAllergy()

        txtDrugAllergy.Text = ""
        txtSymptom.Text = ""
    End Sub

    Protected Sub optSmoke_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optSmoke.SelectedIndexChanged
        Select Case optSmoke.SelectedValue
            Case "2"
                txtCGYear.Visible = True
                lblCGYear.Visible = True
            Case "1"
                lblCGday.Visible = True
                txtCGNo.Visible = True
                lblCGYear.Visible = True
                txtCGYear.Visible = True
                optAlcohol.Visible = True
            Case Else
                txtCGNo.Visible = False
                lblCGday.Visible = False
                txtCGYear.Visible = False
                lblCGYear.Visible = False
                lblCgType.Visible = False
                optCigarette.Visible = False
        End Select
    End Sub

    Protected Sub optAlcohol_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optAlcohol.SelectedIndexChanged
        Select Case optAlcohol.SelectedValue
            Case "3"
                lblAlcohol.Visible = True
                txtAlcoholFQ.Visible = True
            Case Else
                lblAlcohol.Visible = False
                txtAlcoholFQ.Visible = False
        End Select
    End Sub
End Class