﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucPatientInfo.ascx.vb" Inherits=".ucPatientInfo" %>

<table align="center" width="100%">
    <tr>
       <td align="left" rowspan="3" width="100px" >
        <asp:Image ID="imgPatient" runat="server" class="img-circle" ImageUrl="dist/img/user_blank.jpg" Height="75px" />
           <br />
           <small>PID:<asp:Label ID="lblPatientID" runat="server"  CssClass="patientinfo" Text="00"></asp:Label></small>
 </td>
        <td align="left">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                <tr>      
      
        <td width="100" class="texttopic">ชื่อ-นามสกุล</td>
        <td >
            <asp:Label ID="lblName" runat="server" CssClass="patientinfo"></asp:Label>
        </td>
        <td width="40" class="texttopic">เพศ</td>
        <td>
            <asp:Label ID="lblGender" runat="server"  CssClass="patientinfo"></asp:Label>
        </td>
        <td class="texttopic" width="40">อายุ</td>
        <td width="40">
            <asp:Label ID="lblAges" runat="server"  CssClass="patientinfo"></asp:Label>
        </td>
       
      </tr>  
                <tr>      
      
        <td width="100" class="texttopic">เลขบัตร ปชช.</td>
        <td >
          <table border="0" cellpadding="0" cellspacing="0"  width="100%">
               <tr>
        <td><asp:Label ID="lblCardID" runat="server"  CssClass="patientinfo"></asp:Label></td>
        <td width="60" class="texttopic">เบอร์โทร </td>
      <td><asp:Label ID="lblTel" runat="server"  CssClass="patientinfo"></asp:Label></td>

      </tr>     
            </table></td>
        <td width="90" class="texttopic">สิทธิการรักษา</td>
        <td colspan="3">
            <asp:Label ID="lblClaim" runat="server"  CssClass="patientinfo"></asp:Label></td>       
      </tr>  
            </table>
        </td>
    </tr>   
    <tr>                <td align="left">
            
 <table border="0" cellpadding="0" cellspacing="0"  width="100%"> 
         
      <tr>
        <td width="40" class="texttopic">ที่อยู่</td>
        <td><asp:Label ID="lblAddress" runat="server"  CssClass="patientinfo"></asp:Label></td>
        
      </tr>     
      
    </table>

        </td>
    </tr>
</table>
