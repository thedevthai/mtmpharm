﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucMenuVertical.ascx.vb" Inherits=".ucMenuVertical" %>
<ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
        <% If Request("actionType") = "h"  %>
            <li class="active"><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> <span>หน้าแรก</span></a></li>
        <% Else %>
            <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> <span>หน้าแรก</span></a></li> 
        <% End If %> 

      <li>
                <% If Request.QueryString("ActionType") = "pt" Then%>
                    <a class="active" href="Patient.aspx?ActionType=pt&ItemType=pt"><i class="fa fa-user"></i> <span>Patient</span></a>
                <% Else%>
                    <a href="Patient.aspx?ActionType=pt&ItemType=pt"><i class="fa fa-user"></i> <span>Patient</span></a>
                <% End If%>
            </li>
        	 <li>
                <% If Request.QueryString("ActionType") = "ptl" Then%>
                    <a class="active" href="PatientSearch.aspx?ActionType=ptl&ItemType=list"><i class="fa fa-users"></i> <span>Patient Search</span></a>
                <% Else%>
                    <a href="PatientSearch.aspx?ActionType=ptl&ItemType=list"><i class="fa fa-users"></i> <span>Patient Search</span></a>
                <% End If%>
            </li>           

    <li>
                <% If Request.QueryString("ActionType") = "emr" Then%>
                    <a class="active" href="EMR.aspx?ActionType=emr"><i class="fa fa-users"></i> <span>EMR Viewer</span></a>
                <% Else%>
                    <a href="EMR.aspx?ActionType=emr"><i class="fa fa-users"></i> <span>EMR Viewer</span></a>
                <% End If%>
            </li>  
  <li>
                <% If Request.QueryString("ActionType") = "mtm" Then%>
                    <a class="active" href="FormList.aspx?ActionType=act&ItemType=mtm"><i class="fa fa-users"></i> <span>รายการกิจกรรม</span></a>
                <% Else%>
                    <a href="FormList.aspx?ActionType=act&ItemType=mtm"><i class="fa fa-users"></i> <span>รายการกิจกรรม</span></a>
                <% End If%>
            </li>  
             

       <% If Session("RoleID") = isShopAccess Then%>
            <li>
                <% If Request("ItemType") = "shop" Then%>
                    <a class="active" href="LocationsEdit.aspx?ActionType=cus&ItemType=shop"><i class="fa fa-hospital-o"></i><span>ข้อมูลร้านยา</span></a>
                <% Else%>
                    <a href="LocationsEdit.aspx?ActionType=cus&ItemType=shop"><i class="fa fa-hospital-o"></i><span>ข้อมูลร้านยา</span></a>
                <% End If%>
            </li>

    <!--- ใบสั่งยาจาก รพ. ---->
    <li>
                <% If Request("ItemType") = "prescription" Then%>
                    <a class="active" href="apiPrescription.aspx?ActionType=cus&ItemType=prescription"><i class="fa fa-hospital-o"></i><span>ใบสั่งยา</span></a>
                <% Else%>
                    <a href="apiPrescription.aspx?ActionType=cus&ItemType=prescription"><i class="fa fa-hospital-o"></i><span>ใบสั่งยา</span></a>
                <% End If%>
            </li>



            
<% End If%>

            
               <% If Session("RoleID") = isAdminAccess Or Session("RoleID") = isSuperAdminAccess Or (Session("RoleID") = isProjectManager And Session("PRJMNG") = PROJECT_SMOKING) Then%>
           
    
      <% If Request.QueryString("ActionType") = "shop" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-hospital-o"></i> <span>ข้อมูลร้านยา</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

     

               	        
<% If Session("RoleID") = isAdminAccess Or Session("RoleID") = isSuperAdminAccess Then%>
            
      <% If Request("ItemType") = "grp" Then%>
        <li class="active"><a href="LocationGroup.aspx?ActionType=shop&ItemType=grp"> <i class="fa fa-home"></i>ประเภทร้านยา </a></li>
      <% Else %>
        <li><a href="LocationGroup.aspx?ActionType=shop&ItemType=grp"><i class="fa fa-home"></i> ประเภทร้านยา</a></li>
      <% End If %>
                 
      <% If Request("ItemType") = "AddShop" Then%>
        <li class="active"><a href="Locations.aspx?ActionType=shop&ItemType=AddShop"><i class="fa fa-hospital-o"></i>ร้านยา</a></li>
      <% Else %>
        <li><a href="Locations.aspx?ActionType=shop&ItemType=AddShop"><i class="fa fa-hospital-o"></i>ร้านยา</a></li>
      <% End If %> 

<% End If %>      
 
<% If Session("RoleID") = isAdminAccess Or Session("RoleID") = isSuperAdminAccess Or (Session("RoleID") = isProjectManager And Session("PRJMNG") = PROJECT_SMOKING) Then%>                  
      <% If Request("ItemType") = "AddPM" Then%>
        <li class="active"><a href="Pharmacist.aspx?ActionType=shop&ItemType=AddPM"><i class="fa  fa-user-md"></i>เภสัชกร</a></li>
      <% Else %>
        <li><a href="Pharmacist.aspx?ActionType=shop&ItemType=AddPM"><i class="fa fa-user-md"></i>เภสัชกร</a></li>
      <% End If %> 

<% End If%>

          </ul>     
            </li>


  <% End If%>
     

    <% If Request("ActionType") = "rpt" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-pie-chart"></i> <span>รายงาน</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
 <% If Session("RoleID") = isShopAccess Then%>
        <% If Request("ItemType") = "fnc" Then%>
          <li class="active"><a href="ReportCondition.aspx?ActionType=rpt&ItemType=fnc"><i class="fa fa-bar-chart"></i>รายงานสรุปจำนวนผู้รับบริการ</a></li>
      <% Else%>
          <li><a href="ReportCondition.aspx?ActionType=rpt&ItemType=fnc"><i class="fa fa-bar-chart"></i>รายงานสรุปจำนวนผู้รับบริการ</a></li>
      <% End If%>            
     
      <% If Request("ItemType") = "cus" Then%>
          <li class="active"><a href="ReportCondition.aspx?ActionType=rpt&ItemType=cus"><i class="fa fa-pie-chart"></i>รายงานรายชื่อผู้เข้ารับบริการ</a></li>
      <% Else%>
          <li><a href="ReportCondition.aspx?ActionType=rpt&ItemType=cus"><i class="fa fa-pie-chart"></i>รายงานรายชื่อผู้เข้ารับบริการ</a></li>
      <% End If%> 
 <% End If%>  


  <% If Session("RoleID") = isAdminAccess Or Session("RoleID") = isSuperAdminAccess Then%>    
              
     <% If Request("ItemType") = "mtm" Then%>
          <li class="active"><a href="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=mtm"><i class="fa fa-bar-chart"></i>รายงาน MTM</a></li>
      <% Else%>
          <li><a href="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=mtm"><i class="fa fa-bar-chart"></i>รายงาน MTM</a></li>
      <% End If%>            
  
      <% If Request("ItemType") = "mcount" Then%>
          <li class="active"><a href="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=mcount"><i class="fa fa-pie-chart"></i>รายงานสรุปจำนวนบริการ</a></li>
      <% Else%>
          <li><a href="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=mcount"><i class="fa fa-pie-chart"></i>รายงานสรุปจำนวนบริการ</a></li>
      <% End If%> 

       <% If Request("ItemType") = "pcount" Then%>
          <li class="active"><a href="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=pcount"><i class="fa fa-pie-chart"></i>รายงานสรุปปัญหารายบุคคล</a></li>
      <% Else%>
          <li><a href="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=pcount"><i class="fa fa-pie-chart"></i>รายงานสรุปปัญหารายบุคคล</a></li>
      <% End If%> 


  <%-- 
      <% If Request("ItemType") = "fnc" Then%>
          <li class="active"><a href="ReportCondition.aspx?ActionType=rpt&ItemType=fnc"><i class="fa fa-bar-chart"></i>รายงานปัญหาพฤติกรรม</a></li>
      <% Else%>
          <li><a href="ReportCondition.aspx?ActionType=rpt&ItemType=fnc"><i class="fa fa-bar-chart"></i>รายงานปัญหาพฤติกรรม</a></li>
      <% End If%>            
     
      <% If Request("ItemType") = "cus" Then%>
          <li class="active"><a href="ReportCondition.aspx?ActionType=rpt&ItemType=cus"><i class="fa fa-pie-chart"></i>รายงานการจ่ายยา</a></li>
      <% Else%>
          <li><a href="ReportCondition.aspx?ActionType=rpt&ItemType=cus"><i class="fa fa-pie-chart"></i>รายงานการจ่ายยา</a></li>
      <% End If%> 

      <% If Request("ItemType") = "cus" Then%>
          <li class="active"><a href="ReportCondition.aspx?ActionType=rpt&ItemType=cus"><i class="fa fa-pie-chart"></i>รายงานยาเหลือใช้</a></li>
      <% Else%>
          <li><a href="ReportCondition.aspx?ActionType=rpt&ItemType=cus"><i class="fa fa-pie-chart"></i>รายงานยาเหลือใช้</a></li>
      <% End If%> 

               <% If Request("ItemType") = "cus" Then%>
          <li class="active"><a href="ReportCondition.aspx?ActionType=rpt&ItemType=cus"><i class="fa fa-pie-chart"></i>รายงานปัญหาการใช้ยา</a></li>
      <% Else%>
          <li><a href="ReportCondition.aspx?ActionType=rpt&ItemType=cus"><i class="fa fa-pie-chart"></i>รายงานปัญหาการใช้ยา</a></li>
      <% End If%> 

               <% If Request("ItemType") = "cus" Then%>
          <li class="active"><a href="ReportCondition.aspx?ActionType=rpt&ItemType=cus"><i class="fa fa-pie-chart"></i>รายงานจำนวนครั้งของการเติมยา </a></li>
      <% Else%>
          <li><a href="ReportCondition.aspx?ActionType=rpt&ItemType=cus"><i class="fa fa-pie-chart"></i>รายงานจำนวนครั้งของการเติมยา </a></li>
      <% End If%> 

               <% If Request("ItemType") = "cus" Then%>
          <li class="active"><a href="ReportCondition.aspx?ActionType=rpt&ItemType=cus"><i class="fa fa-pie-chart"></i>รายงานการ  refer </a></li>
      <% Else%>
          <li><a href="ReportCondition.aspx?ActionType=rpt&ItemType=cus"><i class="fa fa-pie-chart"></i>รายงานการ  refer </a></li>
      <% End If%> --%>

          <% If Request("ItemType") = "loc" Then%>
          <li class="active"><a href="ReportLocations.aspx?ActionType=rpt&ItemType=loc"><i class="fa fa-pie-chart"></i>ร้านยาแยกประเภทและจังหวัด</a></li>
      <% Else%>
          <li><a href="ReportLocations.aspx?ActionType=rpt&ItemType=loc"><i class="fa fa-pie-chart"></i>ร้านยาแยกประเภทและจังหวัด</a></li>
      <% End If%> 

        <% If Request("ItemType") = "1" Then%>
          <li class="active"><a href="ReportLocationsGroup.aspx?ActionType=rpt&ItemType=1"><i class="fa fa-pie-chart"></i>ร้านยาในโครงการ</a></li>
      <% Else%>
          <li><a href="ReportLocationsGroup.aspx?ActionType=rpt&ItemType=1"><i class="fa fa-pie-chart"></i>ร้านยาในโครงการ</a></li>
      <% End If%> 

      <% If Request("ItemType") = "dr" Then%>
          <li class="active"><a href="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=dr"><i class="fa fa-pie-chart"></i>รายงานยาเหลือใช้</a></li>
      <% Else%>
          <li><a href="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=dr"><i class="fa fa-pie-chart"></i>รายงานยาเหลือใช้</a></li>
      <% End If%> 


 <% End If%> 
              
              </ul>
             </li>
 

  <% If Session("RoleID") = isAdminAccess Or Session("RoleID") = isSuperAdminAccess Then%>

     <% If Request("ActionType") = "news" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-bullhorn"></i> <span>ข่าวประชาสัมพันธ์</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <% If Request("ItemType") = "news" Then%>
        <li class="active"><a href="News_List.aspx?ActionType=news&ItemType=news&patientid=0"> รายการข่าวประชาสัมพันธ์ </a></li>
      <% Else %>
        <li><a href="News_List.aspx?ActionType=news&ItemType=news&patientid=0"> รายการข่าวประชาสัมพันธ์</a></li>
      <% End If %>

       <% If Request("ItemType") = "addnews" Then%>
        <li class="active"><a href="News_Manage.aspx?ActionType=news&ItemType=addnews&patientid=0"> เพิ่มข่าวประชาสัมพันธ์ </a></li>
      <% Else %>
        <li><a href="News_Manage.aspx?ActionType=news&ItemType=addnews&patientid=0"> เพิ่มข่าวประชาสัมพันธ์</a></li>
      <% End If %>
</ul></li>

     
     <% If Request("ActionType") = "setting" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-gears"></i> <span>ตั้งค่าระบบ</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
                 
      <% If Request("ItemType") = "gprov" Then%>
        <li class="active"><a href="ProvincesGroup.aspx?ActionType=setting&ItemType=gprov">กลุ่มจังหวัด/ภาค</a></li>
      <% Else %>
        <li><a href="ProvincesGroup.aspx?ActionType=setting&ItemType=gprov">กลุ่มจังหวัด/ภาค</a></li>
      <% End If %>  

      <% If Request("ItemType") = "prov" Then%>
        <li class="active"><a href="Provinces.aspx?ActionType=setting&ItemType=prov">จังหวัด</a></li>
      <% Else %>
        <li><a href="Provinces.aspx?ActionType=setting&ItemType=prov">จังหวัด</a></li>
      <% End If %>  
       <% If Request("ItemType") = "bank" Then%>
        <li class="active"><a href="Banks.aspx?ActionType=setting&ItemType=bank">ธนาคาร</a></li>
      <% Else %>
        <li><a href="Banks.aspx?ActionType=setting&ItemType=bank">ธนาคาร</a></li>
      <% End If %> 
       
               <% If Request("ItemType") = "hos" Then%>
        <li class="active"><a href="Hospital.aspx?ActionType=setting&ItemType=hos">โรงพยาบาล</a></li>
      <% Else %>
        <li><a href="Hospital.aspx?ActionType=setting&ItemType=hos">โรงพยาบาล</a></li>
      <% End If %> 


  
    
      <% If Request("ItemType") = "med" Then%>
        <li class="active"><a href="Drug.aspx?ActionType=setting&ItemType=med">Drug Master (ยา)</a></li>
      <% Else %>
        <li><a href="Drug.aspx?ActionType=setting&ItemType=med">Drug Master (ยา)</a></li>
      <% End If %> 

      <% If Request("ItemType") = "des" Then%>
        <li class="active"><a href="Desease.aspx?ActionType=setting&ItemType=des">โรค</a></li>
      <% Else %>
        <li><a href="Desease.aspx?ActionType=setting&ItemType=des">โรค</a></li>
      <% End If %>
                      
   <%--   <% If Request("ItemType") = "icd10" Then%>
        <li class="active"><a href="ICD10.aspx?ActionType=setting&ItemType=icd10">ICD10</a></li>
      <% Else %>
        <li><a href="ICD10.aspx?ActionType=setting&ItemType=icd10">ICD10</a></li>
      <% End If %> --%>
                            

       </ul></li>

            <% If Session("RoleID") = isSuperAdminAccess Then%>
                        <% If Request("ActionType") = "user" Then  %>
                            <li class="active treeview">
                        <% Else %>
                            <li class="treeview">
                        <% End If %>        
                          <a href="#">
                            <i class="fa fa-users"></i> <span>ข้อมูลผู้ใช้งาน(User)</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                          </a>
                  <ul class="treeview-menu">
                      <% If Request("ItemType") = "add" Then%>
                        <li class="active"><a href="Users.aspx?ActionType=user&ItemType=add"> <i class="fa fa-users"></i> เพิ่มผู้ใช้งาน </a></li>
                      <% Else %>
                        <li><a href="Users.aspx?ActionType=user&ItemType=add"> <i class="fa fa-users"></i> เพิ่มผู้ใช้งาน</a></li>
                      <% End If %>
                 
                      <% If Request("ItemType") = "history" Then%>
                        <li class="active"><a href="LogfilesByUser.aspx?ActionType=user&ItemType=history"> <i class="fa fa-navicon"></i>ตรวจสอบประวัติการใช้งาน</a></li>
                      <% Else %>
                        <li><a href="LogfilesByUser.aspx?ActionType=user&ItemType=history"> <i class="fa fa-navicon"></i>ตรวจสอบประวัติการใช้งาน</a></li>
                      <% End If %> 
                  </ul>
                       </li>
            <% End If%>

  <% End If%>
     
                               
         <li><a href="ChangePassword.aspx?logout=y"><i class="fa fa-key"></i> <span>เปลี่ยนรหัสผ่าน</span></a></li>    
                 
        <li><a href="Default.aspx?logout=y"><i class="fa fa-lock"></i> <span>ออกจากระบบ</span></a></li>
      
      <!--  <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
        -->
      </ul>