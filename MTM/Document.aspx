﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Document.aspx.vb" Inherits=".Document" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register Src="ucPatientInfo.ascx" TagPrefix="uc2" TagName="ucPatientInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/mtmstyles.css">

  <link rel="stylesheet" type="text/css" href="css/uidialog.css">
  <link rel="stylesheet" type="text/css" href="css/pagestyles.css"> 
  <link rel="stylesheet" type="text/css" href="css/button.css"> 
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server"> 
        <section class="content-header">
      <h1>
        Document Upload
        <small>อัพโหลดเอกสาร</small>
      </h1> 
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
  <div class="row">
    <section class="col-lg-12 connectedSortable">    
      <div class="box box-solid box-success">
            <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">ข้อมูลผู้รับบริการ</h3>             
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>                 
            </div>
            <div class="box-body">
                <uc2:ucPatientInfo runat="server" ID="ucPatientInfo" />
            </div>            
          </div>
    </section>

</div>
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-6 connectedSortable">     
 
      

        
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">เอกสารอัพโหลด</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  
     
        <table width="100%">
             <tr>
                 <td width="100">File</td>
                 <td>
                     <asp:FileUpload ID="FileUpload1" runat="server" Width="300px" />
                 </td>
            </tr> 
            <tr>
                <td>Description</td>
                <td>
                    <asp:TextBox ID="txtDesc" runat="server" CssClass="OptionControl2" Width="90%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td valign="top">&nbsp;</td>
                <td>
                    <asp:Button ID="cmdUploadFile" runat="server" CssClass="buttonSave" Text="Upload" Width="100px" />
                    &nbsp;</td>
            </tr>
    </table>
 
                      <asp:Panel ID="pnDocumentAlert" runat="server" Width="100%">
                         <div class="alert alert-danger alert-dismissible">
                             <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                             <p>
                                 <asp:Label ID="lblDocAlert" runat="server"></asp:Label>
                             </p>
                         </div>
                     </asp:Panel>
         
       
      </div>
      <!-- /.box -->  
</div> 
  
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-6 connectedSortable">
            
      

        <div class="box box-success">
            <div class="box-header">
              <i class="ion ion-medkit"></i>
              <h3 class="box-title">รายการเอกสาร</h3>
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
                 
            </div>
          
            <div class="box-body">
              
                                       
                <asp:GridView ID="grdDocument" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                    <RowStyle BackColor="White" VerticalAlign="Top" />
                    <columns>
                        <asp:HyperLinkField DataNavigateUrlFields="FilePath" DataTextField="DocDate" DataTextFormatString="{0:d}" HeaderText="วันที่" Target="_blank">
                        <ItemStyle HorizontalAlign="Center" Width="100px" />
                        </asp:HyperLinkField>
                        <asp:HyperLinkField DataNavigateUrlFields="FilePath" DataTextField="Descriptions" HeaderText="รายการเอกสารอัพโหลด" Target="_blank" />
                        <asp:TemplateField HeaderText="ลบ">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgDel" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" Height="20px" ImageUrl="images/delete.png" />
                            </ItemTemplate>
                            <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                        </asp:TemplateField>
                    </columns>
                    <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                    <EditRowStyle BackColor="#2461BF" />
                    <AlternatingRowStyle BackColor="#F7F7F7" />
                </asp:GridView>
              
                                       
            </div> 
            <div class="box-footer clearfix no-border">
              
            </div>
          </div>       
 
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->

  <!-- /.content-wrapper -->
  <div class="control-sidebar-bg"></div>
    
</asp:Content>
