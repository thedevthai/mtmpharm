﻿Imports Rajchasi
Public Class ReportLocationsGroup
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadProjectToDDL()
        End If

    End Sub

    Private Sub LoadProjectToDDL()
        Dim ctlPj As New ProjectController

        If (Session("RoleID") = isAdminAccess) Or (Session("RoleID") = isSuperAdminAccess) Then
            dt = ctlPj.Project_GetAll
        Else
            dt = ctlPj.Project_GetByUser(Session("UserID"))
        End If


        If dt.Rows.Count > 0 Then
            With ddlType
                .DataSource = dt
                .DataTextField = "Description"
                .DataValueField = "ProjectID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Protected Sub cmdView_Click(sender As Object, e As EventArgs) Handles cmdView.Click

        FagRPT = "LocationGroup"
        Reportskey = "XLS"
        DisplayPopUpWindows(Me.Page, "window.open('" + ResolveUrl("ReportViewer.aspx?m=rpt&p=p1&pjid=") + ddlType.SelectedValue + "','pop','width=1000,height=800,left=270,top=180,titlebar=no,menubar=no,resizable=yes,toolbar=no,scrollbars=no');")

    End Sub
End Class

