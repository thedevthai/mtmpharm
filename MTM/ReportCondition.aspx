﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReportCondition.aspx.vb" Inherits=".ReportCondition" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1><asp:Label ID="lblReportHeader" runat="server" Text="รายงาน"></asp:Label>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">รายงาน</li>
      </ol>
    </section>

<section class="content">    
        <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-edit"></i>

              <h3 class="box-title">รายงาน</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">





    <table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td height="300" valign="top">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td  valign="top"> <table border="0" align="center" cellpadding="0" cellspacing="3">
  <tr>
    <td>ตั้งแต่</td>
    <td>
        <asp:TextBox ID="txtStartDate" runat="server">        </asp:TextBox>   </td>
    <td>ถึง</td>
    <td>
        <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>           </td>
  </tr>
        
</table></td>
    </tr>
    <tr>
      <td align="center">
        &nbsp;<asp:Button ID="cmdExcel" runat="server" Text="ส่งออก Excel" CssClass="buttonSave" />
        </td>
    </tr>
    <tr>
      <td align="center">
          <asp:Label ID="lblAlert" runat="server" Font-Size="12pt" ForeColor="Red"></asp:Label>
        </td>
    </tr>
  </table>
  </td> 
      </tr>
    </table>

            </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

</section>
</asp:Content>
