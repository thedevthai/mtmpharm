﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="Refer.aspx.vb" Inherits=".Refer" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register Src="ucPatientInfo.ascx" TagPrefix="uc2" TagName="ucPatientInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   

    </asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


     <section class="content-header">
      <h1>Refer
        <small>ส่งต่อผู้รับบริการ</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Refer</li>
      </ol>
    </section>

<section class="content">  
    
    
       <div class="box box-success">
        <div class="box-header with-border">
             <i class="ion ion-forward"></i>
          <h3 class="box-title">Refer</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse"><i class="fa fa-plus"></i></div>
        </div>
        <div class="box-body">  
     
        <table width="100%">
             <tr>
                                                    <td align="left">
                                                        วันที่ Refer</td>
                                                    <td align="left" >
                                                        <asp:TextBox ID="txtDate" runat="server" Width="200px" ></asp:TextBox>
                                                    </td>
                                                </tr>
             <tr>
                 <td>Refer to</td>
                 <td>
                     <dx:aspxcombobox ID="cboHospital" runat="server" CssClass="OptionControl2" Theme="MetropolisBlue" EnableTheming="True" ToolTip="พิมพ์ค้นหาได้" Width="100%">
                     </dx:aspxcombobox>
                 </td>
            </tr>
            <tr>
                <td>เหตุผลที่ Refer</td>
                <td><asp:TextBox ID="txtReason" runat="server" CssClass="OptionControl2" Width="100%"></asp:TextBox>
                   
                </td>
            </tr>
            <tr>
                <td valign="top">&nbsp;</td>
                <td>
                    <asp:Button ID="cmdRefer" runat="server" CssClass="buttonSave" Text="บันทึก" Width="100px" />
                    &nbsp;<asp:Button ID="cmdPrint" runat="server" CssClass="buttonSave" Text="พิมพ์ใบ Refer" Width="100px" Visible="False" />
                    </td>
            </tr>
    </table>
 
                     <asp:Panel ID="pnRefer" runat="server">
                         <div class="alert alert-danger alert-dismissible">
                             <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                             <p>
                                 <asp:Label ID="Label2" runat="server"></asp:Label>
                             </p>
                         </div>
                     </asp:Panel>
           
       
             <!-- /.box-body -->
         <div class="box-footer clearfix">
           
            </div>
      </div>
      <!-- /.box -->  
</div>     
          
    </section>
</asp:Content>
