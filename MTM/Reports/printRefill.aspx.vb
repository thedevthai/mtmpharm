﻿'Imports System.IO
'Imports iTextSharp.text
'Imports iTextSharp.text.pdf
'Imports iTextSharp.text.html
'Imports iTextSharp.text.xml
'Imports iTextSharp.text.html.simpleparser
Imports Rajchasi
Public Class printRefill
    Inherits System.Web.UI.Page

    Dim ctlMTM As New MTMController
    Dim ctlD As New DrugController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request("id") Is Nothing Then
                LoadData(Request("rno"))
                ' ASPXToPDF1.RenderAsPDF()
            End If
        End If
        ' ExportToPDF()
    End Sub

    'Private Sub ExportToPDF()

    '    Response.ContentType = "application/pdf"
    '    Response.AddHeader("content-disposition", "attachment;filename=TestPage.pdf")
    '    Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '    Dim sw As New StringWriter()
    '    Dim hw As New HtmlTextWriter(sw)
    '    Me.Page.RenderControl(hw)
    '    Dim sr As New StringReader(sw.ToString())
    '    Dim pdfDoc As New Document(PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F)
    '    Dim htmlparser As New HTMLWorker(pdfDoc)
    '    PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
    '    pdfDoc.Open()
    '    Try
    '        htmlparser.Parse(sr)
    '    Catch ex As Exception

    '    End Try

    '    pdfDoc.Close()
    '    Response.Write(pdfDoc)
    '    Response.[End]()

    'End Sub
    Private Sub LoadData(ByVal RNO As Integer)
        Dim dtP As New DataTable


        dtP = ctlMTM.DrugRefill_GetByUID(RNO)

        If dtP.Rows.Count > 0 Then
            With dtP.Rows(0)

                lblLocationName.Text = .Item("LocationName")
                lblRefillNo.Text = RNO
                lblDate.Text = DBNull2Str(.Item("RefillDate"))

                lblName.Text = .Item("PatientName")
                lblGender.Text = DBNull2Str(.Item("Sex"))
                lblAges.Text = DBNull2Str(.Item("Age"))
                lblCardID.Text = DBNull2Str(.Item("CardID"))
                lblClaim.Text = DBNull2Str(.Item("Claim"))
                lblAddress.Text = DBNull2Str(.Item("Address"))
                lblTel.Text = DBNull2Str(.Item("Tel"))
                lblUser.Text = Zero2StrNull(.Item("RefillBy"))

                LoadDrugRefill(RNO)

            End With


        End If


        dtP = Nothing
    End Sub
    Private Sub LoadDrugRefill(ByVal RNO As Integer)
        Dim dtP As New DataTable
        dtP = ctlMTM.DrugRefill_GetByRefillNo(RNO)
        If dtP.Rows.Count > 0 Then
            grdDrugRefill.DataSource = dtfInfo
            grdDrugRefill.DataBind()
        End If
    End Sub

    'Private Sub RenderPDF(writer As HtmlTextWriter)
    '    Dim mem As New MemoryStream()
    '    Dim twr As New StreamWriter(mem)
    '    Dim myWriter As New HtmlTextWriter(twr)
    '    MyBase.Render(myWriter)
    '    myWriter.Flush()
    '    myWriter.Dispose()
    '    Dim strmRdr As New StreamReader(mem)
    '    strmRdr.BaseStream.Position = 0
    '    Dim pageContent As String = strmRdr.ReadToEnd()
    '    strmRdr.Dispose()
    '    mem.Dispose()
    '    writer.Write(pageContent)
    '    CreatePDFDocument(pageContent)


    'End Sub


    'Private Sub CreatePDFDocument(strHtml As String)

    '    Dim strFileName As String = HttpContext.Current.Server.MapPath("test.pdf")
    '    ' step 1: creation of a document-object
    '    Dim document As New Document()
    '    ' step 2:
    '    ' we create a writer that listens to the document
    '    PdfWriter.GetInstance(document, New FileStream(strFileName, FileMode.Create))
    '    Dim se As New StringReader(strHtml)
    '    Dim obj As New HTMLWorker(document)
    '    document.Open()
    '    obj.Parse(se)
    '    document.Close()
    '    ShowPdf(strFileName)



    'End Sub

    'Private Sub ShowPdf(strFileName As String)
    '    Response.ClearContent()
    '    Response.ClearHeaders()
    '    Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName)
    '    Response.ContentType = "application/pdf"
    '    Response.WriteFile(strFileName)
    '    Response.Flush()
    '    Response.Clear()
    'End Sub




End Class