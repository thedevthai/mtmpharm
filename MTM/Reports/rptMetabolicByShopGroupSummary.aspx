﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="rptMetabolicByShopGroupSummary.aspx.vb" Inherits=".rptMetabolicByShopGroupSummary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
 <style type="text/css">
         Body{
	background: #fff;
	margin: 0 0 0 0;
	padding: 0 0 0 0;
	font-family: Geneva, Arial, Helvetica,Tahama, sans-serif;	
	color: #000;
	font-size: 13px;
	line-height: 25px;
}

.Page_Header{
font-size: 16px;
	font-style: normal;
	text-align: center;
	font-weight: bold;
  	color: #000;  
 	padding: 5px 0px 5px 0px;
	text-shadow: 1px 1px 1px rgba(255,255,255,0.8);
	
}
.texttopic{
	 	font-size: 13px;
	color:#555;
	font-weight: bold;
}
.TopicArticle
{
	color: #000;
	font-size: 14px;
	text-transform: uppercase;
	padding: 5px;
}
td.brd_rpt{
	border-top: 1px solid #5d5d5d;
	border-left: 1px solid #5d5d5d;
}
td.brd_rpt_r{
	border-top: 1px solid #5d5d5d;
	border-left: 1px solid #5d5d5d;
	border-right: 1px solid #5d5d5d;
}
 
 
     #cmdPrint
     {
         width: 66px;
     }
 
    
 </style>
</head>
<body>
    <form id="form1" runat="server">
  
         
  <script language="Javascript">
      function doprint() {
          //save existing user's info
          //  var h = factory.printing.header;
          //  var f = factory.printing.footer;
          //hide the button
          document.all("cmdPrint").style.visibility = 'hidden';

          window.print();
          ////  factory.printing.SetMarginMeasure(2); 
          //  factory.printing.portrait = true;
          //  factory.printing.leftMargin = 1.75;
          //  factory.printing.topMargin = 1.75;
          //  factory.printing.rightMargin = 0.75;
          //  factory.printing.bottomMargin = 1.75;


          ////set header and footer to blank
          //  factory.printing.header = "";
          //  factory.printing.footer = "";
          //  //print page without prompt
          //  factory.DoPrint(false);
          //  //restore user's info
          //  factory.printing.header = h;
          //  factory.printing.footer = f;
          //show the print button
          // document.all("prnButton").style.visibility = 'visible';
          //  document.all("AButton").style.visibility = 'visible';
          //  document.all("BButton").style.visibility = 'visible';
          document.all("cmdPrint").style.visibility = 'visible';

      }
   
    </script>
 <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" >
  <tr>
 <td align="center" class="Page_Header"> รายงานสรุปผลการคัดกรองความเสี่ยงโรคเมตาบอลิก</td>
    </tr>
    <tr>
 <td align="center" class="Page_Header">  
     <asp:Label ID="lblProvinceName" runat="server"></asp:Label>        </td>
    </tr>
    <tr>
    <td align="left" valign="top"><table border="0" align="center" cellpadding="0" cellspacing="2">
      <tr>
        <td width="45" align="center"><strong>ตั้งแต่</strong></td>
        <td>  <asp:Label ID="lblStartDate"        runat="server"></asp:Label>          </td>
        <td width="30" align="center"><strong>ถึง</strong></td>
        <td>  <asp:Label ID="lblEndDate"        runat="server"></asp:Label>          </td>
</tr>
    </table></td>
    </tr>    
     <tr>
                               <td align="center" valign="top">
                                <table width="100%" align="right" cellpadding="0" cellspacing="0"  >
<tr>
                                         <td align="center"></td>
                         <td width="60" align="center"></td>
            <td width="35" align="center"></td>
            <td width="35" align="center"></td>
            <td width="35" align="center"></td>
            <td width="35" align="center"></td>
            <td width="35" align="center">&nbsp;</td>
            <td width="35" align="center">&nbsp;</td>
            <td width="35" align="center">&nbsp;</td>
            <td width="35" align="center">&nbsp;</td>
            <td width="35" align="center">&nbsp;</td>
            <td width="35" align="center">&nbsp;</td>
            <td width="35" align="center">&nbsp;</td>
            <td width="35" align="center">&nbsp;</td>
            <td width="76" align="center">&nbsp;</td>
            <td width="60" align="center">&nbsp;</td>
            <td width="60" align="center">&nbsp;</td>
            <td width="80" align="center">&nbsp;</td>
            <td width="70" align="center">&nbsp;</td>
</tr>
                       <tr> 
                         <td align="center" >&nbsp;</td>
                         <td align="center" >&nbsp;</td>
                         <td colspan="12" align="center" class="brd_rpt"><strong>ประเภทร้านยา</strong></td>
                         <td colspan="5" rowspan="2" align="center" class="brd_rpt_r" ><strong>สรุปผล</strong></td>
                       </tr>
                       <tr>
                         <td align="center" >&nbsp;</td>
                         <td align="center" >&nbsp;</td>
                         <td colspan="2" align="center" class="brd_rpt">ร้านยาเดี่ยว</td>
                         <td colspan="2" align="center" class="brd_rpt" >Boots</td>
                         
                         <td colspan="2" align="center" class="brd_rpt">Watsons</td>
                         <td colspan="2" align="center" class="brd_rpt" >Xta</td>
                         <td colspan="2" align="center" class="brd_rpt">Pure</td>
                         <td colspan="2" align="center" class="brd_rpt" >ศศภท.</td>
                         </tr>
                                 </table>
       </td>                        
    </tr>   
    <tr>
    <td align="right">                                
<asp:GridView ID="grdSummary" runat="server" CellPadding="2" 
                      AutoGenerateColumns="False" 
                                     HorizontalAlign="Center" Width="100%">
                                     <RowStyle BackColor="#F7F7F7" VerticalAlign="Middle" HorizontalAlign="Center" />
                                     <columns>
                                         <asp:BoundField HeaderText="จังหวัด" DataField="ProvinceName">                                         
                                         <ItemStyle HorizontalAlign="Left" />
                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="รับบริการ" DataField="TotalCount">
                                         <ItemStyle HorizontalAlign="Center" Width="90px" />                                         </asp:BoundField>
                                         <asp:BoundField DataField="ACount" HeaderText="คัด">
                                         <ItemStyle HorizontalAlign="Center" Width="30px" />                                         </asp:BoundField>
<asp:BoundField  DataField="ACountYes"  HeaderText="เป็น">
                                         <ItemStyle Width="30px" ForeColor="Red" />
</asp:BoundField>
                                         <asp:BoundField DataField="BCount" HeaderText="คัด" >
                                         <ItemStyle Width="30px" />
                                         </asp:BoundField>
<asp:BoundField DataField="BCountYes"  HeaderText="เป็น">
 <ItemStyle Width="30px" ForeColor="Red" />
</asp:BoundField>
                                         <asp:BoundField HeaderText="คัด" DataField="WCount" >                                         
                                         <ItemStyle HorizontalAlign="Center" Width="30px" />                                         </asp:BoundField>
<asp:BoundField  DataField="WCountYes" HeaderText="เป็น"> <ItemStyle Width="30px" ForeColor="Red" />
</asp:BoundField>
                                         <asp:BoundField HeaderText="คัด" DataField="XCount" >
                                         <ItemStyle HorizontalAlign="Center" Width="30px" />                                         </asp:BoundField>
                                         <asp:BoundField  DataField="XCountYes"  HeaderText="เป็น"> <ItemStyle Width="30px" ForeColor="Red" /> </asp:BoundField>
                                         <asp:BoundField HeaderText="คัด" DataField="PCount" >
                                         <ItemStyle HorizontalAlign="Center" Width="30px" />  
                                         </asp:BoundField>
                                         <asp:BoundField DataField="PCountYes"  HeaderText="เป็น"> <ItemStyle Width="30px" ForeColor="Red" /></asp:BoundField>
                                         <asp:BoundField HeaderText="คัด" DataField="UCount" >
                                         <ItemStyle HorizontalAlign="Center" Width="30px" />                                         </asp:BoundField>
                                         <asp:BoundField  DataField="UCountYes"  HeaderText="เป็น"> <ItemStyle Width="30px" ForeColor="Red" /></asp:BoundField>
                                         <asp:BoundField HeaderText="เบาหวาน" DataField="D1Count" >
                                         <ItemStyle HorizontalAlign="Center" Width="60px" />                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="ความดัน" DataField="D2Count" >
                                         <ItemStyle Width="60px" />
                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="โรคอ้วน" DataField="D3Count" >
                                         <ItemStyle Width="60px" />
                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="รวมเป็นโรค" DataField="SumYesCount">
                                         <ItemStyle HorizontalAlign="Center" Width="70px" ForeColor="Red" />
                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="เป็น 2 โรค" DataField="DiseaseCount">
                                         <ItemStyle HorizontalAlign="Center" Width="70px" />
                                         </asp:BoundField>
                                     </columns>
                                     <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                     <pagerstyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                     <SelectedRowStyle BackColor="#FF8B00" Font-Bold="True" ForeColor="#333333" />
                                     <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                                         VerticalAlign="Middle"  />
                                     <EditRowStyle BackColor="#2461BF" />
                                     <AlternatingRowStyle BackColor="White" VerticalAlign="Top" />
                                 </asp:GridView>
        </td>
    </tr>
        <tr>
                               <td align="center" valign="top">&nbsp;</td>                        
    </tr>   
   
        <tr>
                               <td align="center" valign="top"><input  type="button" value="Print" id="cmdPrint"  onclick="doprint();" /></td>                        
    </tr>   
   
</table>
</form>
</body>
</html>
