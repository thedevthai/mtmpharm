﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="printRefill.aspx.vb" Inherits="printRefill" %>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <link href="css/reports.css" rel="stylesheet" type="text/css" />
      
<title></title>

    <style type="text/css">
.small,small{font-size:85%}small{font-size:80%}*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}*,:after,:before{color:#000!important;text-shadow:none!important;background:0 0!important;-webkit-box-shadow:none!important;box-shadow:none!important}


.th{
background-image:url(../images/th.gif);
text-indent:5px;
height:20px;
border-bottom: 1px solid #d0d0d0;
border-top: 1px solid #d0d0d0;
/*color: #555299;*/
color: #666;
font-size: 12px;
} 

.gridbutton{
	cursor: pointer;
}
    </style>

</head>
<body>
    <form id="form1" runat="server">
       
  <script language="Javascript">
      function doprint() {
          //save existing user's info
          //  var h = factory.printing.header;
          //  var f = factory.printing.footer;
          //hide the button
          document.all("cmdPrint").style.visibility = 'hidden';
          window.print();
          ////  factory.printing.SetMarginMeasure(2); 
          //  factory.printing.portrait = true;
          //  factory.printing.leftMargin = 1.75;
          //  factory.printing.topMargin = 1.75;
          //  factory.printing.rightMargin = 0.75;
          //  factory.printing.bottomMargin = 1.75;


          ////set header and footer to blank
          //  factory.printing.header = "";
          //  factory.printing.footer = "";
          //  //print page without prompt
          //  factory.DoPrint(false);
          //  //restore user's info
          //  factory.printing.header = h;
          //  factory.printing.footer = f;
          //show the print button
          // document.all("prnButton").style.visibility = 'visible';
          //  document.all("AButton").style.visibility = 'visible';
          //  document.all("BButton").style.visibility = 'visible';
          document.all("cmdPrint").style.visibility = 'visible';
      }
   
    </script>


 <br> <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" >
  <tr>
    <td>

<table width="700" border="0" align="center" cellpadding="0" cellspacing="2">
  <tr>
    <td>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
     <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td rowspan="2" valign="bottom"><table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
           <tr>
             <td align="center" class="Page_Header">ใบจ่ายยา</td>
           </tr>
             <tr>
             <td align="center" class="Page_Header">************************</td>
           </tr>
           <tr>
             <td align="center" class="Page_Header"> <asp:Label ID="lblLocationName" runat="server" Text=""></asp:Label></td>
           </tr>
        
         </table></td>
         
       </tr>
       
     </table></td>
     </tr>
   <tr>
     <td align="center" >&nbsp;</td>
     </tr>
</table>
    
<table width="100%" border="0" cellpadding="0" cellspacing="0" >
<tr>  
        <td width="100" class="texttopic">เลขที่</td>
        <td >
            <asp:Label ID="lblRefillNo" runat="server"></asp:Label>
        </td>
        <td width="100" class="texttopic">วันที่</td>
        <td>
            <asp:Label ID="lblDate" runat="server"></asp:Label>
        </td>       
</tr> 
<tr>      
      
        <td class="texttopic">PID</td>
        <td ><asp:Label ID="lblPatientID" runat="server"  CssClass="patientinfo"></asp:Label>        </td>
        <td class="texttopic">ชื่อ-นามสกุล</td>
        <td>            <asp:Label ID="lblName" runat="server" CssClass="patientinfo"></asp:Label>        </td>       
      </tr> 
                <tr> 
        <td class="texttopic">เพศ</td>
        <td>
            <asp:Label ID="lblGender" runat="server"  CssClass="patientinfo"></asp:Label>
        </td>
        <td class="texttopic" width="40">อายุ</td>
        <td>
            <asp:Label ID="lblAges" runat="server"  CssClass="patientinfo"></asp:Label>
        </td>
       
      </tr>  
                <tr>      
      
        <td class="texttopic">เลขบัตร ปชช.</td>
        <td >
         <asp:Label ID="lblCardID" runat="server"  CssClass="patientinfo"></asp:Label></td>
        <td class="texttopic">เบอร์โทร </td>
      <td><asp:Label ID="lblTel" runat="server"  CssClass="patientinfo"></asp:Label></td>

      </tr>  
                <tr>      
      
        <td class="texttopic">สิทธิ</td>
        <td colspan="3" >
            <asp:Label ID="lblClaim" runat="server"  CssClass="patientinfo"></asp:Label></td>

      </tr>  
                <tr>      
      
        <td class="texttopic">ที่อยู่</td>
        <td colspan="3" >
            <asp:Label ID="lblAddress" runat="server"  CssClass="patientinfo"></asp:Label></td>

      </tr>  
            </table>
       
        </td>
    </tr>
</table>


    <table width="100%" border="0" cellpadding="0" cellspacing="0">  
        <tr>
        <td  valign="bottom">&nbsp; </td>
      </tr>
        <tr>
        <td  valign="bottom">รายการยา</td>
      </tr>
       
        <tr>
        <td  valign="bottom">
                    <asp:GridView ID="grdDrugRefill" runat="server" AutoGenerateColumns="False" CellPadding="3" GridLines="Horizontal" Width="100%" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px">
                        <RowStyle BackColor="#E7E7FF" VerticalAlign="Top" ForeColor="#4A3C8C" />
                        <columns>
                            <asp:BoundField DataField="nRow" HeaderText="No">
                            <HeaderStyle HorizontalAlign="Center" />
                            <itemstyle HorizontalAlign="Center" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DrugName" HeaderText="ยา" >
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="QTY" HeaderText="จำนวน">
                            </asp:BoundField>
                            <asp:BoundField DataField="UOM" HeaderText="หน่วย">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </columns>
                        <footerstyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                        <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Right" BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                        <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" BackColor="#4A3C8C" ForeColor="#F7F7F7" />
                        <AlternatingRowStyle BackColor="#F7F7F7" />
                        <SortedAscendingCellStyle BackColor="#F4F4FD" />
                        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                        <SortedDescendingCellStyle BackColor="#D8D8F0" />
                        <SortedDescendingHeaderStyle BackColor="#3E3277" />
                    </asp:GridView>
            </td>
      </tr>
     
      <tr>
        <td  valign="bottom">&nbsp; </td>
      </tr>
      <tr>
        <td valign="bottom"><table border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td class="texttopic" >(ลงชื่อ)</td> 
            <td width="250" class="TextInputUnderline" >&nbsp;</td>
            </tr>
          
          

          <tr>
            <td class="texttopic" >&nbsp;</td> 
            <td width="250"  align="center" >(
                <asp:Label ID="lblUser" runat="server"></asp:Label>
&nbsp;)</td>
            </tr>
          
          

          <tr>
            <td class="texttopic" >&nbsp;</td> 
            <td align="center" >ผู้จ่ายยา</td>
            </tr>
          
          

        </table></td>
      </tr>
      <tr>
        <td valign="bottom"></td>
      </tr>
    </table></td>
  </tr>
  
   
  <tr>
   <td align="center"> 
       <input  type="button" value="Print" id="cmdPrint"  onclick="doprint();" /> </td>
  </tr>
</table>
</table>  
    
    </td>
  </tr>
</table>
</form>
</body>
</html>