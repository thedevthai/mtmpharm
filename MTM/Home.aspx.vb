﻿Imports Rajchasi

Public Class Homes
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlUser As New UserController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        LoadUserOnline()

    End Sub
    'Private Sub LoadVisitCount()
    '    dt = ctlUser.UserLogFile_GetVisitCount()
    '    grdVisitor.DataSource = dt
    '    grdVisitor.DataBind()

    'End Sub
    Private Sub LoadUserOnline()
        lblUserOnlineCount.Text = Application("OnlineNow")

        dt = ctlUser.GetUsers_Online("MTM")
        lblOnline.Text = "ผู้ใช้งานล่าสุด : "

        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1

                Select Case i Mod 4
                    Case 0
                        lblOnline.Text &= " <span class='label label-success'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & "</span>"
                    Case 1
                        lblOnline.Text &= " <span class='label label-info'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & "</span>"
                    Case 2
                        lblOnline.Text &= " <span class='label label-danger'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & "</span>"
                    Case 3
                        lblOnline.Text &= " <span class='label label-warning'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & "</span>"
                    Case Else
                        lblOnline.Text &= " <span class='label label-success'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & "</span>"
                End Select
                'If i Mod 2 = 0 Then
                '    lblOnline.Text &= " <span class='label label-success'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("Name") & "</span>"
                '    'lblOnline.Text &= "<font color=#1878ca>" & "[" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("Name") & "]" & "</font>"
                'Else
                '    lblOnline.Text &= " <span class='label label-warning'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("Name") & "</span>"
                'End If

            Next

        Else
            lblOnline.Text = "ไม่มี User Online ในขณะนี้"
        End If

    End Sub


End Class