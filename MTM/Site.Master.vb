﻿Imports Rajchasi
Public Class Site
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If


        If Not IsPostBack Then
            Dim ctlS As New SystemConfigController
            lblVersion.Text = ctlS.SystemConfig_GetByCode("Version")
        End If
        hlnkUserName.Text = Session("NameOfUser")
        If Session("RoleID") = isShopAccess Then
            hlnkUserName.NavigateUrl = "LocationsEdit.aspx?id=" & Session("LocationID")
        Else
            hlnkUserName.NavigateUrl = "#"
        End If

    End Sub

End Class