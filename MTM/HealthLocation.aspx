﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="HealthLocation.aspx.vb" Inherits=".HealthLocation" %>

<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
      
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>สถานพยาบาล/หน่วยบริการสาธารณสุข/โรงพยาบาล
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">PCU</li>
      </ol>
    </section>

<section class="content">  
   <div class="row">
   <section class="col-lg-12 connectedSortable"> 
    <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">สถานพยาบาล/หน่วยบริการสาธารณสุข/โรงพยาบาล</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">



           
            
<table width="99%" border="0" cellPadding="1" cellSpacing="1">

                                                <tr>
                                                    <td>รหัสสถานพยาบาล</td>
                                                    <td > <asp:TextBox ID="txtCode" runat="server" BackColor="#FFFF91" 
                                                                  Font-Bold="True" ForeColor="#0033CC" MaxLength="9"></asp:TextBox>
                                                         <asp:HiddenField ID="hdPCUID" runat="server" />
                                                          </td>
                                                  <td>&nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>

                                                <tr>
                                                    <td>ชื่อสถานพยาบาล</td>
                                                    <td  colspan="3"> <asp:TextBox ID="txtName" runat="server" Width="90%" ></asp:TextBox>
                                                          </td>
                                               
                                                </tr>
                                                <tr>
                                                    <td>ประเภท</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlType"  CssClass="OptionControl"  runat="server">
                                                           
                                                                     </asp:DropDownList>                                                    
                                                    </td>
                                                  <td>ระดับบริการ</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlLevel"  CssClass="OptionControl"  runat="server">
                                                           
                                                                     </asp:DropDownList>                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>กระทรวง</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlMinistry" runat="server"  CssClass="OptionControl"  AutoPostBack="True">
                                                           
                                                        </asp:DropDownList>                                                    </td>
                                                  <td>กรม</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlDepartment" runat="server"  CssClass="OptionControl"  AutoPostBack="True">
                                                           
                                                        </asp:DropDownList>                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>ที่อยู่ </td>
                                                    <td colspan="3"><asp:TextBox ID="txtAddress" runat="server" Width="90%"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>จังหวัด</td>
                                                    <td><asp:DropDownList CssClass="OptionControl" 
                                                            ID="ddlProvince" runat="server" AutoPostBack="True"> </asp:DropDownList></td>
                                                    <td   >รหัสไปรษณีย์:</td>
                                                    <td   >
                                                        <asp:TextBox ID="txtZipCode" runat="server" 
                                                         MaxLength="5"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>โทรศัพท์ :                                                    </td>
                                                    <td><table  border="0" cellspacing="2" cellpadding="0">
                                                          <tr>
                                                            <td width="210"><asp:TextBox ID="txtTel" runat="server" Width="200px"></asp:TextBox></td>
                                                            <td width="60" >โทรสาร :</td>
                                                            <td><asp:TextBox ID="txtFax" runat="server" Width="200px"></asp:TextBox></td>
                                                          </tr>
                                                        </table></td>
                                                    <td>E-mail :</td>
                                              <td><asp:TextBox ID="txtMail" runat="server" 
                                                      Width="200px"></asp:TextBox></td>
          </tr>
                                                <tr>
                                                  <td>จำนวนเตียง</td>
                                                  <td><asp:TextBox ID="txtBed" runat="server" MaxLength="4"></asp:TextBox></td>
                                                       <td>รหัสพื้นที่บริการ</td>
                                                       <td><asp:TextBox ID="txtAreaCode" runat="server" Width="200px"></asp:TextBox></td>
          </tr>
                                                <tr>
                                                  <td>Status :</td>
                                                  <td >
                                                      <asp:CheckBox ID="chkStatus" runat="server" Checked="True" Text="Public" /></td>
                                                  <td>รหัส รพ.แม่ข่าย</td>
                                                  <td><asp:TextBox ID="txtParentCode" runat="server" MaxLength="4"></asp:TextBox></td>
                                                </tr>
                                                
     
  </table>     

                 <asp:Panel ID="pnAlert" runat="server">
                  <div class="alert alert-danger alert-dismissible">
                
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <p><asp:Label ID="lblValidate" runat="server"></asp:Label></p>
              </div>
                   </asp:Panel>
                 
     
     </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
</section>
  </div>

    <div align="center">
         <asp:Button ID="cmdSave" runat="server" CssClass="buttonSave" Text="บันทึก" Width="100px" />
&nbsp;<asp:Button ID="cmdClear" runat="server" CssClass="buttonCancle" Text="ยกเลิก" Width="100px" />
    </div>


  <div class="row">
   <section class="col-lg-12 connectedSortable"> 
           <div class="box box-solid box-success">
            <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">รายการสถานพยาบาล/หน่วยบริการสาธารณสุข/โรงพยาบาล</h3>             
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>                 
            </div>
            <div class="box-body">
             <table border="0" cellspacing="1" cellpadding="0">
            <tr>
              <td width="50" >ค้นหา</td>
              <td >
                  <asp:TextBox ID="txtSearch" runat="server" Width="200px"></asp:TextBox>                </td>
              <td >
                  <asp:Button ID="cmdFind" runat="server" CssClass="buttonFind" Text="ค้นหา" Width="100px" />
                </td>
            </tr>
            
          </table>

              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No." DataField="nRow">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
            <asp:BoundField DataField="Code" HeaderText="รหัส">
              <headerstyle HorizontalAlign="Center" />          
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
            <asp:BoundField DataField="Name" HeaderText="ชื่อหน่วย">
              <headerstyle HorizontalAlign="Left" />          
              <itemstyle HorizontalAlign="Left" />                      </asp:BoundField>
                <asp:BoundField DataField="ProvinceName" HeaderText="จังหวัด">
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="TypeName" HeaderText="ประเภทหน่วย" />
                <asp:BoundField DataField="LevelName" HeaderText="ระดับบริการ" />
                <asp:BoundField DataField="DivisionName" HeaderText="กระทรวง">
                </asp:BoundField>
                <asp:BoundField DataField="DepartmentName" HeaderText="กรม" />
            <asp:TemplateField HeaderText="Public">
              <itemtemplate>
                <asp:Image ID="imgStatus" runat="server" ImageUrl="images/icon-ok.png" Visible='<%# DataBinder.Eval(Container.DataItem, "IsPublic") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Code") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgDel" runat="server" 
                                    ImageUrl="images/delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Code") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
      </tr>
                </table>
            </div>            
          </div>
   </section>
  </div>
</section></asp:Content>
