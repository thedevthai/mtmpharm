﻿Imports Rajchasi
Public Class EMR
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlUser As New UserController
    Dim ctlP As New PatientController
    Dim ctlL As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlMTM As New MTMController
    'Dim ctlH As New HomeVisitController
    'Dim ServiceDate As Long
    Dim sAlert As String
    Dim isValid As Boolean
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        'LoadUserOnline()
        'LoadVisitCount()


        If Request("PatientID") Is Nothing Then
            If Session("patientid") Is Nothing Or Session("patientid") = 0 Then
                Response.Redirect("PatientSearch.aspx")
            End If
        Else
            Session("patientid") = Request("PatientID")
        End If

        If Not IsPostBack Then
            isAdd = True
            imgSearchICD.Visible = False
            pnRefer.Visible = False

            'txtBYear.Text = Right(txtServiceDate.Text, 4)

            'If StrNull2Zero(txtBYear.Text) < 2500 Then
            '    txtBYear.Text = StrNull2Zero(txtBYear.Text) + 543
            'Else
            '    txtBYear.Text = StrNull2Zero(txtBYear.Text)
            'End If
            LoadPatientMedical()
            LoadVisit()
            LoadDrugUse()
            LoadDrugProblemToGrid()
            LoadDeseaseRelateToGrid()
            LoadDrugRefillToGrid()
            LoadHospitalRefer()
            LoadDocumentList()
            LoadReferData()
        End If

        cmdEditMedical.Attributes.Add("onclick", "javascript:void(window.open('PatientMedical.aspx',null,'scrollbars=1,width=650,HEIGHT=550'));")

        imgSearchICD.Attributes.Add("onclick", "javascript:void(window.open('ICD10Search.aspx?p=icd10',null,'scrollbars=1,width=650,HEIGHT=550'));")
    End Sub
    Private Sub LoadReferData()
        Dim ctlR As New ReferController

        dt = ctlR.Refer_GetByPatient(Session("PatientID"))
        If dt.Rows.Count > 0 Then

            txtReasonRefer.Text = dt.Rows(0)("ReasonRefer")
            cboHospital.Value = DBNull2Str(dt.Rows(0)("HospitalUID"))
            cmdPrintRefer.Visible = True
        Else
            cmdPrintRefer.Visible = False
        End If
    End Sub

    Private Sub LoadHospitalRefer()
        Dim ctlH As New HospitalController
        dt = ctlH.Hospital_GetByStatus("A")
        cboHospital.DataSource = dt
        cboHospital.ValueField = "HospitalUID"
        cboHospital.TextField = "HospitalName"
        cboHospital.DataBind()
    End Sub

    Private Sub LoadPatientMedical()
        Dim ctlP As New PatientController
        dt = ctlP.Patient_GetMedicalHistory(Session("patientid"))
        If dt.Rows.Count > 0 Then
            lblAlcohol.Text = String.Concat(dt.Rows(0)("Alcohol"))
            lblCigarette.Text = String.Concat(dt.Rows(0)("Smoking"))
            lblAllergy.Text = String.Concat(dt.Rows(0)("Allergy"))
        End If

        dt = Nothing
    End Sub

    Private Sub LoadDocumentList()
        dt = ctlMTM.DocumentUpload_GetByLocation(Session("LocationID"), StrNull2Zero(Session("PatientID")))
        grdDocument.DataSource = dt
        grdDocument.DataBind()
    End Sub

    'Private Sub LoadVisitCount()
    '    dt = ctlUser.UserLogFile_GetVisitCount()
    '    grdVisitor.DataSource = dt
    '    grdVisitor.DataBind()

    'End Sub
    Private Sub LoadDrugRefillToGrid()
        dt = ctlMTM.DrugRefill_GetByLocation(Session("LocationID"), StrNull2Zero(Session("patientid")))
        grdDrugRefill.DataSource = dt
        grdDrugRefill.DataBind()
    End Sub
    Private Sub LoadLocation()

        dt = ctlL.Location_GetByID(Session("LocationID"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)

            End With
        End If
    End Sub

    'Private Sub LoadMTMHerb()
    '    Dim dtH As New DataTable

    '    dtH = ctlP.Master_GetHerbLastInfo(StrNull2Zero(Session("patientid")))
    '    If dtH.Rows.Count > 0 Then
    '        With dtH.Rows(0)
    '            txtDrug1.Text = String.Concat(.Item("MedicationUsed1"))
    '            txtDrug2.Text = String.Concat(.Item("MedicationUsed2"))
    '            txtDrug3.Text = String.Concat(.Item("MedicationUsed3"))
    '            txtDrug4.Text = String.Concat(.Item("MedicationUsed4"))
    '        End With
    '    End If

    'End Sub


#Region "Load Data to Grid"
    Private Sub LoadDeseaseRelateToGrid()
        If Session("RoleID") = isShopAccess Then
            dt = ctlMTM.MTM_Desease_Get(StrNull2Zero(Session("patientid")), Session("LocationID"))
        Else
            dt = ctlMTM.MTM_Desease_Get(StrNull2Zero(Session("patientid")))
        End If

        grdDesease.DataSource = dt
        grdDesease.DataBind()
    End Sub
    Private Sub LoadDrugProblemToGrid()
        dt = ctlMTM.MTM_DrugProblem_GetByUser(StrNull2Zero(Session("patientid")), Session("UserID"))
        grdProblem.DataSource = dt
        grdProblem.DataBind()
    End Sub

#End Region
#Region "Grid Event"
    Protected Sub grdDrugProblem_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdProblem.RowDataBound
        'If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
        '    Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
        '    Dim imgD As Image = e.Row.Cells(7).FindControl("imgDel_D")
        '    imgD.Attributes.Add("onClick", scriptString)
        'End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub
    Private Sub grdDrugProblem_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdProblem.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit_D"

                Case "imgDel_D"
                    ctlMTM.MTM_DrugProblem_Delete(StrNull2Zero(e.CommandArgument()))
                    LoadDrugProblemToGrid()
            End Select
        End If
    End Sub
    Protected Sub grdDesease_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDesease.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(3).FindControl("imgDel_DS")
            imgD.Attributes.Add("onClick", scriptString)

            e.Row.Cells(0).Text = e.Row.RowIndex + 1
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub

    Private Sub grdDesease_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDesease.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel_DS"
                    ctlMTM.MTM_DeseaseRelate_Delete(e.CommandArgument())
                    LoadDeseaseRelateToGrid()
            End Select
        End If
    End Sub

#End Region
    Private Sub LoadVisit()
        dt = ctlMTM.MTM_GetVisit(StrNull2Zero(Session("patientid")), Session("UserID"))
        grdVisit.DataSource = dt
        grdVisit.DataBind()
    End Sub

    Private Sub LoadDrugUse()
        If Session("RoleID") = isShopAccess Then
            dt = ctlP.DrugUse_Get(StrNull2Zero(Session("patientid")), Session("LocationID"))
        Else
            dt = ctlP.DrugUse_Get(StrNull2Zero(Session("patientid")))
        End If
        grdDrugUse.DataSource = dt
        grdDrugUse.DataBind()
    End Sub

    Protected Sub grdVisit_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdVisit.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgVisit"
                    Response.Redirect("MTM.aspx?t=edit&fid=" & e.CommandArgument())
            End Select
        End If
    End Sub

    Protected Sub grdVisit_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdVisit.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub
    Protected Sub cmdNewVisit_Click(sender As Object, e As ImageClickEventArgs) Handles cmdNewVisit.Click
        Response.Redirect("MTM.aspx?t=new")
    End Sub
    Protected Sub cmdDrugDispend_Click(sender As Object, e As ImageClickEventArgs) Handles cmdDrugDispend.Click
        Response.Redirect("DrugRefill.aspx")
    End Sub
    Protected Sub cmdAddRefer_Click(sender As Object, e As ImageClickEventArgs) Handles cmdAddRefer.Click
        Response.Redirect("Refer.aspx")
    End Sub

    Protected Sub cmdUpload_Click(sender As Object, e As ImageClickEventArgs) Handles cmdUpload.Click
        Response.Redirect("Document.aspx")
    End Sub

    Protected Sub cmdEditMedical_Click(sender As Object, e As ImageClickEventArgs) Handles cmdEditMedical.Click
        'ClientScript.RegisterStartupScript(Me.GetType(), "pop", "PatientMedical.aspx", True)

    End Sub

    Protected Sub grdDrugUse_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDrugUse.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(3).FindControl("imgDel_DU")
            imgD.Attributes.Add("onClick", scriptString)

            e.Row.Cells(0).Text = e.Row.RowIndex + 1
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub

    Protected Sub grdDrugUse_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDrugUse.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel_DU"
                    ctlP.DrugUse_Delete(e.CommandArgument())
                    LoadDrugUse()
            End Select
        End If
    End Sub

    Protected Sub grdDocument_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDocument.RowCommand

        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel_Doc"
                    ctlMTM.DocumentUpload_Delete(e.CommandArgument())
                    LoadDocumentList()
            End Select
        End If
    End Sub

    Protected Sub grdDocument_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDocument.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบเอกสารนี้ใช่หรือไม่?"");"
            Dim imgD As Image = e.Row.Cells(2).FindControl("imgDel_Doc")
            imgD.Attributes.Add("onClick", scriptString)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub

    Private Sub grdDrugRefill_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDrugRefill.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub

    Dim ctlICD As New DeseaseController
    Protected Sub cmdAddDesease_Click(sender As Object, e As EventArgs) Handles cmdAddDesease.Click
        If txtICD10Code.Text = "" Then
            DisplayMessage(Me.Page, "ระบุโรคที่ต้องการเพิ่มก่อน")
            Exit Sub
        End If

        'Dim strD() As String
        'strD = Split(txtICD10Code.Text, ":")

        'If Not ctlICD.Desease_IsHas(strD(0).TrimEnd().ToUpper()) Then
        '    DisplayMessage(Me.Page, "กรุณาตรวจสอบโรคที่เพิ่มก่อน")
        '    Exit Sub
        'End If

        ctlP.PatientDesease_Add(Session("PatientID"), txtICD10Code.Text, txtDeseaseOther.Text, Session("patientid"), Session("userid"))
        LoadDeseaseRelateToGrid()
        txtICD10Code.Text = ""
        txtDeseaseOther.Text = ""
    End Sub

    Protected Sub txtICD10Code_TextChanged(sender As Object, e As EventArgs) Handles txtICD10Code.TextChanged
        'LoadICD10Name()
    End Sub
    Private Sub LoadICD10Name()
        If txtICD10Code.Text <> "" Then
            Dim strD() As String
            strD = Split(txtICD10Code.Text, ":")
            txtICD10Code.Text = ctlICD.Desease_GetName(strD(0).TrimEnd().ToUpper())
        End If
    End Sub

    Protected Sub cmdPrintRefer_Click(sender As Object, e As EventArgs) Handles cmdPrintRefer.Click
        Response.Write("<script>javascript:void(window.open('Reports/printRefer.aspx?pid=" & Session("patientid") & "',null,'scrollbars=1,width=650,HEIGHT=550'));</script>")
    End Sub

    Protected Sub cmdRefer_Click(sender As Object, e As EventArgs) Handles cmdRefer.Click

        If cboHospital.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','กรุณาระบุสถานพยาบาลที่ต้องการส่งต่อ');", True)
            Exit Sub
        End If

        If txtReasonRefer.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','กรุณาระบุเหตุผล');", True)
            Exit Sub
        End If
        Dim ctlR As New ReferController

        If Not ctlR.Refer_CheckDup(Session("PatientID")) Then
            ctlR.Refer_Add(ConvertDateToString(ctlR.GET_DATE_SERVER()), Session("PatientID"), cboHospital.Value, Session("LocationID"), txtReasonRefer.Text, Session("UserID"))
        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','ผู้รับบริการรายนี้ได้ทำการส่งต่อแล้ว');", True)
            Exit Sub
        End If

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','บันทึกข้อมูลการส่งต่อเรียบร้อย');", True)

    End Sub
End Class