﻿Imports System.Net
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports System.IO
Imports System.Text

Public Class ReportViewer
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If
        UpdateProgress1.DisplayAfter = 0
        UpdateProgress1.Visible = True
        If Not IsPostBack Then
            LoadReport()
        End If
        'Response.AppendHeader("content-disposition", "attachment;filename=xxx.pdf")
        'Response.Charset = ""
        'Response.ContentType = "application/vnd.ms-pdf"
    End Sub
    Private Sub LoadReport()

        'System.Threading.Thread.Sleep(1000)
        'UpdateProgress1.Visible = True


        Dim credential As New ReportServerCredentials
        ReportViewer1.Reset()
        ReportViewer1.ServerReport.ReportServerCredentials = credential
        ReportViewer1.ServerReport.ReportServerUrl = credential.ReportServerUrl
        ReportViewer1.ShowPrintButton = True

        'Me.ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout)

        'System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("th-TH")
        Dim xParam As New List(Of ReportParameter)

        Select Case FagRPT
            Case "LocationGroup"
                'ReportViewer1.ServerReport.ReportPath = credential.ReportPath("LocationByGroup")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("LocationDetail")
                xParam.Add(New ReportParameter("ProjectID", "1"))
            Case "LocationProject"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportsName)
                xParam.Add(New ReportParameter("ProjectID", "1"))
            Case "PatientSummaryCount"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("PatientSummaryCount")
                xParam.Add(New ReportParameter("BYear", Request("y")))
            Case "SmokingPatientByForm"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("SmokingPatientByForm")
                xParam.Add(New ReportParameter("BYear", Request("y")))

            Case "SummaryServiceCount"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("SummaryServiceCount")
                xParam.Add(New ReportParameter("StartDate", ConvertStrDate2DBString(Request("b"))))
                xParam.Add(New ReportParameter("EndDate", ConvertStrDate2DBString(Request("e"))))
                xParam.Add(New ReportParameter("ProjectID", Request("pj")))
            Case "MTM"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportsName)
                xParam.Add(New ReportParameter("StartDate", ConvertStrDate2DBString(Request("b"))))
                xParam.Add(New ReportParameter("EndDate", ConvertStrDate2DBString(Request("e"))))
                Reportskey = "XLS"
            Case "DR"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportsName)
                xParam.Add(New ReportParameter("StartDate", ConvertStrDate2DBString(Request("b"))))
                xParam.Add(New ReportParameter("EndDate", ConvertStrDate2DBString(Request("e"))))
                xParam.Add(New ReportParameter("ProvinceID", Request("p")))
                Reportskey = "XLS"
            Case "fnc", "cus"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportsName)
                xParam.Add(New ReportParameter("StartDate", ConvertStrDate2DBString(Request("b"))))
                xParam.Add(New ReportParameter("EndDate", ConvertStrDate2DBString(Request("e"))))
                xParam.Add(New ReportParameter("LocationID", Session("LocationID").ToString()))
                Reportskey = "XLS"
        End Select

        ReportViewer1.ServerReport.SetParameters(xParam)

        If Reportskey <> "XLS" Then
            ' Variables
            Dim warnings As Warning()
            Dim streamIds As String()
            Dim mimeType As String = String.Empty
            Dim encoding As String = String.Empty
            Dim extension As String = String.Empty

            ' Setup the report viewer object and get the array of bytes

            Dim bytes As Byte() = ReportViewer1.ServerReport.Render("PDF", Nothing, mimeType, encoding, extension, streamIds, Nothing)

            ' Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType
            'Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=C:\ttt.pdf"))
            Response.BinaryWrite(bytes)
            ' create the file
            'Response.Flush()
            ' send it to the client to download
        Else
            ' Variables
            Dim warnings As Warning()
            Dim streamIds As String()
            Dim mimeType As String = String.Empty
            Dim encoding As String = String.Empty
            Dim extension As String = String.Empty


            ' Setup the report viewer object and get the array of bytes

            Dim bytes As Byte() = ReportViewer1.ServerReport.Render("EXCEL", Nothing, mimeType, encoding, extension, streamIds, Nothing)

            ' Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType
            Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=MTM_" & FagRPT & "_" & ReportsName & "." & extension))
            Response.BinaryWrite(bytes)
            ' create the file
            Response.Flush()
            ' send it to the client to download
        End If

    End Sub


End Class