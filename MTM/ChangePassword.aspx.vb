﻿Imports Rajchasi
Public Class ChangePassword
    Inherits System.Web.UI.Page

    Dim acc As New UserController
    Dim enc As New CryptographyEngine

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            pnResult.Visible = False
            pnForm.Visible = True
        End If

    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If Session("Password") <> txtOld.Text Then

            ' ModalPopupExtender.Show()
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','รหัสผ่านเดิมท่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง');", True)

            Exit Sub
        End If
        acc.User_ChangePassword(Session("username"), enc.EncryptString(txtNew.Text, True))

        acc.User_GenLogfile(Session("Username"), ACTTYPE_UPD, "Users", "Change password", "")
        'ModalPopupExtender.Show()

        ' DisplayMessage(Me.Page, "ระบบเปลี่ยนรหัสผ่านให้ท่านใหม่เรียบร้อยแล้ว")

        pnForm.Visible = False
        pnResult.Visible = True
    End Sub
End Class