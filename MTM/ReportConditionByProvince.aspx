﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReportConditionByProvince.aspx.vb" Inherits=".ReportConditionByProvince" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1> <asp:Label ID="lblReportHeader" runat="server" Text="รายงาน"></asp:Label>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
         
  <div class="box box-success">
            <div class="box-header">
              <i class="ion ion-medkit"></i>

              <h3 class="box-title">เลือกเงื่อนไขรายงาน</h3>
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
                 
            </div>
            <div class="box-body">          
          <table border="0" align="center" width="100%"  cellpadding="0" cellspacing="3">
      <tr>
    <td  width="100" >
        <asp:Label ID="lblProv" runat="server" Text="จังหวัด"></asp:Label>      </td>
    <td colspan="3" align="left">
        <asp:DropDownList ID="ddlProvince" runat="server" AutoPostBack="True" CssClass="OptionControl">        </asp:DropDownList>      </td>
    </tr>
  <tr>
    <td>ตั้งแต่</td>
    <td  width="100" >
        <asp:TextBox ID="txtStartDate" runat="server" Width="120px"></asp:TextBox>
          </td>
    <td Width="30px" align="center">ถึง</td>
    <td>
        <asp:TextBox ID="txtEndDate"  runat="server"></asp:TextBox>    </td>
  </tr>
      <tr>
                                            <td>
                                                <asp:Label ID="lblLocation" runat="server" Text="ร้านยา"></asp:Label>                                            </td>
                                            <td align="left" colspan="3">   
                                                        <dx:ASPxComboBox ID="ddlLocation" runat="server" CssClass="OptionControl2" EnableTheming="True" Theme="MetropolisBlue" ToolTip="พิมพ์ค้นหาได้" Width="95%">
                                                        </dx:ASPxComboBox>
                                                </td>
    </tr>
  
      <tr>
                                            <td>
                                                &nbsp;</td>
                                            <td align="left" colspan="3">
                                                <asp:Button ID="cmdPrint" runat="server" Text="ดูรายงาน" CssClass="buttonSave" Width="100px" />
        &nbsp;</td>
    </tr>
  
</table>
      
       
         
                   
            </div>
</div>
         
    </section>
</asp:Content>
