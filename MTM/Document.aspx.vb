﻿Imports System.IO
Imports Rajchasi
Public Class Document
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlUser As New UserController

    Dim ctlL As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlMTM As New MTMController
    Dim sAlert As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        'LoadUserOnline()
        'LoadVisitCount()


        If Session("patientid") Is Nothing And Request("PatientID") Is Nothing Then
            Response.Redirect("PatientSearch.aspx")
        Else
            If Session("patientid") Is Nothing Then
                Session("patientid") = Request("PatientID")
            End If
        End If
        If Not IsPostBack Then
            isAdd = True
            pnDocumentAlert.Visible = False
            LoadDocumentList()
        End If
    End Sub
    Private Sub LoadDocumentList()
        dt = ctlMTM.DocumentUpload_Get(StrNull2Zero(Session("PatientID")))
        grdDocument.DataSource = dt
        grdDocument.DataBind()
    End Sub
    Protected Sub cmdUploadFile_Click(sender As Object, e As EventArgs) Handles cmdUploadFile.Click
        UploadFile(FileUpload1)
        LoadDocumentList()

        txtDesc.Text = ""
    End Sub
    Dim sFile As String = ""
    Sub UploadFile(ByVal Fileupload As Object)
        Dim FileFullName As String = Fileupload.PostedFile.FileName
        Dim FileNameInfo As String = Path.GetFileName(FileFullName)
        Dim filename As String = Path.GetFileName(Fileupload.PostedFile.FileName)
        Dim objfile As FileInfo = New FileInfo(Server.MapPath(DocumentUpload))
        If FileNameInfo <> "" Then
            'Fileupload.PostedFile.SaveAs(objfile.DirectoryName & "\" & filename)
            sFile = Session("patientid") & ConvertDate2DBString(ctlMTM.GET_DATE_SERVER()) & ConvertTimeToString(Now()) & Path.GetExtension(FileFullName)
            Fileupload.PostedFile.SaveAs(objfile.DirectoryName & "\" & DocumentUpload & "\" & sFile)

            ctlMTM.DocumentUpload_SaveByLocation(Session("patientid"), txtDesc.Text, sFile, Session("userid"), Session("LocationID"))
        End If
        objfile = Nothing
    End Sub
    Private Sub grdDocument_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDocument.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบเอกสารนี้ใช่หรือไม่?"");"
            Dim imgD As Image = e.Row.Cells(2).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Private Sub grdDocument_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDocument.RowCommand

        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel"
                    ctlMTM.DocumentUpload_Delete(e.CommandArgument())
                    LoadDocumentList()
            End Select
        End If
    End Sub


End Class