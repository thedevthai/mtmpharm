﻿Imports Rajchasi
Public Class DrugRefill
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlUser As New UserController

    Dim ctlL As New LocationController
    Dim ctlD As New DrugController
    Dim ctlMTM As New MTMController
    Dim ctlP As New PatientController
    'Dim ServiceDate As Long
    Dim sAlert As String
    Dim dtPOS As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        'LoadUserOnline()
        'LoadVisitCount()

        If Session("patientid") Is Nothing And Request("PatientID") Is Nothing Then
            Response.Redirect("PatientSearch.aspx")
        Else
            If Session("patientid") Is Nothing Then
                Session("patientid") = Request("PatientID")
            End If
        End If
        If Not IsPostBack Then
            isAdd = True
            txtDate.Text = DisplayShortDateTH(Now.Date())
            hdUID.Value = -1
            hdRefillHeaderUID.Value = 0
            pnDrugRefillAlert.Visible = False
            cmdPrint.Visible = False
            cmdSave.Visible = False

            dtPOS.Columns.Clear()

            dtPOS = ctlP.DrugUse_Get4Refill(StrNull2Zero(Session("patientid")), Session("LocationID"))

            'dtPOS.Columns.Add("UID")
            dtPOS.Columns.Add("DrugRefillHeaderUID")
            'dtPOS.Columns.Add("TMTID")
            'dtPOS.Columns.Add("DrugUID")
            dtPOS.Columns.Add("QTY")
            dtPOS.Columns.Add("UOM")
            dtPOS.Columns.Add("UsedRemark")
            dtPOS.Columns.Add("ServiceDate")
            'dtPOS.Columns.Add("DrugName")
            dtPOS.Columns.Add("RefillDate")
            Session("drugrefill") = dtPOS


            'ctlMTM.DrugRefillItem_Save()

            LoadDrugRefill()

        End If

        imgSearchDrug.Attributes.Add("onclick", "javascript:Void(window.open('DrugSearch.aspx?p=drug',null,'scrollbars=1,width=650,HEIGHT=550'));")

        'cmdPrint.Attributes.Add("onclick", "javascript:void(window.open('printRefill.aspx',null,'scrollbars=1,width=650,HEIGHT=550'));")

    End Sub

    Private Sub LoadDrugRefill()
        'dt = ctlMTM.DrugRefill_GetByDate(ConvertStrDate2DBString(txtDate.Text), StrNull2Zero(Session("patientid")))
        If dtPOS.Rows.Count > 0 Then
            grdDrugRefill.DataSource = dtPOS
            grdDrugRefill.DataBind()
            grdDrugRefill.Visible = True
            cmdSave.Visible = True
        Else
            grdDrugRefill.DataSource = Nothing
            cmdSave.Visible = False
        End If
    End Sub



    Private Sub grdDrugRefill_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDrugRefill.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(6).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub

    Private Sub grdDrugRefill_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDrugRefill.RowCommand

        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(StrNull2Zero(e.CommandArgument()))
                Case "imgDel"
                    'ctlMTM.DrugRefill_Delete(e.CommandArgument())
                    dtPOS = Session("drugrefill")
                    dtPOS.Rows(e.CommandArgument).Delete()
                    dtPOS.AcceptChanges()
                    Session("drugrefill") = Nothing
                    Session("drugrefill") = dtPOS

                    grdDrugRefill.DataSource = Nothing
                    LoadDrugRefill()
            End Select
        End If
    End Sub

    Private Sub EditData(pUID As Integer)
        'dt = ctlMTM.DrugRefill_GetByUID(pUID)
        dtPOS = Session("drugrefill")

        If dtPOS.Rows.Count > 0 Then
            With dtPOS.Rows(pUID)
                hdUID.Value = pUID
                txtQTYRefill.Text = String.Concat(.Item("QTY"))
                txtDrug.Text = ctlD.Drug_GetNameByUID(.Item("DrugUID"))
                txtUOMRefill.Text = String.Concat(.Item("UOM"))
                txtRefillDesc.Text = String.Concat(.Item("Descriptions"))
                txtDate.Text = DisplayShortDateTH(.Item("RefillDate"))
            End With

        End If
    End Sub

    Protected Sub cmdDrugRefill_Click(sender As Object, e As EventArgs) Handles cmdDrugRefill.Click
        If txtDrug.Text = "" Then
            DisplayMessage(Me.Page, "กรุณาเลือกยาที่ใช้ก่อน")
            Exit Sub
        End If
        Dim strD() As String
        Dim iDrugUID As Integer = 0

        strD = Split(txtDrug.Text, ":")
        iDrugUID = ctlD.Drug_GetUIDByCode(strD(0).TrimEnd())

        If Not ctlD.Drug_IsHas(strD(0).TrimEnd()) Then
            DisplayMessage(Me.Page, "กรุณาตรวจสอบยาที่เพิ่มก่อน")
            Exit Sub
        End If

        'If StrNull2Zero(hdRefillHeaderUID.Value) = 0 Then
        '    ctlMTM.DrugRefillHeader_Add(ConvertStrDate2InformDateString(txtDate.Text), Session("patientid"), Session("locationid"), txtRefillDesc.Text, 0, Session("username"))
        '    hdRefillHeaderUID.Value = ctlMTM.DrugRefillHeader_GetUID(ConvertStrDate2InformDateString(txtDate.Text), Session("patientid"), Session("locationid"))
        'End If

        'ctlMTM.DrugRefillItem_Save(StrNull2Zero(hdUID.Value.ToString()), StrNull2Zero(hdRefillHeaderUID.Value.ToString()), iDrugUID, txtQTYRefill.Text, txtUOMRefill.Text, txtRefillDesc.Text, Session("username"))

        '----------------------------------------
        dtPOS = Session("drugrefill")

        If hdUID.Value <> -1 Then
            Dim dr As DataRow = dtPOS.Rows(hdUID.Value)
            dr("PatientID") = Session("PatientID")
            dr("QTY") = txtQTYRefill.Text
            dr("UOM") = txtUOMRefill.Text
            dr("DrugUID") = iDrugUID
            dr("TMTID") = strD(0)
            dr("Descriptions") = txtRefillDesc.Text
            dr("SoundexName") = ""
            dr("LocationID") = Session("LocationID")
            dr("DrugName") = strD(1)
            dr("RefillDate") = txtDate.Text
            dr("UID") = hdUID.Value
            dr("DrugRefillHeaderUID") = 0
            dr("UsedRemark") = txtRefillDesc.Text
            dr.AcceptChanges()
        Else
            Dim dr As DataRow = dtPOS.NewRow()
            dr("PatientID") = Session("PatientID")
            dr("QTY") = txtQTYRefill.Text
            dr("UOM") = txtUOMRefill.Text
            dr("DrugUID") = iDrugUID
            dr("TMTID") = strD(0)
            dr("Descriptions") = txtRefillDesc.Text
            dr("SoundexName") = ""
            dr("LocationID") = Session("LocationID")
            dr("DrugName") = strD(1)
            dr("RefillDate") = txtDate.Text
            dr("UID") = dtPOS.Rows.Count
            dr("DrugRefillHeaderUID") = 0
            dr("UsedRemark") = txtRefillDesc.Text

            dtPOS.Rows.Add(dr)
            dtPOS.AcceptChanges()
        End If

        Session("drugrefill") = Nothing
        Session("drugrefill") = dtPOS

        '--------------------------------------

        LoadDrugRefill()

        hdUID.Value = -1
        txtQTYRefill.Text = ""
        txtDrug.Text = ""
        txtUOMRefill.Text = ""
        txtRefillDesc.Text = ""

    End Sub

    Protected Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        hdUID.Value = -1
        txtQTYRefill.Text = ""
        txtDrug.Text = ""
        txtUOMRefill.Text = ""
        txtRefillDesc.Text = ""
    End Sub

    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click
        'ClientScript.RegisterStartupScript(Me.GetType(), "pop", "Report/printRefill.aspx?id=" & hdRefillHeaderUID.Value.ToString(), True)

        Response.Write("<script>javascript: Void(window.open('Reports/printRefill.aspx?id=" & hdRefillHeaderUID.Value.ToString() & "',null,'scrollbars=1,width=650,HEIGHT=550'));</script>")
    End Sub

    Private Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click


        If StrNull2Zero(hdRefillHeaderUID.Value) = 0 Then
            ctlMTM.DrugRefillHeader_Add(ConvertStrDate2InformDateString(txtDate.Text), Session("patientid"), Session("locationid"), txtRefillDesc.Text, 0, Session("username"))
            hdRefillHeaderUID.Value = ctlMTM.DrugRefillHeader_GetUID(ConvertStrDate2InformDateString(txtDate.Text), Session("patientid"), Session("locationid"))
        End If

        ctlMTM.DrugRefillItem_Delete(hdRefillHeaderUID.Value)

        dtPOS = Session("drugrefill")
        For i = 0 To dtPOS.Rows.Count - 1
            With dtPOS.Rows(i)
                If .RowState <> DataRowState.Deleted Then
                    If DBNull2Zero(.Item("QTY")) = 0 Then
                        DisplayMessage(Me.Page, "กรุณาระบุจำนวนที่จ่าย/หน่วย พร้อมรายละเอียดวิธีการกิน/ใช้ ให้ครบถ้วน แล้วกดบันทึกอีกครั้ง")
                        Exit Sub
                    End If

                    ctlMTM.DrugRefillItem_SaveRefill(StrNull2Zero(hdRefillHeaderUID.Value.ToString()), .Item("DrugUID"), .Item("QTY"), .Item("UOM"), .Item("UsedRemark"), Session("username"))
                End If
            End With
        Next

        cmdPrint.Visible = True
        DisplayMessage(Me.Page, "บันทึกรายการจ่ายยาเรียบร้อย")

    End Sub
End Class