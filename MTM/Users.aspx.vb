﻿Imports Rajchasi
Public Class Users
    Inherits System.Web.UI.Page
     
    Dim dt As New DataTable
  
    Dim ctlUser As New UserController
    Dim ctlRole As New RoleController   
    Dim objLct As New LocationInfo
    Dim ctlLct As New LocationController
    Dim objEn As New CryptographyEngine

    Dim objRole As New RoleInfo   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            grdData.PageIndex = 0

            txtUsername.ReadOnly = False
            lblID.Text = 0
            LoadProvinceGroupToDDL()
            LoadUserAccountToGrid()
            LoadRolsToCheckList()
            LoadLocationToDDL()
            txtFindLocation.Visible = False
            ' txtFindPreceptor.visible = False
            ddlLocation.Visible = False
           
            lnkFindLocation.Visible = False
            '  lnkFindPreceptor.visible = False
          
            lblLocation.Visible = False
            imgArrowLocation.Visible = False

            lblRPTRole.Visible = False
            ddlRPTRole.Visible = False
            lblMNGRole.Visible = False
            'ddlProjectRole.Visible = False
            ClearData()

        End If
    End Sub

    Private Sub LoadRolsToCheckList()
        dt = ctlRole.GetRoles
        With optRoles
            .Visible = True
            .DataSource = dt
            .DataTextField = "RoleName"
            .DataValueField = "RoleID"
            .DataBind()
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub LoadLocationToDDL()
        If txtFindLocation.Text = "" Then
            dt = ctlLct.Location_Get
        Else
            dt = ctlLct.Location_GetBySearch(txtFindLocation.Text)
        End If

        ddlLocation.Items.Clear()

        If dt.Rows.Count > 0 Then
            With ddlLocation
                .Visible = True
                .Items.Add("---เลือกร้านยา---")
                .Items(0).Value = 0
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)(objLct.tblField(objLct.fldPos.f01_LocationName).fldName))
                    .Items(i + 1).Value = dt.Rows(i)(objLct.tblField(objLct.fldPos.f00_LocationID).fldName)
                Next
                .SelectedIndex = 0
            End With
        End If

    End Sub
    Private Sub LoadPCUToDDL()
        Dim ctlP As New PCUController
        If txtFindLocation.Text = "" Then
            dt = ctlP.PCU_Get
        Else
            dt = ctlP.PCU_GetBySearch("0", txtFindLocation.Text)
        End If

        ddlLocation.Items.Clear()

        If dt.Rows.Count > 0 Then
            With ddlLocation
                .Visible = True
                .Items.Add("---เลือกหน่วยบริการ---")
                .Items(0).Value = 0
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("Name"))
                    .Items(i + 1).Value = dt.Rows(i)("Code")
                Next
                .SelectedIndex = 0
            End With
        End If

    End Sub

    Private Sub LoadUserAccountToGrid()

        dt = ctlUser.GetUsers_ByGroupSearch(1, ddlRoleFind.SelectedValue, txtSearch.Text)

        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With
        Else
            grdData.DataSource = Nothing
            grdData.Visible = False
        End If
        dt = Nothing

    End Sub
    Protected Function validateData() As Boolean
        Dim result As Boolean = True
        lblvalidate1.Text = ""
        lblvalidate1.Text = ""
        lblvalidate1.Text = ""


        If lblID.Text = 0 Then

            If ddlUserProfile.SelectedIndex = 0 Then
                result = False
                lblvalidate1.Text = "กรุณาเลือกกลุ่มผู้ใช้"
                lblvalidate1.Visible = True
            Else
                Select Case ddlUserProfile.SelectedValue
                    Case 1 'Shop
                        If ddlLocation.SelectedValue = "0" Then
                            result = False
                            lblvalidate1.Text = "กรุณาเลือกร้านยา"
                            lblvalidate1.Visible = True
                        End If

                    Case 2
                        If ddlLocation.SelectedValue = "0" Then
                            result = False
                            lblvalidate1.Text = "กรุณาเลือกหน่วยบริการ"
                            lblvalidate1.Visible = True
                        End If
                End Select
            End If

        End If

        If txtUsername.Text = "" Or txtPassword.Text = "" Then
            result = False
            lblvalidate2.Text = "กรุณากรอก Username และ Password ก่อน"
            lblvalidate2.Visible = True

        Else

            If lblID.Text = "0" Then
                dt = ctlUser.GetUsers_ByUsername(txtUsername.Text)
                If dt.Rows.Count > 0 Then
                    result = False
                    If ddlUserProfile.SelectedValue = 1 Then
                        lblvalidate2.Text = "ร้านยานี้มี Username ในระบบแล้ว"
                    Else
                        lblvalidate2.Text = "มี Username นี้ในระบบแล้ว กรุณาตั้ง Username ใหม่"
                    End If

                    lblvalidate2.Visible = True
                    imgAlert.Visible = True
                Else
                    imgAlert.Visible = False
                End If

            End If

        End If


        'If chkProject.Items(0).Selected = False And chkProject.Items(1).Selected = False Then
        '    lblvalidate3.Text = "เลือกโครงการที่จะให้สิทธิ์ก่อน"
        '    lblvalidate3.Visible = True
        'End If


        Dim n As Integer = 0
        For i = 0 To optRoles.Items.Count - 1
            If optRoles.Items(i).Selected Then
                n = n + 1
            End If
        Next


        If n = 0 Then
            lblvalidate3.Text &= vbCrLf & "กรุณากำหนดสิทธิ์ให้ผู้ใช้ก่อน"
            lblvalidate3.Visible = True
        End If


        Return result
    End Function


    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSave.Click

        'UserID = ctlUser.GetUsersID_ByUsername(txtUsername.Text)
        If validateData() Then

            Dim item, MngProj As Integer
            Dim sLocationID, RPTGRP As String
            Dim UserProfile As Integer = 0

            Dim status As Integer = 0
            If chkStatus.Checked Then
                status = 1
            End If

            If ddlUserProfile.SelectedValue = 1 Then
                sLocationID = ddlLocation.SelectedValue
                UserProfile = 1
            ElseIf ddlUserProfile.SelectedValue = 2 Then
                UserProfile = 2
            Else
                UserProfile = 3
                sLocationID = LOCATID_MTM
            End If

            If optRoles.SelectedValue = isPCU Then
                MngProj = 1 ' ddlProjectRole.SelectedValue
            ElseIf optRoles.SelectedValue = isReportViewerAccess Then
                MngProj = 1 ' ddlProjectRole.SelectedValue
            Else
                MngProj = 0
            End If

            If optRoles.SelectedValue = isReportViewerAccess Then
                RPTGRP = ddlRPTRole.SelectedValue
            Else
                RPTGRP = ""
            End If

            If lblID.Text = 0 Then

                item = ctlUser.User_Add(txtUsername.Text, objEn.EncryptString(txtPassword.Text, True), txtFirstName.Text, txtLastName.Text, 0, status, optRoles.SelectedValue, sLocationID, RPTGRP, MngProj, UserProfile)

                ctlUser.User_GenLogfile(Session("Username"), ACTTYPE_ADD, "Users", "add new user :" & txtUsername.Text, "Result:" & item)
            Else
                item = ctlUser.User_Update(txtUsername.Text, objEn.EncryptString(txtPassword.Text, True), txtFirstName.Text, txtLastName.Text, 0, status, optRoles.SelectedValue, sLocationID, RPTGRP, MngProj)

                ctlUser.User_GenLogfile(Session("Username"), ACTTYPE_UPD, "Users", "update user :" & txtUsername.Text, "Result:" & item)

            End If

            'ctlUser.User_AddLocationProject(1, sLocationID, "A")

            'For i = 0 To chkProject.Items.Count - 1

            '    If optRoles.SelectedValue = isReportViewerAccess Then
            '        ctlUser.User_AddLocationProject(i + 1, txtUsername.Text, Boolean2StatusFlag(chkProject.Items(i).Selected))
            '    Else
            '        ctlUser.User_AddLocationProject(i + 1, sLocationID, Boolean2StatusFlag(chkProject.Items(i).Selected))
            '    End If
            'Next



            'If item = 1 Then
            DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")
            LoadUserAccountToGrid()
            ClearData()
            'End If

        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง!');", True)
        End If
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadUserAccountToGrid()
    End Sub
    Private Sub grdData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand

        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If ctlUser.User_Delete(e.CommandArgument) Then

                        ctlUser.User_GenLogfile(Session("Username"), "DEL", "User", "ลบ user :" & txtUsername.Text, "")

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบข้อมูลเรียบร้อย');", True)
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If

                    LoadUserAccountToGrid()

            End Select

        End If

    End Sub
    Private Sub EditData(ByVal pID As String)
        ClearData()
        dt = ctlUser.User_GetByID(StrNull2Zero(pID))

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False

                Me.lblID.Text = .Item("UserID")
                txtUsername.Text = .Item("Username")
                Me.txtFirstName.Text = String.Concat(.Item("FirstName"))
                txtLastName.Text = String.Concat(.Item("LastName"))

                If DBNull2Zero(.Item("RoleID")) = 1 Then
                    ddlUserProfile.SelectedValue = 1

                ElseIf DBNull2Zero(.Item("RoleID")) = 2 Then
                    ddlUserProfile.SelectedValue = 2
                Else
                    ddlUserProfile.SelectedValue = 3
                End If

                txtFindLocation.Visible = False

                txtFirstName.Enabled = True
                txtLastName.Enabled = True
                txtFirstName.BackColor = Drawing.Color.AliceBlue
                txtLastName.BackColor = Drawing.Color.AliceBlue

                ddlLocation.Items.Clear()
                Select Case DBNull2Zero(.Item("RoleID"))
                    Case 1 'Shop
                        lnkFindLocation.Visible = True
                        lblLocation.Visible = True
                        imgArrowLocation.Visible = True
                        txtFindLocation.Visible = True
                        LoadLocationToDDL()
                        ddlLocation.Visible = True
                        ddlLocation.SelectedValue = .Item("LocationID")
                    Case Else
                        ddlLocation.Visible = False
                        txtFindLocation.Visible = False
                        lnkFindLocation.Visible = False
                        lblLocation.Visible = False
                        imgArrowLocation.Visible = False
                End Select

                optRoles.SelectedValue = DBNull2Zero(.Item("RoleID"))
                ddlUserProfile.Enabled = False

                lblRPTRole.Visible = False
                ddlRPTRole.Visible = False
                lblMNGRole.Visible = False
                'ddlProjectRole.Visible = False

                If DBNull2Zero(.Item("RoleID")) = isReportViewerAccess Then
                    ddlRPTRole.SelectedValue = DBNull2Str(.Item("ReportGroup"))
                    lblRPTRole.Visible = True
                    ddlRPTRole.Visible = True
                ElseIf DBNull2Zero(.Item("RoleID")) = isPCU Then
                    'ddlProjectRole.SelectedValue = DBNull2Str(.Item("ProjectManager"))
                    lblMNGRole.Visible = True
                    'ddlProjectRole.Visible = True
                Else
                    lblRPTRole.Visible = False
                    ddlRPTRole.Visible = False
                    lblMNGRole.Visible = False
                    'ddlProjectRole.Visible = False
                End If

                If .Item("isPublic") = 0 Then
                    chkStatus.Checked = False
                Else
                    chkStatus.Checked = True
                End If

                txtPassword.Text = objEn.DecryptString(.Item("Password"), True)
            End With
            txtUsername.ReadOnly = True

            'chkProject.ClearSelection()
            'Dim dtLP As New DataTable

            'If optRoles.SelectedValue <> isShopAccess Then
            '    dtLP = ctlUser.LocationProject_GetByUsername(txtUsername.Text)
            'Else
            '    dtLP = ctlUser.LocationProject_GetByLocationID(ddlLocation.SelectedValue)
            'End If

            'If dtLP.Rows.Count > 0 Then
            '    For i = 0 To dtLP.Rows.Count - 1
            '        Select Case dtLP.Rows(i)("ProjectID")
            '            Case "1"
            '                chkProject.Items(0).Selected = True
            '            Case "2"
            '                chkProject.Items(1).Selected = True
            '            Case "3"
            '                chkProject.Items(2).Selected = True
            '        End Select
            '    Next
            'End If
            'dtLP = Nothing
        End If

        dt = Nothing
    End Sub

    Protected Sub cmdClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdClear.Click
        ClearData()
        ddlUserProfile.SelectedIndex = 0
    End Sub

    Private Sub ClearData()
        Me.txtUsername.Text = ""
        txtPassword.Text = ""
        Me.txtFirstName.Text = ""
        txtLastName.Text = ""
        lblID.Text = 0
        chkStatus.Checked = True
        txtUsername.ReadOnly = False
        isAdd = True
        imgAlert.Visible = False

        lblvalidate1.Visible = False
        lblvalidate2.Visible = False
        lblvalidate3.Visible = False

        txtFindLocation.Visible = False

        ddlUserProfile.Enabled = True
        ddlLocation.Visible = False

        lnkFindLocation.Visible = False

        lblLocation.Visible = False
        imgArrowLocation.Visible = False

        txtFirstName.Enabled = True
        txtLastName.Enabled = True
        txtUsername.Enabled = True

        txtFirstName.BackColor = Drawing.Color.White
        txtLastName.BackColor = Drawing.Color.White
        txtUsername.BackColor = Drawing.Color.White

    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(8).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If


    End Sub

    Protected Sub ddlUserGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserProfile.SelectedIndexChanged

        ClearData()

        txtFindLocation.Visible = False

        txtFirstName.BackColor = Drawing.Color.AliceBlue
        txtLastName.BackColor = Drawing.Color.AliceBlue

        ddlLocation.Visible = False
        lnkFindLocation.Visible = False
        ddlLocation.Items.Clear()
        lblLocation.Visible = False
        imgArrowLocation.Visible = False
        Select Case ddlUserProfile.SelectedValue
            Case 1 'Shop
                LoadLocationToDDL()
                ddlLocation.Visible = True
                lnkFindLocation.Visible = True
                txtFindLocation.Visible = True
                lblLocation.Visible = True
                imgArrowLocation.Visible = True
                optRoles.SelectedValue = 1
            Case 3 'PCU
                LoadPCUToDDL()
                ddlLocation.Visible = True
                lnkFindLocation.Visible = True
                txtFindLocation.Visible = True
                lblLocation.Visible = True
                imgArrowLocation.Visible = True
                optRoles.SelectedValue = 5

            Case Else 'เจ้าหน้าที่อื่นๆ
                optRoles.SelectedValue = 3
                txtFirstName.Enabled = True
                txtLastName.Enabled = True
                txtFirstName.BackColor = Drawing.Color.White
                txtLastName.BackColor = Drawing.Color.White
        End Select
    End Sub

    Protected Sub lnkFindLocation_Click(sender As Object, e As EventArgs) Handles lnkFindLocation.Click
        LoadLocationToDDL()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadUserAccountToGrid()
    End Sub

    Protected Sub ddlGroupFind_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRoleFind.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadUserAccountToGrid()
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        txtUsername.Text = ddlLocation.SelectedValue
        txtFirstName.Text = ddlLocation.SelectedItem.Text

    End Sub

    Private Sub LoadProvinceGroupToDDL()
        dt = ctlLct.LoadProvinceGroup

        ddlRPTRole.Items.Clear()

        If dt.Rows.Count > 0 Then
            With ddlRPTRole
                .Visible = True
                .Items.Add("---ทั้งหมด---")
                .Items(0).Value = "ALL"
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("ProvinceGroupName"))
                    .Items(i + 1).Value = dt.Rows(i)("ProvinceGroupID")
                Next
                .SelectedIndex = 0
            End With
        End If

    End Sub

    Protected Sub optRoles_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRoles.SelectedIndexChanged
        lblRPTRole.Visible = False
        ddlRPTRole.Visible = False
        lblMNGRole.Visible = False
        'ddlProjectRole.Visible = False

        If optRoles.SelectedValue = isReportViewerAccess Then
            lblRPTRole.Visible = True
            ddlRPTRole.Visible = True
        ElseIf optRoles.SelectedValue = isPCU Then
            lblMNGRole.Visible = True
            'ddlProjectRole.Visible = True
        Else
            lblRPTRole.Visible = False
            ddlRPTRole.Visible = False
            lblMNGRole.Visible = False
            'ddlProjectRole.Visible = False
        End If
    End Sub
End Class

