<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PatientMedical.aspx.vb" Inherits=".PatientMedical" %>

<%@ Register Src="~/ucPatientInfo.ascx" TagPrefix="uc1" TagName="ucPatientInfo" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  
  <meta http-equiv="Content-Type" content="text/html; charset=windows-874" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title> Medication Therapy Management by Community Pharmacy</title>
  
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">  

  <link rel="stylesheet" type="text/css" href="css/mtmstyles.css">  
  <link rel="stylesheet" type="text/css" href="css/pagestyles.css"> 
  <link rel="stylesheet" type="text/css" href="css/button.css"> 

  <link rel="stylesheet" href="components/bootstrap/dist/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">  

      <link rel="icon" href="favicon.ico" type="image/x-icon"/>
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
     
<!-- JS -->
<script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>

</head>

<body class="hold-transition skin-blue sidebar-mini">
<form id="form1" name="form1"  runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<section class="wrapper">
 
  <header class="main-header">    
    <a href="#" class="logo">
    <span class="logo-mini"><b>MTM</b></span>
    <span class="logo-lg"><b>MTM</b>PHARM</span>
    </a>
    <nav class="navbar navbar-static-top">    
  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
        <div class="slk-header" >
            <span  class="header-full">Medication Therapy Management </span>
            <span  class="header-mid"> by Community Pharmacy </span>
            <span  class="header-mini"></span>
        </div>
    </nav>
  </header>
 
  <section class="contentCPA">   
    <!-- Main content -->
<section class="content">
    <div>
          <div class="row">
        <!-- Left col -->
        <section class="col-lg-6 connectedSortable">     
 
<div class="box box-success">
        <div class="box-header with-border">
             <i class="ion ion-heart-broken"></i>
          <h3 class="box-title">Patient Info.</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
<uc1:ucPatientInfo runat="server" ID="ucPatientInfo" />  
              </div>
     
</div>

<div class="box box-success">
        <div class="box-header with-border">
             <i class="ion ion-heart-broken"></i>
          <h3 class="box-title">�ĵԡ����آ�Ҿ</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
           

 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  
   
   <tr>
    <td><table border="0" cellpadding="0" cellspacing="2"  align="Left">
     <tr>
        <td>����ٺ������</td>
        <td>
            <table border="0" cellspacing="2" cellpadding="0">
              <tr>
                <td align="left"><asp:RadioButtonList ID="optSmoke" runat="server" 
                RepeatDirection="Horizontal" AutoPostBack="True" CellPadding="10" CellSpacing="4">
                  <asp:ListItem Value="5">��ԡ�ٺ����</asp:ListItem>
                  <asp:ListItem Value="4">�ٺ��Ш�</asp:ListItem>
                  <asp:ListItem Selected="True" Value="3">����ٺ</asp:ListItem>
                </asp:RadioButtonList></td>
                <td>
                  
                            <asp:TextBox ID="txtCGNo" runat="server" Width="40px"></asp:TextBox>
&nbsp;<asp:Label ID="lblCGday" runat="server" Text="�ǹ/�ѹ"></asp:Label>
            &nbsp;<asp:TextBox ID="txtCGYear" runat="server" Width="40px"></asp:TextBox>
&nbsp;<asp:Label ID="lblCGYear" runat="server" Text="��"></asp:Label>
                       
                </td>
               
                </tr>
            </table>       

        </td>
      </tr>
     <tr>
        <td></td>
        <td>  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td> <asp:Label ID="lblCgType" runat="server" Text="��Դ�ͧ���������ٺ"></asp:Label></td>
                      <td> <asp:RadioButtonList ID="optCigarette" runat="server" 
                RepeatDirection="Horizontal">
                  <asp:ListItem Value="10">�ǹ�ͧ</asp:ListItem>
                  <asp:ListItem Selected="True" Value="11">������ͧ</asp:ListItem>
                  <asp:ListItem Value="12">������俿��</asp:ListItem>
                
                  </asp:RadioButtonList></td>
                    </tr>
                  </table>
           </td>
      </tr>
      <tr>
        <td>��ô�����š�����</td>
        <td align="left"><table border="0" cellspacing="2" cellpadding="0">
              <tr>
                <td align="left">
                    <asp:RadioButtonList ID="optAlcohol" runat="server" 
                RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="9">������Ш�</asp:ListItem>
                     <asp:ListItem Value="8">�������駤���</asp:ListItem>
                  <asp:ListItem Value="7">�´�������ԡ����</asp:ListItem>
                  <asp:ListItem Selected="True" Value="6">������</asp:ListItem>
                </asp:RadioButtonList></td>
                <td align="left">                    
                            <asp:TextBox ID="txtAlcoholFQ" runat="server" Width="50px"></asp:TextBox>
                  &nbsp;<asp:Label ID="lblAlcohol" runat="server" Text=" ����/�ѻ����"></asp:Label>
                                          </td>
              </tr>
            </table>
            </td>
      </tr>
 
     
      <tr>
        <td>&nbsp;</td>
        <td align="left">
         
 
            <asp:Button ID="cmdSave" runat="server" CssClass="buttonSave" Text="�ѹ�֡" Width="100px" />
         
 
            </td>
      </tr>
 
     
      </table></td> 
  </tr>
 </table>
  </div>
     
</div>

<div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">����ѵ�����</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">   
          <table align="left" class="table">
        
              <tr>
                  <td width="100"> �����������</td>
                 
                  <td colspan="4"> 
                      <asp:RadioButtonList ID="optAllergy" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="N">�����</asp:ListItem>
                          <asp:ListItem Value="Y">��</asp:ListItem>
                      </asp:RadioButtonList>
                  </td>
                 
              </tr>
              
             
              
             
               <tr>
                  <td>
                      �кت�����</td>
                 
                  <td>
                      <asp:TextBox ID="txtDrugAllergy" runat="server"   Width="100%" MaxLength="1000"></asp:TextBox>
                   </td>
                 
                  <td align="center" width="70px">�ҡ���ʴ�</td>
                 
                  <td>
                      <asp:TextBox ID="txtSymptom" runat="server"   Width="100%" MaxLength="1000"></asp:TextBox>
                   </td>
                 
                  <td width="35px">
                      <asp:ImageButton ID="imgAddAllergy" runat="server" ImageUrl="images/add.png" />
                   </td>
                 
              </tr>                 
              <tr>
                  <td> </td>
                 
                  <td colspan="4">
                      �ó�����Һ������ �ô�к�����������ѡ���ҡ�� / �ä����</td>
                 
              </tr>
              
             
              <tr>
                  <td> ��¡������</td>
                 
                  <td colspan="4">
                              <asp:GridView ID="grdAllergy" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                                  <RowStyle BackColor="White" VerticalAlign="Top" />
                                  <columns>
                                      <asp:BoundField DataField="nRow" HeaderText="No">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="DrugAllergy" HeaderText="��/������� �����">
                                      <headerstyle HorizontalAlign="Left" />
                                      <itemstyle HorizontalAlign="Left" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="Symptom" HeaderText="�ҡ����" >
                                      <HeaderStyle HorizontalAlign="Left" />
                                      </asp:BoundField>
                                      <asp:TemplateField HeaderText="ź">
                                          <ItemTemplate>
                                              <asp:ImageButton ID="imgDel" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" ImageUrl="images/delete.png" />
                                          </ItemTemplate>
                                          <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                      </asp:TemplateField>
                                  </columns>
                                  <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                  <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                                  <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                  <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                                  <EditRowStyle BackColor="#2461BF" />
                                  <AlternatingRowStyle BackColor="#F7F7F7" />
                              </asp:GridView>
                          </td>               
               
                 
              </tr>
              
             
        </table>
    
      </div>
</div>

</section>

              <div align="center"><asp:Button ID="cmdOK" runat="server" CssClass="btnCss" Text="Close" Width="100px" /></div>

              </div>
 
    </div>

</section>
 </section>
 </section> 
    </form>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {

            $('.btnCss').click(function () {

                var scripts = new Array();                    

                $('.cssList').each(function () {
                    if ($(this).attr("selected", true)) {
                        scripts.push($(this).val());                         
                    }
                });               

                //window.opener.getListItems(scripts, pages); 
                 window.opener.location.reload(); 
                window.close();
            });
        });

    </script>
</body>
</html>
