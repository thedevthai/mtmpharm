﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web

Public Class ReportData
    Public Sub New()
        Me.ReportParameters = New List(Of Parameter)()
        Me.DataParameters = New List(Of Parameter)()
    End Sub
    Public Property IsLocal() As Boolean
        Get
            Return m_IsLocal
        End Get
        Set(value As Boolean)
            m_IsLocal = value
        End Set
    End Property
    Private m_IsLocal As Boolean
    Public Property ReportName() As String
        Get
            Return m_ReportName
        End Get
        Set(value As String)
            m_ReportName = value
        End Set
    End Property
    Private m_ReportName As String
    Public Property ReportParameters() As List(Of Parameter)
        Get
            Return m_ReportParameters
        End Get
        Set(value As List(Of Parameter))
            m_ReportParameters = value
        End Set
    End Property
    Private m_ReportParameters As List(Of Parameter)
    Public Property DataParameters() As List(Of Parameter)
        Get
            Return m_DataParameters
        End Get
        Set(value As List(Of Parameter))
            m_DataParameters = value
        End Set
    End Property
    Private m_DataParameters As List(Of Parameter)
End Class
Public Class Parameter
    Public Property ParameterName() As String
        Get
            Return m_ParameterName
        End Get
        Set(value As String)
            m_ParameterName = value
        End Set
    End Property
    Private m_ParameterName As String
    Public Property Value() As String
        Get
            Return m_Value
        End Get
        Set(value As String)
            m_Value = value
        End Set
    End Property
    Private m_Value As String
End Class