﻿Public Class SiteSimple
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not Request("PatientID") Is Nothing Then
            Session("patientid") = Request("PatientID")
        End If
        If Session("patientid") Is Nothing Then
            Response.Redirect("PatientSearch.aspx")
        End If


        If Not IsPostBack Then

        End If
        hlnkUserName.Text = Session("NameOfUser")
        If Session("RoleID") = isShopAccess Then
            hlnkUserName.NavigateUrl = "LocationsEdit.aspx?id=" & Session("LocationID")
        Else
            hlnkUserName.NavigateUrl = "#"
        End If

    End Sub

End Class