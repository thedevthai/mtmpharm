﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Homes.aspx.vb" Inherits=".Homes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/mtmstyles.css">

    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server"> 
        <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-8 connectedSortable">     

   <div class="box box-success">
            <div class="box-header">
              <i class="ion ion-document"></i>

              <h3 class="box-title">ดาวน์โหลด</h3>
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
                 
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                
                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                          <tr>
                            <td width="50" align="center"><img src="images/excel.png" width="20" height="20" /></td>
                            <td><a href="http://www.mtmpharm.com/Docs/ThaiHealthLocation.xlsx" target="_blank">รายชื่อสถานพยาบาลในประเทศ</a></td>
                            <td width="50" align="center">&nbsp;</td>
                            <td></td>
                            <td rowspan="2" align="right" valign="top">
                             </td>
                          </tr>
                          
                        </table>
                                 
            </div>
            <!-- /.box-body -->
            
          </div>
            
  <div class="box box-success">
            <div class="box-header">
              <i class="ion ion-bookmark"></i>

              <h3 class="box-title">คู่มือการใช้งาน</h3>
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
                 
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                
                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                 <tr>
                            <td width="50" align="center"><img src="images/youtube.png" height="20" /></td>
                            <td><a href="https://youtu.be/pWxUpudweM4" target="_blank">VDO วิธีการใช้งานโปรแกรม MTMPharm</a></td>
                            <td width="50" align="center">&nbsp;</td>
                            <td></td>
                            <td rowspan="2" align="right" valign="top">
                             </td>
                          </tr>
                          <tr>
                            <td width="50" align="center"><img src="images/pdf.png" width="20" height="20" /></td>
                            <td><a href="http://www.mtmpharm.com/Docs/UserManual.pdf" target="_blank">คู่มือการใช้งานสำหรับร้านยา</a></td>
                            <td width="50" align="center">&nbsp;</td>
                            <td></td>
                            <td rowspan="2" align="right" valign="top">
                             </td>
                          </tr>
                          <tr>
                            <td align="center"><img src="images/ppt_icon.png" width="20" height="20" /></td>
                            <td><a href="http://www.mtmpharm.com/Docs/UserManual.pptx" target="_blank">Presentration การใช้งานสำหรับร้านยา</a></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                        </table>
                                 
            </div>
            <!-- /.box-body -->
            
          </div>
        

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-4 connectedSortable">
            <!--
               <div class="box box-danger">
            <div class="box-header">
              <i class="fa fa-users"></i>

              <h3 class="box-title"> Admin'Message : ประกาศจากผู้ดูแลระบบ</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body text-center">
            <h3 class="text-red">ขณะนี้ ผู้ดูแลระบบกำลังปรับปรุงระบบอยู่<br />
         เพื่อป้องกันข้อมูลสูญหาย ให้ท่านหลีกเลี่ยงการบันทึกแบบฟอร์มทุกโครงการ <br />แต่ท่านยังสามารถดูข้อมูลหรือรายงานในระบบได้ปกติ</h3><br />
      ท่านสามารถเข้ามาบันทึกข้อมูลได้อีกครั้ง ภายใน 10 นาที หรือ หลังจากข้อความประกาศนี้หายไป <br />
         ขออภัยในความไม่สะดวก

            </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
            -->
 
        
                <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-users"></i>

              <h3 class="box-title">User Online : <asp:Label ID="lblUserOnlineCount" runat="server" Text=""></asp:Label></h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
            <asp:Label ID="lblOnline" runat="server" Width="99%"></asp:Label>
            </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

              

        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->

  <!-- /.content-wrapper -->
  <div class="control-sidebar-bg"></div>
    
</asp:Content>
