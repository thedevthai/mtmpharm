﻿Imports Rajchasi
Public Class HealthLocation
    Inherits System.Web.UI.Page
    Dim ctlUser As New UserController
    Dim ctlL As New PCUController
    Dim dt As New DataTable
    Dim ds As New DataSet
    Dim objEn As New CryptographyEngine
    Dim objUser As New UserController
    Dim ctlRf As New ReferenceValueController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            grdData.PageIndex = 0

            hdPCUID.Value = ""
            LoadPCUToGrid()

            LoadProvinceToDDL()
            LoadTypeToDDL()
            LoadLevelToDDL()
            LoadMinistryToDDL()
            LoadDepartmentToDDL()
        End If

        txtZipCode.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
    End Sub
    Private Sub LoadPCUToGrid()
        If Trim(txtSearch.Text) <> "" Then
            dt = ctlL.PCU_GetBySearchAll("0", txtSearch.Text)
        Else
            dt = ctlL.PCU_GetAll
        End If

        With grdData
            .Visible = True
            .DataSource = dt
            .DataBind()
        End With

    End Sub

    Private Sub LoadProvinceToDDL()
        Dim ctlbase As New ApplicationBaseClass
        dt = ctlbase.LoadProvince
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadTypeToDDL()
        dt = ctlRf.ReferenceValue_GetByDomainCode("PCUTYPE")
        If dt.Rows.Count > 0 Then
            With ddlType
                .Enabled = True
                .DataSource = dt
                .DataTextField = "DisplayName"
                .DataValueField = "ValueCode"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadLevelToDDL()
        dt = ctlRf.ReferenceValue_GetByDomainCode("PCUTYPE")
        If dt.Rows.Count > 0 Then
            With ddlLevel
                .Enabled = True
                .DataSource = dt
                .DataTextField = "DisplayName"
                .DataValueField = "ValueCode"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadMinistryToDDL()
        dt = ctlRf.ReferenceValue_GetByDomainCode("PCUDIV")
        If dt.Rows.Count > 0 Then
            With ddlMinistry
                .Enabled = True
                .DataSource = dt
                .DataTextField = "DisplayName"
                .DataValueField = "ValueCode"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadDepartmentToDDL()
        dt = ctlRf.ReferenceValue_GetByDomainCode("PCUDEPT")
        If dt.Rows.Count > 0 Then
            With ddlDepartment
                .Enabled = True
                .DataSource = dt
                .DataTextField = "DisplayName"
                .DataValueField = "ValueCode"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadPCUToGrid()
    End Sub


    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If ctlL.PCU_Delete(e.CommandArgument) Then

                        objUser.User_GenLogfile(Session("Username"), ACTTYPE_DEL, "PCU", "ลบหน่วยบริการ:" & e.CommandArgument, "")
                        objUser.User_DeleteByUsername(e.CommandArgument)
                        objUser.User_GenLogfile(Session("Username"), "DEL", "User", "ลบ user :" & e.CommandArgument, "")

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบข้อมูลเรียบร้อย');", True)
                        LoadPCUToGrid()
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If


            End Select


        End If
    End Sub

    Private Sub EditData(ByVal pID As String)
        Dim dtE As New DataTable

        dtE = ctlL.PCU_GetByID(pID)
        If dtE.Rows.Count > 0 Then
            With dtE.Rows(0)
                isAdd = False

                Me.hdPCUID.Value = String.Concat(.Item("UID"))
                txtCode.Text = String.Concat(.Item("Code"))
                txtName.Text = String.Concat(.Item("Name"))
                ddlType.SelectedValue = String.Concat(.Item("TypeID"))
                ddlLevel.SelectedValue = String.Concat(.Item("LevelID"))
                ddlMinistry.SelectedValue = String.Concat(.Item("MinistryID"))
                ddlMinistry.SelectedValue = String.Concat(.Item("DepartmentID"))
                txtAddress.Text = String.Concat(.Item("Address"))
                ddlProvince.SelectedValue = String.Concat(.Item("ProvinceID"))
                Me.txtZipCode.Text = String.Concat(.Item("ZipCode"))
                txtTel.Text = String.Concat(.Item("Tel"))
                txtFax.Text = String.Concat(.Item("Fax"))
                txtBed.Text = String.Concat(.Item("Bed"))
                txtMail.Text = String.Concat(.Item("EMail"))
                chkStatus.Checked = ConvertStatusFlag2Boolean(.Item("StatusFlag"))
                txtAreaCode.Text = String.Concat(.Item("AreaCode"))
                txtParentCode.Text = String.Concat(.Item("ParentCode"))
            End With

        End If
        dtE = Nothing
    End Sub

    Protected Function validateData() As Boolean
        Dim result As Boolean = True
        lblValidate.Text = ""

        If ddlType.SelectedIndex = -1 Then
            result = False
            lblValidate.Text &= "- กรุณาเลือกประเภทหน่วยบริการ  <br />"
            lblValidate.Visible = True
        End If

        If txtCode.Text = "" Then
            result = False
            lblValidate.Text &= "- รหัสสถานพยาบาล  <br />"
            lblValidate.Visible = True
        End If

        If txtName.Text = "" Then
            result = False
            lblValidate.Text &= "- กรุณาระบุชื่อหน่วยบริการ  <br />"
            lblValidate.Visible = True

        End If
        Return result
    End Function

    Private Sub ClearData()
        Me.hdPCUID.Value = ""
        txtCode.Text = ""
        txtName.Text = ""
        txtAddress.Text = ""
        ddlProvince.SelectedIndex = 1
        Me.txtZipCode.Text = ""
        chkStatus.Checked = True
        txtBed.Text = ""
        txtAreaCode.Text = ""
        txtTel.Text = ""
        txtFax.Text = ""
        txtSearch.Text = ""
        txtParentCode.Text = ""

    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript: Return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(8).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If validateData() Then

            lblValidate.Visible = False

            Dim PCUID As Integer = 0
            Dim item As Integer
            PCUID = StrNull2Zero(hdPCUID.Value)

            If hdPCUID.Value = "" Then
                item = ctlL.PCU_Save(PCUID, txtCode.Text, txtName.Text, ddlType.SelectedValue, ddlMinistry.SelectedValue, ddlDepartment.SelectedValue, StrNull2Zero(txtBed.Text), txtAddress.Text, ddlProvince.SelectedValue, txtZipCode.Text, txtTel.Text, txtFax.Text, txtMail.Text, txtAreaCode.Text, Boolean2StatusFlag(chkStatus.Checked), StrNull2Zero(Session("UserID")))

                objUser.User_GenLogfile(Session("Username"), ACTTYPE_ADD, "PCU", "เพิ่มใหม่ หน่วยบริการ:" & txtName.Text, "")

                item = objUser.User_Add(txtCode.Text, objEn.EncryptString("1234", True), txtName.Text, ddlProvince.SelectedItem.Text, 0, 1, 2, txtCode.Text, "", 0, 2)

                objUser.User_GenLogfile(Session("Username"), ACTTYPE_ADD, "Users", "add new user :" & txtCode.Text, "Result:" & item)

            Else

                item = ctlL.PCU_Save(PCUID, txtCode.Text, txtName.Text, ddlType.SelectedValue, ddlMinistry.SelectedValue, ddlDepartment.SelectedValue, StrNull2Zero(txtBed.Text), txtAddress.Text, ddlProvince.SelectedValue, txtZipCode.Text, txtTel.Text, txtFax.Text, txtMail.Text, txtAreaCode.Text, Boolean2StatusFlag(chkStatus.Checked), StrNull2Zero(Session("UserID")))


                objUser.User_GenLogfile(Session("Username"), ACTTYPE_UPD, "PCUs", "แก้ไข หน่วยบริการ:" & txtName.Text, "")

            End If

            LoadPCUToGrid()
            ClearData()
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','บันทึกข้อมูลเรียบร้อย');", True)
        End If
    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadPCUToGrid()
    End Sub

End Class

