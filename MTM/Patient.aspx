﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Patient.aspx.vb" Inherits=".Patient" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<link rel="stylesheet" type="text/css" href="css/pagestyles.css">
<link rel="stylesheet" type="text/css" href="css/mtmstyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
<link rel="stylesheet" type="text/css" href="css/dc_table.css">
   

<script type="text/javascript">
        function autoTab(obj) {
            /* กำหนดรูปแบบข้อความโดยให้ _ แทนค่าอะไรก็ได้ แล้วตามด้วยเครื่องหมาย 
            หรือสัญลักษณ์ที่ใช้แบ่ง เช่นกำหนดเป็น รูปแบบเลขที่บัตรประชาชน 
            4-2215-54125-6-12 ก็สามารถกำหนดเป็น _-____-_____-_-__ 
            รูปแบบเบอร์โทรศัพท์ 08-4521-6521 กำหนดเป็น __-____-____ 
            หรือกำหนดเวลาเช่น 12:45:30 กำหนดเป็น __:__:__ 
            ตัวอย่างข้างล่างเป็นการกำหนดรูปแบบรหัสสินค้า
            รหัสสินค้า 11-BRID-Y1207 
            */
            var pattern = new String("_-____-_____-___"); // กำหนดรูปแบบในนี้ 
            var pattern_ex = new String("-"); // กำหนดสัญลักษณ์หรือเครื่องหมายที่ใช้แบ่งในนี้ 
            var returnText = new String("");
            var obj_l = obj.value.length;
            var obj_l2 = obj_l - 1;
            for (i = 0; i < pattern.length; i++) {
                if (obj_l2 == i && pattern.charAt(i + 1) == pattern_ex) {
                    returnText += obj.value + pattern_ex;
                    obj.value = returnText;
                }
            }
            if (obj_l >= pattern.length) {
                obj.value = obj.value.substr(0, pattern.length);
            }
        }
</script> 


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>ข้อมูลผู้รับบริการ
        <small>(Patient)</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
    
     <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        //$(document).ready(function () {

        //    $('.btnCssDrugRefill').click(function () {
        //        window.open('DrugSearch.aspx?p=drugrefill','Drug','height=400,width=600');
        //        return false;
        //    });

        //     $('.btnCssDrugRemain').click(function () {
        //        window.open('DrugSearch.aspx?p=drugremain','Drug','height=400,width=600');
        //        return false;
        //    });
          

        //     $('.btnCssICD').click(function () {
        //        window.open('ICD10Search.aspx?p=icd10','ICD10','height=400,width=600');
        //        return false;
        //    });

        //});

        function getListItems(items,pname) {
            if (pname == 'icd10') {
                $('.icd10').val(items);
            }           
            if (pname == 'drug') {
                $('.drug').val(items);
            }
            if (pname == 'druguse') {
                $('.druguse').val(items);
            }
            //$('.csstextbox').val(items);
        } 

    </script>


 <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-users"></i>

              <h3 class="box-title">ข้อมูลส่วนตัว</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"> 
   
  <tr>
    <td valign="top"><table width="100%" border="0" cellspacing="2" cellpadding="0">
     <tr>
    <td colspan="4"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="200" valign="top">Patien ID</td>
        <td align="left" valign="top" class="NameEN"> <asp:Label ID="lblPatientID" runat="server"></asp:Label></td>
        <td width="100" valign="top" class="texttopic">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        </tr>
      <tr>
        <td width="200" valign="top" class="texttopic">ชื่อ-นามสกุล<img src="images/star.png" width="10" height="10" /></td>
                       <td align="left" valign="top">
            <asp:TextBox ID="txtForeName" runat="server"></asp:TextBox>&nbsp;<asp:TextBox ID="txtSurname" runat="server"></asp:TextBox></td>
        <td width="100" valign="top" class="texttopic">เพศ</td>
        <td align="left"><asp:RadioButtonList ID="optGender" runat="server" RepeatDirection="Horizontal">
          <asp:ListItem Selected="True" Value="M">ชาย</asp:ListItem>
          <asp:ListItem Value="F">หญิง</asp:ListItem>
        </asp:RadioButtonList></td>
        </tr>
        <tr>
         <td valign="top" class="texttopic">วัน/เดือน/ปีเกิด</td>
        <td align="left" valign="top">
            <asp:TextBox ID="txtBirthDate" runat="server" AutoPostBack="True"></asp:TextBox>&nbsp;(วว/ดด/ปปปป{พ.ศ.})  
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                ControlToValidate="txtBirthDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
        <td valign="top" class="texttopic">อายุ<img src="images/star.png" width="10" height="10" /></td>
        <td valign="top"><asp:TextBox ID="txtAges" runat="server" Width="50px"></asp:TextBox> 
          &nbsp;ปี</td>
      </tr>
      <tr>
        <td valign="top" class="texttopic">เลขบัตร ปชช.</td>
        <td align="left" valign="top"><asp:TextBox ID="txtCardID" runat="server" 
                Width="200px" MaxLength="16"></asp:TextBox></td>
        <td valign="top" class="texttopic">&nbsp;</td>
        <td valign="top">
            &nbsp;</td>
        </tr>
      <tr>
        <td valign="top" class="texttopic">เบอร์โทรศัพท์บ้าน</td>
        <td align="left" valign="top" colspan="3">
            <table>
                <tr>
                    <td><asp:TextBox ID="txtTelephone" runat="server" Width="200px"></asp:TextBox></td>
                    <td>มือถือ<img src="images/star.png" width="10" height="10" /></td>
                    <td><asp:TextBox ID="txtMobile" runat="server" Width="200px"></asp:TextBox></td>
                    <td>เวลาที่สะดวกให้ติดต่อกลับ</td>
                    <td>
                        <asp:TextBox ID="txtTimeContact" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
          </td>
        </tr>
      <tr>
        <td valign="top" class="texttopic">ที่อยู่</td>
        <td colspan="3" align="left" valign="top"><asp:RadioButtonList ID="optAddressType" runat="server" 
                RepeatDirection="Horizontal" CellPadding="0" CellSpacing="0">
          <asp:ListItem Selected="True" Value="LIVE">ที่อยู่ปัจจุบัน</asp:ListItem>
          <asp:ListItem Value="CARD">ที่อยู่ตามบัตรประชาชน</asp:ListItem>
        </asp:RadioButtonList></td>
        </tr>
      <tr>
        <td valign="top" class="texttopic">บ้านเลขที่/หมู่บ้าน/อาคาร/ซอย</td>
        <td align="left" valign="top"><asp:TextBox ID="txtAddress" runat="server" Width="250px"></asp:TextBox></td>
        <td valign="top" class="texttopic">ถนน</td>
        <td valign="top"><asp:TextBox ID="txtRoad" runat="server" Width="200px"></asp:TextBox></td>
      </tr>
      <tr>
        <td valign="top" class="texttopic">ตำบล/แขวง</td>
        <td align="left" valign="top"><asp:TextBox ID="txtDistrict" runat="server" Width="250px"></asp:TextBox></td>
        <td valign="top" class="texttopic">อำเภอ/เขต</td>
        <td valign="top"><asp:TextBox ID="txtCity" runat="server" Width="200px"></asp:TextBox></td>
      </tr>
      <tr>
        <td valign="top" class="texttopic">จังหวัด</td>
        <td align="left" valign="top"><asp:DropDownList CssClass="OptionControl" ID="ddlProvince" runat="server" Width="250px"> </asp:DropDownList></td>
        <td valign="top" class="texttopic">รหัสไปรษณีย์</td>
        <td valign="top"><asp:TextBox ID="txtZipCode" runat="server" MaxLength="5"></asp:TextBox></td>
      </tr>
        <tr>
        <td valign="top" class="texttopic">ระดับการศึกษา</td>
        <td colspan="3" align="left" valign="top"><asp:RadioButtonList ID="optEducation" runat="server" RepeatDirection="Horizontal">
          <asp:ListItem Selected="True" Value="0">ไม่ระบุ</asp:ListItem>
<asp:ListItem Value="1">ไม่ได้ศึกษา</asp:ListItem>
          <asp:ListItem Value="2">ประถมศึกษา</asp:ListItem>
          <asp:ListItem Value="3">มัธยมศึกษาตอนต้น</asp:ListItem>
          <asp:ListItem Value="4">มัธยมศึกษาตอนปลาย/ปวช./ปวส.</asp:ListItem>
            <asp:ListItem Value="5">ปริญญาตรีขึ้นไป</asp:ListItem>
        </asp:RadioButtonList></td>
      </tr>
       <tr>
        <td valign="top" class="texttopic">อาชีพ</td>
        <td colspan="3" align="left" valign="top"><asp:RadioButtonList ID="optOccupation" runat="server" RepeatColumns="2">
          <asp:ListItem Selected="True" Value="0">ไม่ระบุ</asp:ListItem>
          <asp:ListItem Value="1">ไม่มีอาชีพ</asp:ListItem>
          <asp:ListItem Value="2">เกษตรกรรม</asp:ListItem>
          <asp:ListItem Value="3">ข้าราชการ/รัฐวิสาหกิจ/พนักงานของรัฐ</asp:ListItem>
          <asp:ListItem Value="4"> พนักงานหน่วยงานเอกชนหรือลูกจ้าง/ค้าขาย/ธุรกิจส่วนตัว</asp:ListItem>
          <asp:ListItem Value="5"> นักเรียน/นักศึกษา</asp:ListItem>
          <asp:ListItem Value="6"> พระ/นักบวช</asp:ListItem>
        </asp:RadioButtonList>

        </td>
      </tr>
      <tr>
        <td valign="top" class="texttopic">สิทธิการรักษา</td>
        <td colspan="3" align="left" valign="top"><asp:RadioButtonList ID="optClaim" runat="server" RepeatDirection="Horizontal" RepeatColumns="2">
          <asp:ListItem Selected="True" Value="UCS">ประกันสุขภาพถ้วนหน้า</asp:ListItem>
          <asp:ListItem Value="SSS">ประกันสังคม</asp:ListItem>
          <asp:ListItem Value="OFC">ข้าราชการ/รัฐวิสาหกิจ</asp:ListItem>
          <asp:ListItem Value="NON">ทราบสิทธิแต่ประสงค์จ่ายเงินเอง</asp:ListItem>
            <asp:ListItem Value="INS">ประกันชีวิต</asp:ListItem>
            <asp:ListItem Value="0">ไม่ระบุ</asp:ListItem>
        </asp:RadioButtonList></td>
        </tr>
    
      
      <tr>
        <td valign="top" class="texttopic">สถานะ</td>
        <td align="left" valign="top"><asp:CheckBox ID="chkClose" runat="server" Text="Active" Checked="True" /></td>
        <td valign="top" class="texttopic">&nbsp;</td>
        <td valign="top">&nbsp;</td>
      </tr>
    </table></td>
  
            
    </table></td>
  </tr>
 
 </table>

                  </div>
           
          </div>
 <div class="row">
        <!-- Left col -->
        <section class="col-lg-6 connectedSortable"> 
            <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">ประวัติคนไข้</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">   
          <table align="left" class="table">
        
              <tr>
                  <td width="120"> HN</td>
                 
                  <td> 
                      <asp:TextBox ID="txtHN" runat="server"   Width="200" MaxLength="200"></asp:TextBox>
                  </td>
                 
              </tr>
              
                 <tr>
                  <td>
                      โรงพยาบาลแม่ข่าย</td>
                 
                  <td>
                     <dx:ASPxComboBox ID="cboHospital" runat="server" CssClass="OptionControl2" Theme="MetropolisBlue" EnableTheming="True" ToolTip="พิมพ์ค้นหาได้" Width="100%">
                         <Columns>
                             <dx:ListBoxColumn Caption="ชื่อโรงพยาบาล" FieldName="HospitalName">
                             </dx:ListBoxColumn>
                             <dx:ListBoxColumn Caption="จังหวัด" FieldName="ProvinceName">
                             </dx:ListBoxColumn>
                             <dx:ListBoxColumn Caption="ประเภท"  FieldName="TypeName">
                             </dx:ListBoxColumn>
                         </Columns>
                     </dx:ASPxComboBox>

                   </td>
                 
              </tr>
                                
             
        </table>
    
      </div>
</div>
           

         </section>
      <section class="col-lg-6 connectedSortable"> 
 <div class="box box-success">
        <div class="box-header with-border">
             <i class="ion ion-heart-broken"></i>
          <h3 class="box-title">พฤติกรรมสุขภาพ</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
           

 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  
   
   <tr>
    <td><table border="0" cellpadding="0" cellspacing="2"  align="Left">
     <tr>
        <td>การสูบบุหรี่</td>
        <td>
            <table border="0" cellspacing="2" cellpadding="0">
              <tr>
                <td align="left"><asp:RadioButtonList ID="optSmoke" runat="server" 
                RepeatDirection="Horizontal" CellPadding="10" CellSpacing="4">
                  <asp:ListItem Value="5">เลิกสูบแล้ว</asp:ListItem>
                  <asp:ListItem Value="4">สูบประจำ</asp:ListItem>
                  <asp:ListItem Selected="True" Value="3">ไม่สูบ</asp:ListItem>
                </asp:RadioButtonList></td>
                <td>
                            <asp:TextBox ID="txtCGNo" runat="server" Width="40px"></asp:TextBox>
&nbsp;<asp:Label ID="lblCGday" runat="server" Text="มวน/วัน"></asp:Label>
            &nbsp;<asp:TextBox ID="txtCGYear" runat="server" Width="40px"></asp:TextBox>
&nbsp;<asp:Label ID="lblCGYear" runat="server" Text="ปี"></asp:Label>
                       
                    </td>
               
                </tr>
            </table>       

        </td>
      </tr>
     <tr>
        <td></td>
        <td>  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td> <asp:Label ID="lblCgType" runat="server" Text="ชนิดของบุหรี่ที่สูบ"></asp:Label></td>
                      <td> <asp:RadioButtonList ID="optCigarette" runat="server" 
                RepeatDirection="Horizontal">
                  <asp:ListItem Value="10">มวนเอง</asp:ListItem>
                  <asp:ListItem Selected="True" Value="11">บุหรี่ซอง</asp:ListItem>
                  <asp:ListItem Value="12">บุหรี่ไฟฟ้า</asp:ListItem>
                
                  </asp:RadioButtonList></td>
                    </tr>
                  </table>
           </td>
      </tr>
      <tr>
        <td>การดื่มแอลกอฮอล์</td>
        <td align="left"><table border="0" cellspacing="2" cellpadding="0">
              <tr>
                <td align="left">
                    <asp:RadioButtonList ID="optAlcohol" runat="server" 
                RepeatDirection="Horizontal">
                  <asp:ListItem Value="9">ดื่มประจำ</asp:ListItem>
                     <asp:ListItem Value="8">ดื่มครั้งคราว</asp:ListItem>
                  <asp:ListItem Value="7">เคยดื่มแต่เลิกแล้ว</asp:ListItem>
                  <asp:ListItem Selected="True" Value="6">ไม่ดื่ม</asp:ListItem>
                </asp:RadioButtonList></td>
                <td align="left">                   
                            <asp:TextBox ID="txtAlcoholFQ" runat="server" Width="50px"></asp:TextBox>
                  &nbsp;<asp:Label ID="lblAlcohol" runat="server" Text=" ครั้ง/สัปดาห์"></asp:Label>
                                          
                   </td>
              </tr>
            </table>
            </td>
      </tr>
 
     
      </table></td> 
  </tr>
 </table>
  </div>
     
</div>

         </section>
  </div>

<asp:Panel ID="pnDetail" runat="server">
 <div class="row">
        <!-- Left col -->
        <section class="col-lg-6 connectedSortable"> 

    <div class="box box-success">
        <div class="box-header with-border">
             <i class="ion ion-leaf"></i>
          <h3 class="box-title">โรคที่เป็น/โรคประจำตัว</h3>
          <div class="box-tools pull-right">
    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"  title="Collapse"><i class="fa fa-plus"></i></button>
          </div>
        </div>
        <div class="box-body">            
          <table width="100%">
            <tr>
                <td  width="150px">เพิ่มโรค</td>
                <td colspan="2"><asp:TextBox ID="txtDeseaseName" runat="server"  Width="100%"></asp:TextBox></td>               
            </tr>
              <tr>
                  <td>จำนวนปีที่เป็นโรคมาแล้ว</td>
                  <td>                    <asp:TextBox ID="txtDescription" runat="server" CssClass="OptionControl" Width="100%"></asp:TextBox>              </td>
                  <td align="center" width="80">
                      <asp:Button ID="cmdAddDesease" runat="server" CssClass="buttonSave" Text="เพิ่มโรค" Width="70px" />
                  </td>
              </tr>             
        </table>    
        </div>
   

      <asp:UpdatePanel ID="UpdatePanelDesease" runat="server">
                          <ContentTemplate>
                              <asp:GridView ID="grdDesease" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                                  <RowStyle BackColor="White" VerticalAlign="Top" />
                                  <columns>
                                      <asp:BoundField DataField="nRow" HeaderText="No">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="DeseaseName" HeaderText="โรคที่เป็น">
                                      <headerstyle HorizontalAlign="Left" />
                                      <itemstyle HorizontalAlign="Left" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField=Descriptions HeaderText="จำนวน (ปี)">
                                      <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                      </asp:BoundField>
                                      <asp:TemplateField HeaderText="ลบ">
                                          <ItemTemplate>
                                              <asp:ImageButton ID="imgDel_DS" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" ImageUrl="images/delete.png" Height="20px" />
                                          </ItemTemplate>
                                          <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                      </asp:TemplateField>
                                  </columns>
                                  <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                  <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                                  <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                  <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                                  <EditRowStyle BackColor="#2461BF" />
                                  <AlternatingRowStyle BackColor="#F7F7F7" />
                              </asp:GridView>
                          </ContentTemplate>
                          <Triggers>
                              
                              <asp:AsyncPostBackTrigger ControlID="cmdAddDesease" EventName="Click" />
                              
                              <asp:AsyncPostBackTrigger ControlID="grdDesease" EventName="RowCommand" />
                              
                          </Triggers>
                      </asp:UpdatePanel>             
      
  <div class="box-footer">            
 </div>
</div>
      
    <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">ยาที่ใช้ ณ ปัจจุบัน</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">              
          <table align="left"  width="100%">
            <tr>
                <td width="100">เพิ่มยาที่ใช้</td>
                <td colspan="2">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtDrugCode" runat="server" CssClass="druguse" Width="100%"></asp:TextBox>
                            </td>
                            <td  width="30px" align="center">
                                <asp:ImageButton ID="imgSearchDrugUse" runat="server"  ImageUrl="images/search.png" /></td>
                           
                        </tr>
                    </table>
                </td>
            </tr>
              <tr>
                  <td>ระบุวิธีกิน</td>
                  <td>
                      <asp:TextBox ID="txtDrugDescription" runat="server" CssClass="OptionControl" Width="100%"></asp:TextBox>
                  </td>
                  <td align="center" width="80">
                      <asp:Button ID="cmdAddDrug" runat="server" CssClass="buttonSave" Text="เพิ่มยา" Width="70px" />
                  </td>
              </tr>
              <tr>
                  <td colspan="3">
                      <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                          <ContentTemplate>
                              <asp:GridView ID="grdDrugUse" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                                  <RowStyle BackColor="White" VerticalAlign="Top" />
                                  <columns>
                                      <asp:BoundField DataField="nRow" HeaderText="No">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="Name" HeaderText="ยาที่ใช้">
                                      <headerstyle HorizontalAlign="Left" />
                                      <itemstyle HorizontalAlign="Left" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="Descriptions" HeaderText="รายละเอียด" />
                                      <asp:TemplateField HeaderText="ลบ">
                                          <ItemTemplate>
                                              <asp:ImageButton ID="imgDel_DU" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" ImageUrl="images/delete.png" />
                                          </ItemTemplate>
                                          <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                      </asp:TemplateField>
                                  </columns>
                                  <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                  <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                                  <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                  <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                                  <EditRowStyle BackColor="#2461BF" />
                                  <AlternatingRowStyle BackColor="#F7F7F7" />
                              </asp:GridView>
                          </ContentTemplate>
                          <Triggers>
                              <asp:AsyncPostBackTrigger ControlID="cmdAddDrug" EventName="Click" />
                              <asp:AsyncPostBackTrigger ControlID="grdDrugUse" EventName="RowCommand" />
                          </Triggers>
                      </asp:UpdatePanel>
                  </td>
              </tr>
              <tr>
                  <td colspan="3" class="LocationName">กรณี ผป ใช้ กลุ่มที่ไม่ใช่ยารักษา ได้แก่</td>
              </tr>
              <tr>
                  <td colspan="3">
                      <table width="100%">
                          <tr>
                              <td width="100">อาหารเสริม</td>
                              <td>
                                  <asp:TextBox ID="txtDrug1" runat="server" CssClass="OptionControl" Width="100%"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                              <td>วิตามิน</td>
                              <td>
                                  <asp:TextBox ID="txtDrug2" runat="server" CssClass="OptionControl" Width="100%"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                              <td>สมุนไพร</td>
                              <td>
                                  <asp:TextBox ID="txtDrug3" runat="server" CssClass="OptionControl" Width="100%"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                              <td>อื่นๆ</td>
                              <td>
                                  <asp:TextBox ID="txtDrug4" runat="server" CssClass="OptionControl" Width="100%"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                              <td>&nbsp;</td>
                              <td>
                                  <asp:Button ID="cmdSaveDrug" runat="server" CssClass="buttonSave" Text="บันทึก" Width="82px" />
                              </td>
                          </tr>
                      </table>
                  </td>
              </tr>
        </table>
    
      </div>
</div>

  </section>
        <section class="col-lg-6 connectedSortable">   
    <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">ประวัติแพ้ยา</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">   
          <table align="left" class="table">
        
              <tr>
                  <td width="100"> แพ้ยาหรือไม่</td>
                 
                  <td colspan="4"> 
                      <asp:RadioButtonList ID="optAllergy" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="N">ไม่แพ้</asp:ListItem>
                          <asp:ListItem Value="Y">แพ้</asp:ListItem>
                      </asp:RadioButtonList>
                  </td>
                 
              </tr>
              
             
              
             
               <tr>
                  <td>
                      ระบุชื่อยา</td>
                 
                  <td>
                      <asp:TextBox ID="txtDrugAllergy" runat="server"   Width="100%" MaxLength="1000"></asp:TextBox>
                   </td>
                 
                  <td align="center" width="70px">อาการแสดง</td>
                 
                  <td>
                      <asp:TextBox ID="txtSymptom" runat="server"   Width="100%" MaxLength="1000"></asp:TextBox>
                   </td>
                 
                  <td width="35px">
                      <asp:ImageButton ID="imgAddAllergy" runat="server" ImageUrl="images/add.png" />
                   </td>
                 
              </tr>                 
              <tr>
                  <td> </td>
                 
                  <td colspan="4">
                      กรณีไม่ทราบชื่อยา โปรดระบุว่าเป็นยาใช้รักษาอาการ / โรคอะไร</td>
                 
              </tr>
              
             
              <tr>
                  <td> รายการแพ้ยา</td>
                 
                  <td colspan="4">
                              <asp:GridView ID="grdAllergy" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                                  <RowStyle BackColor="White" VerticalAlign="Top" />
                                  <columns>
                                      <asp:BoundField DataField="nRow" HeaderText="No">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="DrugAllergy" HeaderText="ยา/กลุ่มยา ที่แพ้">
                                      <headerstyle HorizontalAlign="Left" />
                                      <itemstyle HorizontalAlign="Left" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="Symptom" HeaderText="อาการแพ้" >
                                      <HeaderStyle HorizontalAlign="Left" />
                                      </asp:BoundField>
                                      <asp:TemplateField HeaderText="ลบ">
                                          <ItemTemplate>
                                              <asp:ImageButton ID="imgDel_DU" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" ImageUrl="images/delete.png" />
                                          </ItemTemplate>
                                          <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                      </asp:TemplateField>
                                  </columns>
                                  <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                  <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                                  <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                  <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                                  <EditRowStyle BackColor="#2461BF" />
                                  <AlternatingRowStyle BackColor="#F7F7F7" />
                              </asp:GridView>
                          </td>               
               
                 
              </tr>
              
             
        </table>
    
      </div>
</div>

        </section>
      </div>
 </asp:Panel>  
    
    <br />
    <div align="center">  
      <asp:Button ID="cmdSave" runat="server" CssClass="buttonSave" Text="บันทึก" Width="100px" />
&nbsp;<asp:Button ID="cmdDelete" runat="server" CssClass="buttonRed" Text="ลบ" Width="100px" />
&nbsp;<asp:Button ID="cmdClear" runat="server" CssClass="buttonSave" Text="เพิ่มผู้รับบริการใหม่" />
  </div>
    <br />

<div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Patient</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">   
          <table align="left" class="table">        
              <tr>
                  <td>
                               <table border="0" cellspacing="2" cellpadding="0">
            <tr>
              <td width="50" >ค้นหา</td>
                <td   > 
                    <asp:CheckBox ID="chkisLocation" runat="server" Text="เฉพาะผู้รับบริการที่ร้านเพิ่มเอง" Checked="true" />
                    </td>
              <td >
                  <asp:TextBox ID="txtSearch" runat="server" Width="200px"></asp:TextBox>                </td>
              <td >
                  <asp:Button ID="cmdFind" runat="server" CssClass="buttonFind" Text="ค้นหา" />
                </td>
            </tr>
            
          </table>

                  </td>                 
              </tr>
              <tr>
                  <td>
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True">
            <RowStyle BackColor="#F7F7F7" />
            <columns>
                <asp:BoundField HeaderText="Patient ID" DataField="PatientID" >
                <ItemStyle HorizontalAlign="Center" Width="70px" />                </asp:BoundField>
                <asp:BoundField DataField="ForeName" HeaderText="ชื่อ" />
                <asp:BoundField DataField="Surname" HeaderText="นามสกุล" />
                <asp:BoundField DataField="Ages" HeaderText="อายุ" >
                <ItemStyle HorizontalAlign="Center" Width="30px" />                </asp:BoundField>
            <asp:BoundField HeaderText="เพศ" DataField="Gender">                      
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="70px" />                      </asp:BoundField>
                <asp:TemplateField HeaderText="เลขบัตร ปชช.">
                    <ItemTemplate>
                        <asp:Label ID="lblCardID" runat="server"> <%# FormatCardID(DBNull2Str(DataBinder.Eval(Container.DataItem, "CardID")))%></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField HeaderText="จังหวัด" DataField="ProvinceName" >
                </asp:BoundField>
                <asp:TemplateField>
                    <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PatientID")%>' 
                                        ImageUrl="images/icon-edit.png" Visible='<%# DataBinder.Eval(Container.DataItem, "isLocation")%>'  />
                                 <!--
                                     <asp:ImageButton ID="imgDel"  cssclass="gridbutton"  runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PatientID")%>' 
                                        ImageUrl="images/delete.png" Visible='<%# DataBinder.Eval(Container.DataItem, "isLocation")%>'  />       -->             
                                     </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="45px" />
                </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" 
                      VerticalAlign="Middle" HorizontalAlign="Center" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>
                  </td>                 
              </tr>
        </table>
    
      </div>
</div>

</section>

</asp:Content>
 

