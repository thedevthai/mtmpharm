﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="apiPrescription.aspx.vb" Inherits=".apiPrescription" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <section class="content-header">
      <h1>ใบสั่งยา
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">ใบสั่งยา</li>
      </ol>
    </section>
       
      <section class="content">  
<div class="row">
   <section class="col-lg-12 connectedSortable">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">ค้นหาใบสั่งยา</h3>
    </div> 
    <div class="box-body">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>โรงพยาบาล</label>
              <asp:DropDownList ID="ddlHospital" runat="server" cssclass="form-control select2" >
            </asp:DropDownList>
          </div>
        </div>
      
    

        <div class="col-md-4">
          <div class="form-group">
            <label>ผู้รับบริการ</label>
                <asp:DropDownList ID="ddlPatient" runat="server" cssclass="form-control select2"  >
            </asp:DropDownList>
          </div>

        </div>
       
        <div class="col-md-2">
          <div class="form-group">
            <label>เลขที่ใบสั่งยา</label>
            <asp:TextBox ID="txtPrescriptionNo" runat="server" cssclass="form-control" placeholder=""></asp:TextBox>
          </div>

        </div>
           <div class="col-md-2">
          <div class="form-group">
            <label></label>
              <br />
                  <asp:Button ID="cmdSearch" runat="server" Text="ค้นหา" CssClass="buttonSave" Width="100px" />
          </div>

        </div>

       </div>
         </div> 
  </div>

        <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">รายการใบสั่งยา</h3>
    </div> 
    <div class="box-body">
            <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th class="sorting_asc_disabled"></th>    
                  <th>วันที่</th>
                  <th>โรงพยาบาล</th>
                  <th>ชื่อผู้รับบริการ</th>
                  <th>เพศ</th>  
                     <th>อายุ</th>
                  <th>สถานะ</th>
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtPrescription.Rows %>
                <tr>
                 <td width="30px"><a class="editemp" href="CompanyModify?cid=<% =String.Concat(row("UID")) %>" >
                     <img src="/images/icon-edit.png" /></a></td>
                  <td><% =String.Concat(row("CompanyCode")) %></td>
                  <td><% =String.Concat(row("HospitalName")) %>    </td>
                  <td><% =String.Concat(row("PatientName")) %></td>
                  <td><% =String.Concat(row("Sex")) %></td> 
                     <td><% =String.Concat(row("Age")) %></td> 
                  <td><% =IIf(String.Concat(row("StatusFlag")) = "A", "<img src='images/icon-ok.png'>", "") %> </td>
                   
                </tr>
            <%  Next %>
                </tbody>               
              </table>   
 </div> 
  </div>

  </section>
</div>
          </section>

</asp:Content>
