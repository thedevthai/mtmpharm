﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="DrugRefill.aspx.vb" Inherits=".DrugRefill" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register Src="ucPatientInfo.ascx" TagPrefix="uc2" TagName="ucPatientInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/mtmstyles.css">

  <link rel="stylesheet" type="text/css" href="css/uidialog.css">
  <link rel="stylesheet" type="text/css" href="css/pagestyles.css"> 
  <link rel="stylesheet" type="text/css" href="css/button.css"> 
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server"> 
        <section class="content-header">
      <h1>
        Drug Refill
        <small>บันทึกการจ่ายยา</small>
      </h1> 
    </section>
    
    <!-- Main content -->
    <section class="content">
        
     <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        //$(document).ready(function () {

        //    $('.btnCssDrugRefill').click(function () {
        //        window.open('DrugSearch.aspx?p=drugrefill','Drug','height=400,width=600');
        //        return false;
        //    });

        //     $('.btnCssDrugRemain').click(function () {
        //        window.open('DrugSearch.aspx?p=drugremain','Drug','height=400,width=600');
        //        return false;
        //    });
          

        //     $('.btnCssICD').click(function () {
        //        window.open('ICD10Search.aspx?p=icd10','ICD10','height=400,width=600');
        //        return false;
        //    });

        //});

        function getListItems(items,pname) {        
            if (pname == 'drug') {
                $('.drug').val(items);
            }
        } 

    </script>


      <!-- Main row -->
  <div class="row">
    <section class="col-lg-12 connectedSortable">    
      <div class="box box-solid box-success">
            <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">ข้อมูลผู้รับบริการ</h3>             
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>                 
            </div>
            <div class="box-body">
                <uc2:ucPatientInfo runat="server" ID="ucPatientInfo" />
            </div>            
          </div>
    </section>

</div>
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-6 connectedSortable">     
 
      

        
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">บันทึกจ่ายยา</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  
     
        <table width="100%">
            <tr>
                <td width="150">วันที่</td>
                <td>
                                <asp:TextBox ID="txtDate" runat="server" CssClass="OptionControl2" Width="100px"></asp:TextBox> <small>(วว/ดด/ปปปป พ.ศ.)</small>
                </td>
            </tr>
            <tr>
                <td width="150"></td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="รูปแบบวันที่ไม่ถูกต้อง" ForeColor="Red" ControlToValidate="txtDate" ValidationExpression="\d{2}/\d{2}/\d{4}" ></asp:RegularExpressionValidator>
                    <asp:HiddenField ID="hdUID" runat="server" />
                    <asp:HiddenField ID="hdRefillHeaderUID" runat="server" />
                </td>
            </tr>
             <tr>
                 <td>ยา</td>
                 <td>
                       <table width="100%">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtDrug" runat="server" CssClass="drug" Width="100%"></asp:TextBox>
                            </td>
                            <td  width="30px">
                                <asp:ImageButton ID="imgSearchDrug" runat="server"  ImageUrl="images/search.png" /></td>
                           
                        </tr>
                    </table>
                 </td>
            </tr>
            <tr>
                <td>จำนวน</td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtQTYRefill" runat="server" CssClass="OptionControl2" Width="100px"></asp:TextBox>
                            </td>
                            <td>หน่วย</td>
                            <td>
                                <asp:TextBox ID="txtUOMRefill" runat="server" CssClass="OptionControl2" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>วิธีใช้/รับประทาน</td>
                <td>
                    <asp:TextBox ID="txtRefillDesc" runat="server" CssClass="OptionControl2" Width="100%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td valign="top">&nbsp;</td>
                <td>
                    <asp:Button ID="cmdDrugRefill" runat="server" CssClass="buttonSave" Text="เพิ่มรายการยา" Width="100px" />
                    &nbsp;<asp:Button ID="cmdCancel" runat="server" CssClass="buttonSave" Text="ยกเลิก" Width="100px" />
                    </td>
            </tr>
    </table>
 
                     <asp:Panel ID="pnDrugRefillAlert" runat="server">
                         <div class="alert alert-danger alert-dismissible">
                             <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                             <p>
                                 <asp:Label ID="lblRemainAlert" runat="server"></asp:Label>
                             </p>
                         </div>
                     </asp:Panel>
         
       
      </div>
      <!-- /.box -->  
</div> 
  
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-6 connectedSortable">
            
      

        <div class="box box-success">
            <div class="box-header">
              <i class="ion ion-medkit"></i>
              <h3 class="box-title">รายการยาที่จ่ายให้ผู้รับบริการ</h3>
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
                 
            </div>
          
            <div class="box-body">
       
           
                    <asp:GridView ID="grdDrugRefill" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                        <RowStyle BackColor="White" VerticalAlign="Top" />
                        <columns>
                            <asp:BoundField DataField="ServiceDate" HeaderText="วันที่" DataFormatString="{0:dd-MM-yyyy}">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DrugName" HeaderText="ยา" >
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="QTY" HeaderText="จำนวน">
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="UOM" HeaderText="หน่วย">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" /> 
                            </asp:BoundField>

                              <asp:BoundField DataField="UsedRemark" HeaderText="วิธีใช้/รัปประทาน">
                            <HeaderStyle HorizontalAlign="left" />
                            <ItemStyle HorizontalAlign="left" /> 
                            </asp:BoundField>

                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Container.DataItemIndex %>' CssClass="gridbutton" ImageUrl="images/icon-edit.png" />
                                </ItemTemplate>
                                <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Del">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDel" runat="server" CommandArgument='<%#  Container.DataItemIndex %>'  CssClass="gridbutton" ImageUrl="images/delete.png" />
                                </ItemTemplate>
                                <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                            </asp:TemplateField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="#F7F7F7" />
                    </asp:GridView>
                
                                       
            </div> 
            <div class="box-footer clearfix no-border">
              
                    <asp:Button ID="cmdSave" runat="server" CssClass="buttonSave" Text="บันทึก / Confirm" />
                    &nbsp;<asp:Button ID="cmdPrint" runat="server" CssClass="buttonSave" Text="พิมพ์ใบจ่ายยา" Width="100px" />
              
            </div>
          </div>       
 
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->

  <!-- /.content-wrapper -->
  <div class="control-sidebar-bg"></div>
    
</asp:Content>
