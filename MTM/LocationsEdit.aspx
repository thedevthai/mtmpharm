﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LocationsEdit.aspx.vb" Inherits=".LocationsEdit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 </asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>ข้อมูลร้านยา
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">ข้อมูลร้านยา</li>
      </ol>
    </section>

<section class="content">    
  

          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-users"></i>

              <h3 class="box-title">บันทึก/แก้ไข ข้อมูลร้านยา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">   
<table width="100%" border="0" cellPadding="1" cellSpacing="1">
                                                <tr>
                                                     <td width="150" align="left" >รหัส :  </td>
                                                  <td align="left" >
                                                       
                                                              <asp:Label ID="lblID" runat="server" Font-Bold="True" ForeColor="#41A206"></asp:Label>                                                     </td>   <td width="150" align="left">&nbsp;                                                        
                                                         รหัสหน่วยบริการ</td>
                                                    <td align="left" >
                                                        <asp:TextBox ID="txtCode" runat="server" Width="200px"></asp:TextBox>
                                                     </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" >ประเภท :</td>
                                                    <td align="left" >
                                                        <asp:Label ID="lblTypeName" runat="server"></asp:Label>                                                    </td>
                                                  <td align="left" >จังหวัด : </td>
                                                    <td align="left" >
                                                        <asp:Label ID="lblProvinceName" runat="server"></asp:Label>                                                    </td>
                                                </tr>
                                                <tr>
                                                  <td align="left" >ชื่อร้านยา:</td>
                                                  <td align="left" ><asp:TextBox ID="txtName" runat="server" Width="300px" CssClass="input_control"></asp:TextBox></td>
  <td align="left" >ประเภทร้านยา:</td>
                                                      <td align="left">
                                                           <table >
                                                             <tr>
                                                                 <td>
                                                        <asp:DropDownList ID="ddlTypeShop" runat="server" CssClass="OptionControl">
                                                            <asp:ListItem Selected="True">ร้านยาคุณภาพ</asp:ListItem>
                                                            <asp:ListItem Value="ร้านยา GPP">ร้านยา GPP</asp:ListItem>
                                                            <asp:ListItem Value="ร้านยาเภสัชกร"></asp:ListItem>
                                                                     </asp:DropDownList>                                                    
                                                                 </td><td>ระบุ</td>
                                                                 <td>                                                    
                                                        <asp:TextBox ID="txtTypeName" runat="server"></asp:TextBox>                                                    </td>
                                                             
                                                             </tr>
                                                         </table>
                                                      </td>


                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" >ที่อยู่ :</td>
                                                    <td align="left"><asp:TextBox ID="txtAddress" runat="server" Width="400px" TextMode="MultiLine"></asp:TextBox></td>
                                                    <td align="left" valign="bottom" >รหัสไปรษณีย์:</td>
                                                    <td align="left" valign="bottom" >
                                                        <asp:TextBox ID="txtZipCode" runat="server" 
                                                        CssClass="input_control" MaxLength="5"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" >โทรศัพท์ :                                                    </td>
                                                    <td align="left" ><table width="100%" border="0" cellspacing="2" cellpadding="0">
                                                          <tr>
                                                            <td><asp:TextBox ID="txtTel" runat="server" Width="200px"></asp:TextBox></td>
                                                            <td width="60">โทรสาร :</td>
                                                            <td><asp:TextBox ID="txtFax" runat="server" Width="200px"></asp:TextBox></td>
                                                          </tr>
                                                        </table></td>
                                                    <td align="left" >E-mail :</td>
                                                    <td align="left" ><asp:TextBox ID="txtMail" runat="server" 
                                                            Width="200px"></asp:TextBox></td>
                                                </tr>
                                                  <tr>
                                                  <td align="left" >ชื่อเจ้าของ(คู่สัญญา)</td>
                                                  <td  align="left" ><table width="100%" border="0" cellpadding="0" cellspacing="2">
                                                <tr>
                                                          <td><asp:TextBox ID="txtCoName" runat="server" Width="200px"></asp:TextBox></td>
                                                          <td width="60">E-mail :</td>
                                                          <td><asp:TextBox ID="txtCoMail" runat="server" Width="200px"></asp:TextBox></td>
                                                    </tr>
                                                      </table></td>
                                                      <td >โทรศัพท์ : </td>
                                                      <td><asp:TextBox ID="txtCoTel" runat="server" Width="200px"></asp:TextBox></td>
          </tr>
                                                <tr>
                                                  <td align="left" >เลขที่บัญชี :</td>
                                                  <td align="left" >
                                                      <asp:TextBox ID="txtAccNo" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                                                  <td align="left" >ธนาคาร :</td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlBank" runat="server" CssClass="OptionControl">                                                    </asp:DropDownList>                                                    </td>
          </tr>
                                                <tr>
                                                  <td align="left" >ชื่อบัญชี :</td>
                                                  <td align="left" >
                                                      <asp:TextBox ID="txtAccName" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                                                  <td align="left" >สาขา :</td>
                                                <td align="left" >
                                                    <asp:TextBox ID="txtBrunch" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                                                </tr>
                                                <tr>
                                                  <td align="left" >ประเภทบัญชี :</td>
                                                  <td colspan="3" align="left">
                                                      <asp:RadioButtonList ID="optBankType" runat="server" 
                                                          RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True" Value="SAV">ออมทรัพย์/สะสมทรัพย์/เผื่อเรียก</asp:ListItem>
                                                    <asp:ListItem Value="DDA">กระแสรายวัน/เดินสะพัด</asp:ListItem>
                                                    <asp:ListItem Value="BOC">ฝากประจำ</asp:ListItem>
                                                  </asp:RadioButtonList></td>
                                                 </tr>
                                                <tr>
                                                  <td align="left" >เลขบัตรประชาชน:</td>
                                                  <td align="left">
                                                      <asp:TextBox ID="txtCardID" runat="server" Width="300px" MaxLength="13"></asp:TextBox>                                                    &nbsp;(เฉพาะตัวเลขเท่านั้น)</td>
                                                  <td align="left" >&nbsp;</td>
                                                  <td align="left" >&nbsp;</td>
                                                </tr>
     <tr>
                                                  <td align="left" >ปี พ.ศ.ที่สมัครเข้าโครงการ</td>
                                                  <td align="left">       
                                                      <asp:TextBox ID="txtYear" runat="server" MaxLength="4"></asp:TextBox>
                                                    </td>
                                                  <td align="left" >&nbsp;</td>
                                                  <td align="left" >&nbsp;</td>
                                                </tr>

  </table>     

 </div>
 <div class="box-footer clearfix">
           <asp:Label ID="lblValidate" runat="server"    Visible="False" CssClass="validateAlert" Width="99%"></asp:Label>
            </div>
          </div>
       
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-users"></i>

              <h3 class="box-title">เภสัชกรที่ปฏิบัติหน้าที่ <asp:Label ID="lblUserOnlineCount" runat="server" Text=""></asp:Label></h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
<asp:GridView ID="grdData" 
                             runat="server" CellPadding="2" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" CssClass="txtcontent" 
                             Font-Bold="False">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                            <asp:BoundField />
                        <asp:BoundField DataField="PrefixName" HeaderText="คำนำหน้า" >                            
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="90px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FirstName" HeaderText="ชื่อ" >
                            <HeaderStyle HorizontalAlign="Left" />                            
                            <ItemStyle Width="120px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LastName" HeaderText="นามสกุล" >
                            <HeaderStyle HorizontalAlign="Left" />                            
                            <ItemStyle Width="150px" />
                            </asp:BoundField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" 
                                                            />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView>              
            </div>
           
          </div>

    <div align="center">
        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="100px" />
        &nbsp;<asp:Button ID="cmdClear" runat="server" Text="ยกเลิก" CssClass="buttonSave" Width="100px" /> 
    </div>
               
  </section>      
</asp:Content>
