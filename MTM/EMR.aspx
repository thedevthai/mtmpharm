﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="EMR.aspx.vb" Inherits=".EMR" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register Src="ucPatientInfo.ascx" TagPrefix="uc2" TagName="ucPatientInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/mtmstyles.css">

  <link rel="stylesheet" type="text/css" href="css/uidialog.css">
  <link rel="stylesheet" type="text/css" href="css/pagestyles.css"> 
  <link rel="stylesheet" type="text/css" href="css/button.css"> 
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server"> 
        <section class="content-header">
      <h1>
        EMR 
        <small>Viewer</small>
      </h1>
 <div class="menuicon">
     
           <table  border="0" align="center" cellpadding="0" cellspacing="2">
         <tr>
          <td><asp:ImageButton ID="cmdNewVisit" runat="server" ImageUrl="images/public48.png" ToolTip="Create New Visit" Width="32px" /></td>
         
              <td><asp:ImageButton ID="cmdDrugDispend" runat="server" ImageUrl="images/pills48.png"  Width="32px" ToolTip="จ่ายยา" /></td>           
              <td><asp:ImageButton ID="cmdAddRefer" runat="server" ImageUrl="images/ambulance48.png"  Width="32px" ToolTip="Refer " /></td>  <td><asp:ImageButton ID="cmdUpload" runat="server" ImageUrl="images/doc48.png"  Width="32px" ToolTip="Document Upload " /></td>
        </tr>         
        </table>
                                    
        
      </div>
    </section>
    
    <!-- Main content -->
    <section class="content">           
         

     <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        //$(document).ready(function () {

        //    $('.btnCssDrugRefill').click(function () {
        //        window.open('DrugSearch.aspx?p=drugrefill','Drug','height=400,width=600');
        //        return false;
        //    });

        //     $('.btnCssDrugRemain').click(function () {
        //        window.open('DrugSearch.aspx?p=drugremain','Drug','height=400,width=600');
        //        return false;
        //    });
          

        //     $('.btnCssICD').click(function () {
        //        window.open('ICD10Search.aspx?p=icd10','ICD10','height=400,width=600');
        //        return false;
        //    });

        //});

        function getListItems(items,pname) {
            if (pname == 'icd10') {
                $('.icd10').val(items);
            }
            if (pname == 'drugrefill') {
                $('.drugrefill').val(items);
            }
            if (pname == 'drugremain') {
                $('.drugremain').val(items);
            }
            if (pname == 'drug') {
                $('.drug').val(items);
            }
            if (pname == 'druguse') {
                $('.druguse').val(items);
            }
            //$('.csstextbox').val(items);
        } 

    </script>

      <!-- Main row -->
  <div class="row">
    <section class="col-lg-12 connectedSortable">    
      <div class="box box-solid box-success">
            <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">ข้อมูลผู้รับบริการ</h3>             
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>                 
            </div>
            <div class="box-body">
                <uc2:ucPatientInfo runat="server" ID="ucPatientInfo" />
            </div>            
          </div>
    </section>

</div>
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-6 connectedSortable">     
 
   
<div class="box box-success">
        <div class="box-header with-border">
             <i class="ion ion-heart-broken"></i>
          <h3 class="box-title">พฤติกรรมสุขภาพ</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
           

 <table width="100%" border="0">  
   <tr>
    <td width="100px">การสูบบุหรี่</td>
        <td> <asp:Label ID="lblCigarette" runat="server"></asp:Label> </td>
      </tr> 
      <tr>
        <td>การดื่มแอลกอฮอล์</td>
        <td align="left"><asp:Label ID="lblAlcohol" runat="server"></asp:Label></td> 
  </tr>
      <tr>
        <td>แพ้ยา</td>
        <td align="left"><asp:Label ID="lblAllergy" runat="server"></asp:Label></td> 
  </tr>
 </table>
  </div>
      <div class="box-footer pull-right">
                
          <asp:ImageButton ID="cmdEditMedical" runat="server" ImageUrl="images/icon-edit.png" Height="16px" />
                
        </div>
</div>
             

    <div class="box box-success">
        <div class="box-header with-border">
             <i class="ion ion-leaf"></i>
          <h3 class="box-title">โรคที่เป็น</h3>
          <div class="box-tools pull-right">
    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"  title="Collapse"><i class="fa fa-plus"></i></button>
          </div>
        </div>
        <div class="box-body">            
          <table width="100%">
            <tr>
                <td  width="180px">เพิ่มโรคที่เป็น</td>
                <td colspan="2"> 
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtICD10Code" runat="server" CssClass="icd10" Width="100%"></asp:TextBox>
                            </td>

                            <td width="30px">
                                <asp:ImageButton ID="imgSearchICD" runat="server" CssClass="btnCssICD" ImageUrl="images/search.png" Visible="False" />
                            </td>
                           
                        </tr>
                    </table>
                </td>
               
            </tr>
              <tr>
                  <td>ระบุจำนวนปีที่เป็นโรคมาแล้ว</td>

                  <td>
                      <asp:TextBox ID="txtDeseaseOther" runat="server" CssClass="OptionControl" Width="100%"></asp:TextBox>
                  </td>
                  <td align="center" width="80">
                      <asp:Button ID="cmdAddDesease" runat="server" CssClass="buttonSave" Text="เพิ่มโรค" Width="70px" />
                  </td>
              </tr>             
        </table>    
        </div>
   

      <asp:UpdatePanel ID="UpdatePanelDesease" runat="server">
                          <ContentTemplate>
                              <asp:GridView ID="grdDesease" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                                  <RowStyle BackColor="White" VerticalAlign="Top" />
                                  <columns>
                                      <asp:BoundField HeaderText="No">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="DeseaseName" HeaderText="โรคที่เป็น">
                                      <headerstyle HorizontalAlign="Left" />
                                      <itemstyle HorizontalAlign="Left" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="Descriptions" HeaderText="จำนวน (ปี)">
                                      <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                      </asp:BoundField>
                                      <asp:TemplateField HeaderText="ลบ">
                                          <ItemTemplate>
                                              <asp:ImageButton ID="imgDel_DS" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" ImageUrl="images/delete.png" Height="20px" />
                                          </ItemTemplate>
                                          <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                      </asp:TemplateField>
                                  </columns>
                                  <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                  <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                                  <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                  <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                                  <EditRowStyle BackColor="#2461BF" />
                                  <AlternatingRowStyle BackColor="#F7F7F7" />
                              </asp:GridView>
                          </ContentTemplate>
                          <Triggers>
                              
                              <asp:AsyncPostBackTrigger ControlID="cmdAddDesease" EventName="Click" />
                              
                          </Triggers>
                      </asp:UpdatePanel>             
      
  <div class="box-footer">            
 </div>
</div>
 
            <div class="box box-success">
        <div class="box-header with-border">
             <i class="ion ion-egg"></i>
          <h3 class="box-title">ยาที่ใช้ ณ ปัจจุบัน</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">             <i class="fa fa-minus"></i></button>          
          </div>
        </div>
        <div class="box-body">   
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                          <ContentTemplate>
                              <asp:GridView ID="grdDrugUse" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                                  <RowStyle BackColor="White" VerticalAlign="Top" />
                                  <columns>
                                      <asp:BoundField DataField="nRow" HeaderText="No">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="Name" HeaderText="ยาที่ใช้">
                                      <headerstyle HorizontalAlign="Left" />
                                      <itemstyle HorizontalAlign="Left" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="Descriptions" HeaderText="รายละเอียด" />
                                      <asp:TemplateField HeaderText="ลบ">
                                          <ItemTemplate>
                                              <asp:ImageButton ID="imgDel_DU" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" ImageUrl="images/delete.png"  Height="20px" />
                                          </ItemTemplate>
                                          <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                      </asp:TemplateField>
                                  </columns>
                                  <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                  <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                                  <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                  <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                                  <EditRowStyle BackColor="#2461BF" />
                                  <AlternatingRowStyle BackColor="#F7F7F7" />
                              </asp:GridView>
                          </ContentTemplate>
                          <Triggers>
                          </Triggers>
                      </asp:UpdatePanel>
      </div>
             <div class="box-footer">
                
        </div>

</div>

    <div class="box box-success">
        <div class="box-header with-border">
             <i class="ion ion-pricetag"></i>
          <h3 class="box-title">ปัญหาที่พบ</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">        
           
                    <asp:GridView ID="grdProblem" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                        <RowStyle BackColor="White" VerticalAlign="Top" />
                        <columns>
                            <asp:BoundField DataField="nRow" HeaderText="No">
                            <HeaderStyle HorizontalAlign="Center" />
                            <itemstyle HorizontalAlign="Center" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ServiceDate" HeaderText="วันที่">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ProblemDesc" HeaderText="ปัญหาที่พบ" />
                            <asp:BoundField DataField="ResultTXT" HeaderText="สรุป" />
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="#F7F7F7" />
                    </asp:GridView>
               
       
             <!-- /.box-body -->
      
        <!-- /.box-footer-->
      </div>
      <div class="box-footer">
        </div>  <!-- /.box -->  
</div> 
 
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-6 connectedSortable">
            
              <div class="box box-success">
        <div class="box-header with-border">
            <i class="ion ion-clock"></i>
          <h3 class="box-title">MTM Visit</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>            
          </div>
        </div>
        <div class="box-body">  
                      <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                          <ContentTemplate>
                              <asp:GridView ID="grdVisit" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                                  <RowStyle BackColor="White" VerticalAlign="Top" />
                                  <columns>
                                      <asp:BoundField DataField="SEQ" HeaderText="No">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="ServiceDateTXT" HeaderText="Visit Date">
                                      <headerstyle HorizontalAlign="Left" />
                                      <itemstyle HorizontalAlign="Left" />
                                      </asp:BoundField>
                                      <asp:TemplateField>
                                          <ItemTemplate>
                                              <asp:ImageButton ID="imgVisit" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" ImageUrl="images/search.png" Height="20px" />
                                          </ItemTemplate>
                                          <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                      </asp:TemplateField>
                                  </columns>
                                  <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                  <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                                  <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                  <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                                  <EditRowStyle BackColor="#2461BF" />
                                  <AlternatingRowStyle BackColor="#F7F7F7" />
                              </asp:GridView>
                          </ContentTemplate>
                          <Triggers>
                               
                          </Triggers>
                      </asp:UpdatePanel>
                
      </div>
                   <div class="box-footer clearfix">
           
            </div>
</div>
      

        <div class="box box-success">
            <div class="box-header">
              <i class="ion ion-medkit"></i>
              <h3 class="box-title">รายการยาที่จ่ายให้ผู้รับบริการ</h3>
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
                 
            </div>
          
            <div class="box-body">
       
                                       
                    <asp:GridView ID="grdDrugRefill" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                        <RowStyle BackColor="White" VerticalAlign="Top" />
                        <columns>
                            <asp:BoundField DataField="nRow" HeaderText="No">
                            <HeaderStyle HorizontalAlign="Center" />
                            <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="RefillDate" HeaderText="วันที่" DataFormatString="{0:d}">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DrugName" HeaderText="ยา" >
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="QTY" HeaderText="จำนวน">
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="UOM" HeaderText="หน่วย">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="#F7F7F7" />
                    </asp:GridView>
       
                                       
            </div> 
            <div class="box-footer clearfix no-border">
              
            </div>
          </div>       
 
  <div class="box box-success">
            <div class="box-header">
              <i class="ion ion-document"></i>

              <h3 class="box-title">Document</h3>
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
                 
            </div>
     
            <div class="box-body">
              
                                       
                <asp:GridView ID="grdDocument" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                    <RowStyle BackColor="White" VerticalAlign="Top" />
                    <columns>
                        <asp:HyperLinkField DataNavigateUrlFields="FilePath" DataTextField="DocDate" DataTextFormatString="{0:d}" HeaderText="วันที่">
                        <ItemStyle HorizontalAlign="Center" Width="100px" />
                        </asp:HyperLinkField>
                        <asp:HyperLinkField DataNavigateUrlFields="FilePath" DataTextField="Descriptions" HeaderText="รายการเอกสารอัพโหลด" Target="_blank" />
                        <asp:TemplateField HeaderText="ลบ">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgDel_Doc" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" Height="20px" ImageUrl="images/delete.png" />
                            </ItemTemplate>
                            <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                        </asp:TemplateField>
                    </columns>
                    <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                    <EditRowStyle BackColor="#2461BF" />
                    <AlternatingRowStyle BackColor="#F7F7F7" />
                </asp:GridView>
              
                                       
            </div>
          <div class="box-footer clearfix">
           
            </div>
            
          </div>
       <div class="box box-success">
        <div class="box-header with-border">
             <i class="ion ion-forward"></i>
          <h3 class="box-title">Refer</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse"><i class="fa fa-plus"></i></div>
        </div>
        <div class="box-body">  
     
        <table width="100%">
             <tr>
                 <td width="100">Refer to</td>
                 <td>
                     <dx:aspxcombobox ID="cboHospital" runat="server" CssClass="OptionControl2" Width="95%" Theme="MetropolisBlue" EnableTheming="True" ToolTip="พิมพ์ค้นหาได้">                         
                     </dx:aspxcombobox>
                 </td>
            </tr>
            <tr>
                <td>เหตุผลที่ Refer</td>
                <td> <asp:TextBox ID="txtReasonRefer" runat="server" CssClass="OptionControl2" Width="95%"></asp:TextBox>
                  
                </td>
            </tr>
            <tr>
                <td valign="top">&nbsp;</td>
                <td>
                    <asp:Button ID="cmdRefer" runat="server" CssClass="buttonSave" Text="บันทึก" Width="100px" />
                   </td>
            </tr>
    </table>
 
                     <asp:Panel ID="pnRefer" runat="server">
                         <div class="alert alert-danger alert-dismissible">
                             <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                             <p>
                                 <asp:Label ID="Label2" runat="server"></asp:Label>
                             </p>
                         </div>
                     </asp:Panel>
           
       
             <!-- /.box-body -->
       
      </div> <div class="box-footer clearfix">
            <asp:Button ID="cmdPrintRefer" runat="server" CssClass="buttonSave" Text="พิมพ์ใบ Refer" Width="100px" />
            </div>
      <!-- /.box -->  
</div>     
         
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->

  <!-- /.content-wrapper -->
  <div class="control-sidebar-bg"></div>
    
</asp:Content>
