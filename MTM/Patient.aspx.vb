﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Rajchasi
Public Class Patient
    Inherits System.Web.UI.Page
    Dim dt As New DataTable 
    Dim ctlP As New PatientController
    Dim objuser As New UserController
    Dim ctlMed As New DrugController
    Dim ctlD As New DeseaseController

    Dim strD() As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then

            If Request("acttype") = "view" Then
                cmdSave.Visible = False
                cmdClear.Visible = False
                cmdDelete.Visible = False
            Else
                cmdSave.Visible = True
                cmdClear.Visible = True
                cmdDelete.Visible = True
            End If
            If Session("RoleID") = isAdminAccess Then
                chkisLocation.Visible = False
            Else
                chkisLocation.Visible = True
            End If
            LoadProvinceToDDL()
            ClearData()
            LoadDataToComboBox()

            If Not Request("PatientID") Is Nothing Then
                EditData(Request("PatientID"))
            End If
            LoadPatientData()

            cmdDelete.Visible = False
        End If
        txtAges.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtZipCode.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtCardID.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")

        Dim scriptString As String = "javascript:return confirm(""คุณแน่ใจที่จะลบผู้รับบริการรายนี้ใช่หรือไม่?"");"
        cmdDelete.Attributes.Add("onClick", scriptString)

        txtCardID.Attributes.Add("OnKeyPress", "return autoTab(this);")

        'imgSearchICD.Attributes.Add("onclick", "javascript:void(window.open('ICD10Search.aspx?p=icd10',null,'scrollbars=1,width=650,HEIGHT=550'));")
        imgSearchDrugUse.Attributes.Add("onclick", "javascript:void(window.open('DrugSearch.aspx?p=druguse',null,'scrollbars=1,width=650,HEIGHT=550'));")


    End Sub

    Private Sub LoadPatientData()
        Dim isLoc As Integer
        If Session("RoleID") = isAdminAccess Or Session("RoleID") = isSuperAdminAccess Then
            isLoc = 0
        Else
            If chkisLocation.Checked Then
                isLoc = 1
            Else
                isLoc = 0
            End If
        End If
        dt = ctlP.Patient_GetByLocation(Session("LocationID"), txtSearch.Text, isLoc)
        If dt.Rows.Count > 0 Then
            With grdData
                .DataSource = dt
                .DataBind()
                .Visible = True
            End With
        Else
            grdData.Visible = False
            grdData.DataSource = Nothing
        End If
        dt = Nothing
    End Sub

    Private Sub LoadProvinceToDDL()
        dt = ctlP.LoadProvince
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                .SelectedIndex = 1
            End With
        End If
        dt = Nothing
    End Sub
    'Private Sub LockControls()
    '    txtForeName.ReadOnly = True
    '    txtSurname.ReadOnly = True
    '    optGender.Enabled = False
    '    txtAges.ReadOnly = True
    '    txtCardID.ReadOnly = True
    '    txtBirthDate.ReadOnly = True
    '    txtTelephone.ReadOnly = True
    '    txtMobile.ReadOnly = True
    '    optAddressType.Enabled = False
    '    txtAddress.ReadOnly = True
    '    txtRoad.ReadOnly = True
    '    txtDistrict.ReadOnly = True
    '    txtCity.ReadOnly = True
    '    ddlProvince.Enabled = False
    '    optClaim.Enabled = False
    '    txtZipCode.ReadOnly = True
    '    chkClose.Enabled = False
    'End Sub

    Private Sub EditData(pID As String)
        ClearData()
        dt = ctlP.Patient_GetAllByID(StrNull2Zero(pID))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)

                If Request("t") = "new" Then
                    lblPatientID.Text = ""
                ElseIf Request("t") = "edit" Then
                    lblPatientID.Text = String.Concat(.Item("itemID"))
                ElseIf Request("t") Is Nothing Then
                    lblPatientID.Text = ""
                End If

                lblPatientID.Text = DBNull2Str(.Item("PatientID"))
                Session("patientid") = DBNull2Str(.Item("PatientID"))
                txtForeName.Text = DBNull2Str(.Item("ForeName"))
                txtSurname.Text = DBNull2Str(.Item("SurName"))
                optGender.SelectedValue = DBNull2Str(.Item("Gender"))
                txtAges.Text = DBNull2Str(.Item("Ages"))
                txtCardID.Text = FormatCardID(DBNull2Str(.Item("CardID")))
                txtBirthDate.Text = DisplayStr2ShortDateTH(DBNull2Str(.Item("BirthDate")))
                txtTelephone.Text = DBNull2Str(.Item("Telephone"))
                txtMobile.Text = DBNull2Str(.Item("Mobile"))
                optAddressType.SelectedValue = DBNull2Str(.Item("AddressType"))
                txtAddress.Text = DBNull2Str(.Item("AddressNo"))
                txtRoad.Text = DBNull2Str(.Item("Road"))
                txtDistrict.Text = DBNull2Str(.Item("District"))
                txtCity.Text = DBNull2Str(.Item("City"))
                ddlProvince.SelectedValue = DBNull2Str(.Item("ProvinceID"))
                txtZipCode.Text = DBNull2Str(.Item("ZipCode"))
                optClaim.SelectedValue = DBNull2Str(.Item("MainClaim"))
                txtTimeContact.Text = DBNull2Str(.Item("TimeContact"))

                optAllergy.SelectedValue = DBNull2Str(.Item("isAllergy"))
                'txtDrugAllergy.Text = DBNull2Str(.Item("DrugAllergy"))

                'txtHomesss.Text = DBNull2Str(.Item("Homesss"))



                'txtHospital.Text = DBNull2Str(.Item("HospitalName"))
                'txtMedicalHistory.Text = DBNull2Str(.Item("MedicalHistory"))
                'txtDrugUse.Text = DBNull2Str(.Item("DrugUse"))

                chkClose.Checked = ConvertActive2Boolean(DBNull2Zero(.Item("Status")))
                LoadAllergy(StrNull2Zero(lblPatientID.Text))

                If String.Concat(.Item("Smoke")) <> "" Then
                    optSmoke.SelectedValue = String.Concat(.Item("Smoke"))
                End If
                txtCGYear.Text = String.Concat(.Item("SmokeYear"))
                txtCGNo.Text = String.Concat(.Item("SmokeCigarette"))

                If String.Concat(.Item("CigaretteType")) <> "" Then
                    optCigarette.SelectedValue = String.Concat(.Item("CigaretteType"))
                End If

                If String.Concat(.Item("Alcohol")) <> "" Then
                    optAlcohol.SelectedValue = String.Concat(.Item("Alcohol"))
                End If

                txtAlcoholFQ.Text = String.Concat(.Item("AlcoholFQ"))

                txtDrug1.Text = String.Concat(.Item("MedicationUsed1"))
                txtDrug2.Text = String.Concat(.Item("MedicationUsed2"))
                txtDrug3.Text = String.Concat(.Item("MedicationUsed3"))
                txtDrug4.Text = String.Concat(.Item("MedicationUsed4"))

                If (Session("LocationID") <> DBNull2Str(.Item("CreateBy"))) Then
                    cmdSave.Visible = False
                    cmdDelete.Visible = False

                    If Session("RoleID") = isAdminAccess Or Session("RoleID") = isSuperAdminAccess Then
                        cmdSave.Visible = True
                        cmdDelete.Visible = True
                    End If
                Else
                    cmdSave.Visible = True
                    cmdDelete.Visible = True
                End If


                LoadDataToComboBox()
                txtHN.Text = DBNull2Str(.Item("HN"))
                cboHospital.Value = DBNull2Str(.Item("HospitalName"))


            End With

            LoadPatientDeseaseToGrid()
            LoadDrugUse()


        End If
        dt = Nothing

    End Sub
    Private Sub LoadHospital()
        Dim ctlH As New HospitalController
        Dim dtH As New DataTable
        dtH = ctlH.Hospital_GetByStatus("A")
        cboHospital.DataSource = dtH
        cboHospital.ValueField = "HospitalUID"
        cboHospital.TextField = "HospitalName"
        cboHospital.DataBind()
    End Sub

    Private Sub ClearData()
        lblPatientID.Text = ""
        Session("patientid") = 0
        Session("patientid") = Nothing
        txtForeName.Text = ""
        txtSurname.Text = ""
        optGender.SelectedIndex = 0
        txtAges.Text = ""
        txtCardID.Text = ""
        txtBirthDate.Text = ""
        txtTelephone.Text = ""
        txtMobile.Text = ""
        txtTimeContact.Text = ""
        optAddressType.SelectedIndex = 0
        txtAddress.Text = ""
        txtRoad.Text = ""
        txtDistrict.Text = ""
        txtCity.Text = ""
        ddlProvince.SelectedIndex = 0
        optClaim.SelectedIndex = 0
        txtZipCode.Text = ""
        optAllergy.SelectedValue = "N"
        txtDrugAllergy.Text = ""
        'txtHomesss.Text = ""
        chkClose.Checked = True
        cmdSave.Visible = True
        cmdDelete.Visible = False
        txtDrugAllergy.Text = ""
        txtSymptom.Text = ""
        grdAllergy.Visible = False
        txtHN.Text = ""
        'txtHospital.Text = ""
        'txtMedicalHistory.Text = ""
        'txtDrugUse.Text = ""
        cboHospital.Text = ""
        txtDrug1.Text = ""
        txtDrug2.Text = ""
        txtDrug3.Text = ""
        txtDrug4.Text = ""
        optSmoke.SelectedIndex = 0
        txtCGYear.Text = ""
        txtCGNo.Text = ""
        optCigarette.SelectedValue = ""
        optAlcohol.SelectedIndex = 0
        txtAlcoholFQ.Text = ""

        txtDeseaseName.Text = ""
        txtDescription.Text = ""
        txtDrugCode.Text = ""
        txtDrugDescription.Text = ""
         
        grdDesease.Visible = False
        grdDrugUse.Visible = False
        grdAllergy.Visible = False

    End Sub
    Private Sub txtBirthDate_TextChanged(sender As Object, e As System.EventArgs) Handles txtBirthDate.TextChanged
        Try
            txtAges.Text = DateDiff(DateInterval.Year, CDate(txtBirthDate.Text), ctlP.GET_DATE_SERVER.Date)
            txtCardID.Focus()
            LoadDataToComboBox()
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        If Trim(txtForeName.Text) = "" Then
            DisplayMessage(Me.Page, "กรุณาป้อนชื่อผู้รับบริการก่อน")
            Exit Sub
        End If
        If Trim(txtSurname.Text) = "" Then
            DisplayMessage(Me.Page, "กรุณาป้อนนามสกุลผู้รับบริการก่อน")
            Exit Sub
        End If
        'If Trim(txtCardID.Text) = "" Then
        '    DisplayMessage(Me.Page, "กรุณาป้อนเลขบัตรประชาชนก่อน")
        '    Exit Sub
        'End If
        'If StrNull2Zero(txtAges.Text) = 0 Then
        '    DisplayMessage(Me.Page, "กรุณาป้อนอายุผู้รับบริการก่อน")
        '    'Exit Sub
        'End If
        If txtAges.Text = "" Then
            DisplayMessage(Me.Page, "กรุณาป้อนอายุผู้รับบริการก่อน")
            Exit Sub
        End If

        If Trim(txtMobile.Text) = "" Then
            DisplayMessage(Me.Page, "กรุณาป้อนเบอร์มือถือผู้รับบริการก่อน หากไม่มีให้ใส่เครื่องหมาย (-)")
            Exit Sub
        End If


        'Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))
        Dim BirthDate As String = ConvertStrDate2DBString(txtBirthDate.Text)
        Dim Hospital As String
        Hospital = cboHospital.Value

        If lblPatientID.Text = "" Then 'Add new

            If Trim(Replace(txtCardID.Text, "-", "")) <> "" Then
                If ctlP.Patient_ChkDupPatientByIDCard(Trim(Replace(txtCardID.Text, "-", ""))) = True Then
                    DisplayMessage(Me.Page, "ผู้รับบริการรายนี้มีอยู่ในฐานข้อมูลแล้ว")
                    Exit Sub
                End If
            End If

            If ctlP.Patient_ChkDupPatient(Trim(txtForeName.Text), Trim(txtSurname.Text)) = True Then
                DisplayMessage(Me.Page, "ผู้รับบริการรายนี้มีอยู่ในฐานข้อมูลแล้ว")
                Exit Sub
            End If

            'If ctlP.Patient_ChkDupPatient(txtCardID.Text) = True Then
            '    DisplayMessage(Me.Page, "หมายเลขบัตรประชาชนนี้มีอยู่ในฐานข้อมูลแล้ว")
            '    Exit Sub
            'End If


            ctlP.Patient_Add(txtForeName.Text, txtSurname.Text, optGender.SelectedValue, BirthDate, StrNull2Zero(txtAges.Text), Replace(txtCardID.Text, "-", ""), txtTelephone.Text, txtMobile.Text, txtTimeContact.Text, optAddressType.SelectedValue, txtAddress.Text, txtRoad.Text, txtDistrict.Text, txtCity.Text, ddlProvince.SelectedValue, ddlProvince.SelectedItem.Text, txtZipCode.Text, optEducation.SelectedValue, optOccupation.SelectedValue, optClaim.SelectedValue, Convert2Active(chkClose.Checked), Session("username"), CLng(ConvertStrDate2DBString(ctlP.GET_DATE_SERVER.Date)), Session("LocationID"), "", optAllergy.SelectedValue.ToString(), txtHN.Text, Hospital, StrNull2Zero(optSmoke.SelectedValue), StrNull2Zero(txtCGYear.Text), StrNull2Zero(txtCGNo.Text), StrNull2Zero(optCigarette.SelectedValue), StrNull2Zero(optAlcohol.SelectedValue), StrNull2Zero(txtAlcoholFQ.Text), txtDrug1.Text, txtDrug2.Text, txtDrug3.Text, txtDrug4.Text)

            objuser.User_GenLogfile(Session("Username"), ACTTYPE_ADD, "Patient", "เพิ่ม Patient: " & txtForeName.Text & " " & txtSurname.Text, "")

        Else
            ctlP.Patient_Update(lblPatientID.Text, txtForeName.Text, txtSurname.Text, optGender.SelectedValue, BirthDate, StrNull2Zero(txtAges.Text), Replace(txtCardID.Text, "-", ""), txtTelephone.Text, txtMobile.Text, txtTimeContact.Text, optAddressType.SelectedValue, txtAddress.Text, txtRoad.Text, txtDistrict.Text, txtCity.Text, ddlProvince.SelectedValue, ddlProvince.SelectedItem.Text, txtZipCode.Text, optEducation.SelectedValue, optOccupation.SelectedValue, optClaim.SelectedValue, Convert2Active(chkClose.Checked), Session("username"), "", optAllergy.SelectedValue.ToString(), txtHN.Text, Hospital, StrNull2Zero(optSmoke.SelectedValue), StrNull2Zero(txtCGYear.Text), StrNull2Zero(txtCGNo.Text), StrNull2Zero(optCigarette.SelectedValue), StrNull2Zero(optAlcohol.SelectedValue), StrNull2Zero(txtAlcoholFQ.Text), txtDrug1.Text, txtDrug2.Text, txtDrug3.Text, txtDrug4.Text)

            objuser.User_GenLogfile(Session("Username"), ACTTYPE_UPD, "Patient", "แก้ไข Patient: " & txtForeName.Text & " " & txtSurname.Text & " ID:" & lblPatientID.Text, "")
        End If


        'Response.Redirect("ResultPage.aspx")
        'ClearData()
        LoadPatientData()
        DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")

        LoadDataToComboBox()
    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub
    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadPatientData()
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadPatientData()
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                    'Case "imgDel"
                    '    If ctlP.Patient_Delete(e.CommandArgument) Then

                    '        objuser.User_GenLogfile(Session("Username"), ACTTYPE_DEL, "Patient", "ลบ Patient:" & e.CommandArgument, "")

                    '        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบข้อมูลเรียบร้อย');", True)
                    '        LoadPatientData()
                    '    Else
                    '        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง")
                    '    End If


            End Select


        End If
    End Sub

    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        If ctlP.Patient_CheckIsService(StrNull2Long(lblPatientID.Text)) Then
            DisplayMessage(Me.Page, "ไม่สามารถลบผู้รับบริการรายนี้ได้ เนื่องจากได้มีการบันทึกกิจกรรมให้บริการผู้รับบริการรายนี้ในระบบแล้ว")
            Exit Sub
        End If

        ctlP.Patient_Delete(StrNull2Long(lblPatientID.Text))
        DisplayMessage(Me.Page, "ลบข้อมูลเรียบร้อย")
        LoadPatientData()
    End Sub
    Private Sub LoadAllergy(pPatientID As Integer)
        Dim dtA As New DataTable

        dtA = ctlP.Patient_GetAllergy(pPatientID, txtForeName.Text.Trim() & txtSurname.Text.Trim())
        If dtA.Rows.Count > 0 Then
            optAllergy.SelectedValue = "Y"
            With grdAllergy
                .DataSource = dtA
                .DataBind()
                .Visible = True
            End With
        Else
            optAllergy.SelectedValue = "N"
            grdAllergy.Visible = False
            grdAllergy.DataSource = Nothing
        End If
        dt = Nothing
    End Sub

    Protected Sub imgAddAllergy_Click(sender As Object, e As ImageClickEventArgs) Handles imgAddAllergy.Click
        If txtForeName.Text = "" And txtSurname.Text = "" Then
            DisplayMessage(Me.Page, "กรุณาระบุชื่อ-นามสกุล ผู้ป่วยก่อน")
            Exit Sub
        End If


        If txtDrugAllergy.Text = "" Or txtSymptom.Text = "" Then
            DisplayMessage(Me.Page, "กรุณาระบุยาและอาการที่แพ้ก่อน")
            Exit Sub
        End If

        ctlP.PatientAllergy_Save(StrNull2Zero(lblPatientID.Text), txtForeName.Text.Trim() & txtSurname.Text.Trim(), txtDrugAllergy.Text, txtSymptom.Text)
        LoadAllergy(StrNull2Zero(lblPatientID.Text))
        txtDrugAllergy.Text = ""
        txtSymptom.Text = ""
        txtDrugAllergy.Focus()
    End Sub
    Dim ctlICD As New DeseaseController
    Protected Sub cmdAddDesease_Click(sender As Object, e As EventArgs) Handles cmdAddDesease.Click
        If txtForeName.Text = "" And txtSurname.Text = "" Then
            DisplayMessage(Me.Page, "กรุณาระบุชื่อ-นามสกุล ผู้ป่วยก่อน")
            Exit Sub
        End If

        If txtDeseaseName.Text = "" Then
            DisplayMessage(Me.Page, "ระบุโรคที่ต้องการเพิ่มก่อน")
            Exit Sub
        End If

        'Dim strD() As String
        'strD = Split(txtICD10Code.Text, ":")

        'If Not ctlICD.Desease_IsHas(strD(0).TrimEnd().ToUpper()) Then
        '    DisplayMessage(Me.Page, "กรุณาตรวจสอบโรคที่เพิ่มก่อน")
        '    Exit Sub
        'End If   strD(0).TrimEnd().ToUpper()

        ctlP.PatientDesease_Add(StrNull2Zero(lblPatientID.Text), txtDeseaseName.Text, txtDescription.Text, txtForeName.Text.Trim() + txtSurname.Text.Trim(), Session("userid"))
        LoadPatientDeseaseToGrid()

        txtDeseaseName.Text = ""
        txtDescription.Text = ""
    End Sub



    Protected Sub cmdSaveDrug_Click(sender As Object, e As EventArgs) Handles cmdSaveDrug.Click

        If txtForeName.Text = "" And txtSurname.Text = "" Then
            DisplayMessage(Me.Page, "กรุณาระบุชื่อ-นามสกุล ผู้ป่วยก่อน")
            Exit Sub
        End If

        If Session("patientid") Is Nothing Or Session("patientid") = "" Then
            ctlP.Patient_UpdateHerbByName(txtForeName.Text.Trim() & txtSurname.Text.Trim(), txtDrug1.Text, txtDrug2.Text, txtDrug3.Text, txtDrug4.Text)
        Else
            ctlP.Patient_UpdateHerb(StrNull2Zero(lblPatientID.Text), txtDrug1.Text, txtDrug2.Text, txtDrug3.Text, txtDrug4.Text)
        End If

    End Sub
    Private Sub LoadPatientDeseaseToGrid()
        If Session("patientid") Is Nothing Or Session("patientid") = "" Then
            dt = ctlP.PatientDesease_GetByName2(txtForeName.Text.Trim() & txtSurname.Text.Trim())
        Else
            dt = ctlP.PatientDesease_Get2(StrNull2Zero(Session("patientid")))
        End If
        grdDesease.Visible = True
        grdDesease.DataSource = dt
        grdDesease.DataBind()
    End Sub
    Private Sub LoadDrugUse()
        If Session("patientid") Is Nothing Or Session("patientid") = "" Then
            dt = ctlP.DrugUse_GetByName(txtForeName.Text.Trim() & txtSurname.Text.Trim())
        Else
            dt = ctlP.DrugUse_Get(StrNull2Zero(Session("patientid")))
        End If

        grdDrugUse.Visible = True
        grdDrugUse.DataSource = dt
        grdDrugUse.DataBind()
    End Sub
    Private Sub LoadDataToComboBox()
        LoadHospital()
    End Sub

    Protected Sub cmdAddDrug_Click(sender As Object, e As EventArgs) Handles cmdAddDrug.Click
        If txtForeName.Text = "" And txtSurname.Text = "" Then
            DisplayMessage(Me.Page, "กรุณาระบุชื่อ-นามสกุล ผู้ป่วยก่อน")
            Exit Sub
        End If


        If txtDrugCode.Text = "" Then
            DisplayMessage(Me.Page, "กรุณาเลือกยาที่ใช้ก่อน")
            LoadDataToComboBox()
            Exit Sub
        End If

        Dim iDrugUID As Integer = 0

        strD = Split(txtDrugCode.Text, ":")
        iDrugUID = ctlMed.Drug_GetUIDByCode(strD(0).TrimEnd())

        If Not ctlMed.Drug_IsHas(strD(0).TrimEnd()) Then
            DisplayMessage(Me.Page, "กรุณาตรวจสอบยาที่เพิ่มก่อน")
            LoadDataToComboBox()
            Exit Sub
        End If

        ctlP.DrugUse_Save(StrNull2Zero(lblPatientID.Text), iDrugUID, txtDrugDescription.Text, Session("UserID"), txtForeName.Text.Trim() + txtSurname.Text.Trim())

        LoadDrugUse()
        txtDrugDescription.Text = ""
        txtDrugCode.Text = ""
        LoadDataToComboBox()
    End Sub

    Private Sub grdDesease_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDesease.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Private Sub grdDesease_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDesease.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel_DS"
                    ctlP.PatientDesease_Delete(e.CommandArgument)
                    LoadPatientDeseaseToGrid()
            End Select
        End If
    End Sub

    Protected Sub grdDrugUse_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDrugUse.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel_DU"
                    ctlP.DrugUse_Delete(e.CommandArgument())
                    LoadDrugUse()
            End Select
        End If
    End Sub

    Private Sub grdDrugUse_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDrugUse.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Protected Sub grdAllergy_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdAllergy.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel_AG"
                    ctlP.PatientAllergy_Delete(e.CommandArgument())
                    LoadAllergy(StrNull2Zero(lblPatientID.Text))
            End Select
        End If
    End Sub

    Private Sub grdAllergy_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdAllergy.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Protected Sub optSmoke_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optSmoke.SelectedIndexChanged
        Select Case optSmoke.SelectedValue
            Case "2"
                txtCGYear.Visible = True
                lblCGYear.Visible = True
            Case "1"
                lblCGday.Visible = True
                txtCGNo.Visible = True
                lblCGYear.Visible = True
                txtCGYear.Visible = True
                optAlcohol.Visible = True
            Case Else
                txtCGNo.Visible = False
                lblCGday.Visible = False
                txtCGYear.Visible = False
                lblCGYear.Visible = False
                lblCgType.Visible = False
                optCigarette.Visible = False
        End Select
    End Sub

    Protected Sub optAlcohol_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optAlcohol.SelectedIndexChanged
        Select Case optAlcohol.SelectedValue
            Case "3"
                lblAlcohol.Visible = True
                txtAlcoholFQ.Visible = True
            Case Else
                lblAlcohol.Visible = False
                txtAlcoholFQ.Visible = False
        End Select
    End Sub
End Class