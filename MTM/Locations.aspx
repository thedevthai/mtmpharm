﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Locations.aspx.vb" Inherits=".Locations" %>

<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
               
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>ข้อมูลร้านยา
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">  
   <div class="row">
   <section class="col-lg-12 connectedSortable"> 
    <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลร้านยา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">




    <table width="99%" border="0"  cellpadding="0" cellspacing="2">
     
        <tr>
            <td  >             
            
<table border="0" cellPadding="1" cellSpacing="1">

                                                <tr>
                                                    <td>รหัสร้านยา </td>
                                                    <td >
                                                      
                                                              <asp:TextBox ID="txtLocationID" runat="server" BackColor="#FFFF91" 
                                                                  Font-Bold="True" ForeColor="#41A206" Width="150px"></asp:TextBox>
                                                         </td>
                                                  <td>&nbsp;</td>
                                                    <td>
                                                        <asp:HiddenField ID="hdLocationID_Old" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>กลุ่มร้านยา :</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlType" runat="server"  CssClass="OptionControl" Width="300px">                                                        </asp:DropDownList>                                                    </td>
                                                  <td>จังหวัด : </td>
                                                    <td><asp:DropDownList CssClass="OptionControl" 
                                                            ID="ddlProvince" runat="server" Width="200px"> </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                  <td>ชื่อร้านยา:</td>
                                                  <td><asp:TextBox ID="txtName" runat="server" Width="300px" ></asp:TextBox></td>
                                                    <td>ประเภทร้านยา:</td>
                                                     <td >
                                                         <table >
                                                             <tr>
                                                                 <td>
                                                        <asp:DropDownList ID="ddlTypeShop"  CssClass="OptionControl"  runat="server">
                                                            <asp:ListItem Selected="True">ร้านยาคุณภาพ</asp:ListItem>
                                                            <asp:ListItem Value="ร้านยา GPP">ร้านยา GPP</asp:ListItem>
                                                            <asp:ListItem Value="ร้านยาเภสัชกร"></asp:ListItem>
                                                                     </asp:DropDownList>                                                    
                                                                 </td><td>ระบุ</td>
                                                                 <td>                                                    
                                                        <asp:TextBox ID="txtTypeName" runat="server"></asp:TextBox>                                                    </td>
                                                             
                                                             </tr>
                                                         </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td  valign="top" class="texttopic">ที่อยู่ :</td>
                                                    <td><asp:TextBox ID="txtAddress" runat="server" Width="99%" TextMode="MultiLine" Height="50px"></asp:TextBox></td>
                                                    <td  valign="bottom" class="texttopic">รหัสไปรษณีย์:</td>
                                                    <td  valign="bottom" class="texttopic">
                                                        <asp:TextBox ID="txtZipCode" runat="server" 
                                                         MaxLength="5"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>โทรศัพท์ :                                                    </td>
                                                    <td><table width="100%" border="0" cellspacing="2" cellpadding="0">
                                                          <tr>
                                                            <td width="210"><asp:TextBox ID="txtTel" runat="server" Width="200px"></asp:TextBox></td>
                                                            <td width="60" class="texttopic">โทรสาร :</td>
                                                            <td><asp:TextBox ID="txtFax" runat="server" Width="200px"></asp:TextBox></td>
                                                          </tr>
                                                        </table></td>
                                                    <td>E-mail :</td>
                                              <td><asp:TextBox ID="txtMail" runat="server" 
                                                      Width="200px"></asp:TextBox></td>
          </tr>
                                                <tr>
                                                  <td>ชื่อเจ้าของ(คู่สัญญา)</td>
                                                  <td><table width="100%" border="0" cellspacing="2" cellpadding="0">
                                                        <tr>
                                                          <td width="210"><asp:TextBox ID="txtCoName" runat="server" Width="200px"></asp:TextBox></td>
                                                          <td width="60" class="texttopic">E-mail :</td>
                                                          <td><asp:TextBox ID="txtCoMail" runat="server" Width="200px"></asp:TextBox></td>
                                                    </tr>
                                                  </table></td>
                                                       <td>โทรศัพท์ :</td>
                                                       <td><asp:TextBox ID="txtCoTel" runat="server" Width="200px"></asp:TextBox></td>
          </tr>
                                                <tr>
                                                  <td>เลขที่บัญชี :</td>
                                                  <td>
                                                      <asp:TextBox ID="txtAccNo" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                                                  <td>ธนาคาร :</td>
                                                <td>
                                                    <asp:DropDownList ID="ddlBank"  CssClass="OptionControl" runat="server">                                                    </asp:DropDownList>                                                    </td>
          </tr>
                                                <tr>
                                                  <td>ชื่อบัญชี :</td>
                                                  <td>
                                                      <asp:TextBox ID="txtAccName" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                                                  <td>สาขา :</td>
                                                <td>
                                                    <asp:TextBox ID="txtBrunch" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                                                </tr>
                                                <tr>
                                                  <td>ประเภทบัญชี :</td>
                                                  <td colspan="3" >
                                                      <asp:RadioButtonList ID="optBankType" runat="server" 
                                                          RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True" Value="SAV">ออมทรัพย์/สะสมทรัพย์/เผื่อเรียก</asp:ListItem>
                                                    <asp:ListItem Value="DDA">กระแสรายวัน/เดินสะพัด</asp:ListItem>
                                                    <asp:ListItem Value="BOC">ฝากประจำ</asp:ListItem>
                                                  </asp:RadioButtonList></td>
                                                </tr>
                                                <tr>
                                                  <td>เลขบัตรประชาชน:</td>
                                                  <td >
                                                      <asp:TextBox ID="txtCardID" runat="server" Width="300px" MaxLength="13"></asp:TextBox>                                                    &nbsp;(เฉพาะตัวเลขเท่านั้น)</td>
                                                  <td>Status :</td>
                                                  <td><asp:CheckBox ID="chkStatus" runat="server" Checked="True" Text="Public" /></td>
                                                </tr>
                                                <tr>
                                                  <td>ปี พ.ศ.ที่สมัครเข้าโครงการ</td>
                                                  <td >       
                                                      <asp:TextBox ID="txtYear" runat="server" MaxLength="4"></asp:TextBox>
                                                    </td>
                                                  <td>&nbsp;</td>
                                                  <td>&nbsp;</td>
                                                </tr> 

     
  </table>     

            </td>
      </tr>
       <tr>
          <td align="center" valign="top"><asp:Label ID="lblValidate" runat="server" 
                  Visible="False" CssClass="validateAlert" Width="99%"></asp:Label></td>
      </tr>
       
    </table>
     </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
</section>
  </div>
  <div class="row">
    <section class="col-lg-6 connectedSortable"> 
         <div class="box box-solid box-success">
            <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">เภสัชกรที่ปฏิบัติหน้าที่</h3>             
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>                 
            </div>
            <div class="box-body">
                
         <table border="0" align="center" cellpadding="0" cellspacing="2">
      <tr>
         <td width="80">ID</td>
         <td width="220">
            <asp:Label ID="lblPersonID" runat="server"></asp:Label>                     </td>
         <td width="80">คำนำหน้า</td>
         <td><asp:DropDownList ID="ddlPrefix" runat="server" CssClass="OptionControl" Width="200px"> </asp:DropDownList></td>
      </tr>
       
      
       <tr>
         <td>ชื่อ</td>
         <td>
             <asp:TextBox ID="txtFirstName" runat="server" Width="200px"></asp:TextBox>           </td>
         <td>นามสกุล</td>
         <td><asp:TextBox ID="txtLastName" runat="server" Width="200px"></asp:TextBox></td>
       </tr>
      
       <tr>
         <td>ตำแหน่ง</td>
         <td colspan="3">
             <asp:TextBox ID="txtPositionName" runat="server" Width="200px"></asp:TextBox>           </td>
       </tr>
      
       <tr>
         <td colspan="4" align="center"><span class="texttopic">
          <asp:Button ID="cmdSavePerson" runat="server" CssClass="buttonSave" Text="บันทึกเภสัชกร"></asp:Button> 
         </span></td>
         </tr>
       
     </table>
                <br />

         <asp:GridView ID="grdPharmacist" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" CssClass="txtcontent" 
                             Font-Bold="False" Width="100%">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                        <asp:BoundField DataField="PrefixName" HeaderText="คำนำหน้า" >                            
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="90px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FirstName" HeaderText="ชื่อ" >
                            <HeaderStyle HorizontalAlign="Left" />                            </asp:BoundField>
                            <asp:BoundField DataField="LastName" HeaderText="นามสกุล" >
                            <HeaderStyle HorizontalAlign="Left" />                            </asp:BoundField>
                            <asp:BoundField HeaderText="ตำแหน่ง" DataField="PositionName">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEditP" runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem,"PersonID") %>' 
                                        ImageUrl="images/icon-edit.png" />
                                    &nbsp;
                                     <asp:ImageButton ID="imgDelP"  cssclass="gridbutton"  runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem,"PersonID") %>' 
                                        ImageUrl="images/delete.png" />                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" 
                                                            />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView>                

            </div>            
          </div>
    </section>
     <section class="col-lg-6 connectedSortable">  
         <div class="box box-solid box-success">
            <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">เครือข่ายโรงพยาบาล</h3>             
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>                 
            </div>
            <div class="box-body">
                 <table width="100%" >
            <tr>
              <td width="50" >เลือก รพ.</td>
              <td >
                     <dx:ASPxComboBox ID="ddlHospital" runat="server" CssClass="OptionControl2" Theme="MetropolisBlue" EnableTheming="True" ToolTip="พิมพ์ค้นหาได้" Width="100%">
                     </dx:ASPxComboBox>

                 </td>
              <td >
                  <asp:Button ID="cmdAddHos" runat="server" CssClass="buttonSave" Text="เพิ่ม รพ." Width="100px" />
                </td>
            </tr>     
            </table>
              <asp:GridView ID="grdHospital" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No." DataField="nRow">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
            <asp:BoundField DataField="HospitalName" HeaderText="โรงพยาบาล">
              <headerstyle HorizontalAlign="Left" />          
              <itemstyle HorizontalAlign="Left" />                      </asp:BoundField>
                <asp:BoundField DataField="ProvinceName" HeaderText="จังหวัด">
                <ItemStyle HorizontalAlign="Left" />                </asp:BoundField>
            <asp:TemplateField HeaderText="Delete">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgDelH" runat="server" 
                                    ImageUrl="images/delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>
            
     

            </div>            
          </div>
    </section>
  </div>
    <div align="center">
         <asp:Button ID="cmdSave" runat="server" CssClass="buttonSave" Text="บันทึก" Width="100px" />
&nbsp;<asp:Button ID="cmdClear" runat="server" CssClass="buttonCancle" Text="ยกเลิก" Width="100px" />
    </div>


  <div class="row">
   <section class="col-lg-12 connectedSortable"> 
           <div class="box box-solid box-success">
            <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">รายการร้านยา</h3>             
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>                 
            </div>
            <div class="box-body">
             <table border="0" cellspacing="1" cellpadding="0">
            <tr>
              <td width="50" >ค้นหา</td>
              <td >
                  <asp:TextBox ID="txtSearch" runat="server" Width="200px"></asp:TextBox>                </td>
              <td >
                  <asp:Button ID="cmdFind" runat="server" CssClass="buttonFind" Text="ค้นหา" />
                </td>
            </tr>
            
          </table>

              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No.">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
            <asp:BoundField DataField="LocationID" HeaderText="รหัส">
              <headerstyle HorizontalAlign="Center" />          
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
            <asp:BoundField DataField="LocationName" HeaderText="ร้านยา">
              <headerstyle HorizontalAlign="Left" />          
              <itemstyle HorizontalAlign="Left" />                      </asp:BoundField>
                <asp:BoundField DataField="ProvinceName" HeaderText="จังหวัด">
                <ItemStyle HorizontalAlign="Left" />                </asp:BoundField>
                <asp:BoundField DataField="LocationGroupName" HeaderText="ประเภทร้านยา" />
            <asp:TemplateField HeaderText="Public">
              <itemtemplate>
                <asp:Image ID="imgStatus" runat="server" ImageUrl="images/icon-ok.png" Visible='<%# DataBinder.Eval(Container.DataItem, "IsPublic") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "LocationID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgDel" runat="server" 
                                    ImageUrl="images/delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "LocationID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
      </tr>
                </table>
            </div>            
          </div>
   </section>
  </div>
</section></asp:Content>
