﻿Imports Microsoft.ApplicationBlocks.Data

Public Class ServiceTypeController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet
    Public Function ServiceType_GetAll() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "ServiceType_GetAll")
        Return ds.Tables(0)
    End Function
    Public Function ServiceType_GetByLocationID(LID As String, ProjID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "ServiceType_GetByLocationIDAndProjectID", LID, ProjID)
        Return ds.Tables(0)
    End Function

    Public Function ServiceType_GetByLocationID(LID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "ServiceType_GetByLocationID", LID)
        Return ds.Tables(0)
    End Function
    Public Function ServiceType_GetByProjectID(ProjID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "ServiceType_GetByProjectID", ProjID)
        Return ds.Tables(0)
    End Function

    Public Function ServiceType_GetByPatient(LID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "ServiceType_GetByPatient", LID)
        Return ds.Tables(0)
    End Function

    Public Function ServiceType_GetProfile(PID As Integer, LID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "ServiceType_GetProfile", PID, LID)
        Return ds.Tables(0)
    End Function
    Public Function ServiceType_GetByID(id As String) As DataSet
        SQL = "select * from ServiceType where ServiceTypeid='" & id & "'"
        Return SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
    End Function

    Public Function ServiceType_GetNameByID(id As String) As String
        SQL = "select Descriptions  from ServiceType where ServiceTypeid='" & id & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Str(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If

    End Function

    Public Function ServiceType_Add(ByVal pCode As String, ByVal pName As String, desc As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("ServiceType_Add"), pCode, pName, desc)
    End Function

    Public Function ServiceType_Update(ByVal pID_old As String, ByVal pID_new As String, ByVal pName As String, desc As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "ServiceType_Update", pID_old, pID_new, pName, desc)
    End Function

    Public Function ServiceType_Delete(ByVal pID As String) As Integer
        SQL = "delete from ServiceType where ServiceTypeid ='" & pID & "'"
        Return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, SQL)
    End Function
End Class
