﻿Imports Microsoft.ApplicationBlocks.Data

Public Class HospitalController

    Inherits ApplicationBaseClass
    Public ds As New DataSet
        Public Function Hospital_Get() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Hospital_Get"))
            Return ds.Tables(0)
        End Function
        Public Function Hospital_GetByUID(pUID As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Hospital_GetByUID"), pUID)
            Return ds.Tables(0)
        End Function
        Public Function Hospital_GetBySearch(pKey As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Hospital_GetBySearch"), pKey)
            Return ds.Tables(0)
        End Function
        Public Function Hospital_GetByStatus(pKey As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Hospital_GetByStatus"), pKey)
            Return ds.Tables(0)
        End Function

        Public Function Hospital_GetByDefinitionUID(pUID As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Hospital_GetByDefinitionUID"), pUID)
            Return ds.Tables(0)
        End Function

    Public Function Hospital_Add(ByVal Name As String, ByVal ProvID As Integer, Status As String, ByVal UpdBy As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Hospital_Add"), Name, ProvID, Status, UpdBy)

    End Function
    Public Function Hospital_Update(ByVal HospitalUID As Integer, ByVal Name As String, ByVal ProvID As Integer, Status As String, ByVal UpdBy As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Hospital_Update"), HospitalUID _
           , Name _
           , ProvID _
           , Status _
           , UpdBy)

    End Function
    Public Function Hospital_Delete(ByVal HospitalUID As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Hospital_Delete"), HospitalUID)
        End Function

    End Class

