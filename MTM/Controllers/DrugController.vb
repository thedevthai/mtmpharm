﻿Imports Microsoft.ApplicationBlocks.Data
Public Class DrugController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet
    Public Function Drug_GetAll() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Drug_GetAll"))
        Return ds.Tables(0)
    End Function
    Public Function Drug_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Drug_Get"))
        Return ds.Tables(0)
    End Function
    Public Function Drug_GetByID(PID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Drug_GetByID", PID)
        Return ds.Tables(0)
    End Function
    Public Function Drug_GetBySearch(PID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Drug_GetBySearch", PID)
        Return ds.Tables(0)
    End Function

    Public Function Drug_GetNameByUID(UID As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Drug_GetNameByUID", UID)
        If ds.Tables(0).Rows.Count > 0 Then
            Return String.Concat(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If
    End Function
    Public Function Drug_GetUIDByCode(Code As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Drug_GetUIDByCode", Code)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function


    Public Function Drug_GetName(Code As String) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Drug_GetName", Code)
        If ds.Tables(0).Rows.Count > 0 Then
            Return String.Concat(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If
    End Function

    Public Function Drug_Add(Name As String, Aliasname As String, Status As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Drug_Add", Name, Aliasname, Status)
    End Function
    Public Function Drug_Update(UID As Integer, Name As String, Aliasname As String, Status As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Drug_Update", UID, Name, Aliasname, Status)
    End Function

    Public Function Drug_Delete(UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Drug_Delete", UID)
    End Function


    Public Function Drug_IsHas(Code As String) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Drug_GetByCode", Code)
        If ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function


End Class
