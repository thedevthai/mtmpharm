﻿Imports Rajchasi
Public Class Locations
    Inherits System.Web.UI.Page
    Dim ctlUser As New UserController
    Dim ctlL As New LocationController
    Dim dt As New DataTable
    Dim ds As New DataSet
    Dim objEn As New CryptographyEngine
    Dim objUser As New UserController
    Dim ctlPs As New PersonController
    Dim ctlH As New HospitalController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            grdData.PageIndex = 0
            hdLocationID_Old.Value = ""
            LoadPrefixToDDL()

            LoadLocationToGrid()

            LoadProvinceToDDL()
            LoadBankToDDL()
            LoadLocationGroupToDDL()

            'GenLocationNumber()
            LoadHospitalToDLL()
        End If

        txtZipCode.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
    End Sub
    Private Sub LoadPrefixToDDL()
        dt = ctlPs.LoadPrefix
        If dt.Rows.Count > 0 Then
            With ddlPrefix
                .Enabled = True
                .DataSource = dt
                .DataTextField = "PrefixName"
                .DataValueField = "PrefixID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    'Private Sub GenLocationNumber()
    '    lblNewID.Text = objUser.genRunningNumber(ddlType.SelectedValue, ddlProvince.SelectedValue)
    '    txtLocationID.Text = ddlType.SelectedValue & ddlProvince.SelectedValue & lblNewID.Text
    'End Sub
    Private Sub LoadLocationToGrid()
        If Trim(txtSearch.Text) <> "" Then
            dt = ctlL.Location_GetBySearchAll("0", txtSearch.Text)
        Else
            dt = ctlL.Location_GetAll
        End If

        With grdData
            .Visible = True
            .DataSource = dt
            .DataBind()

            'For i = 0 To .Rows.Count - 1
            '    .Rows(i).Cells(0).Text = i + 1
            'Next
            Try


                Dim nrow As Integer = dt.Rows.Count
                If .PageCount > 1 Then
                    If .PageIndex > 0 Then
                        If (.PageSize) < nrow - (.PageSize * .PageIndex) Then
                            For i = 0 To .PageSize - 1
                                .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                            Next
                        Else
                            For i = 0 To nrow - (.PageSize * .PageIndex) - 1
                                .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                            Next
                        End If
                    Else
                        For i = 0 To .PageSize - 1
                            .Rows(i).Cells(0).Text = i + 1
                        Next
                    End If
                Else
                    For i = 0 To nrow - 1
                        .Rows(i).Cells(0).Text = i + 1
                    Next
                End If
            Catch ex As Exception
                'DisplayMessage(Me.Page, ex.Message)
            End Try
        End With

    End Sub
    Private Sub LoadHospitalToGrid()
        Dim dtP As New DataTable

        dtP = ctlL.Location_GetHospital(txtLocationID.Text)
        If dtP.Rows.Count > 0 Then
            With grdHospital
                .Visible = True
                .DataSource = dtP
                .DataBind()
            End With
        Else
            grdHospital.Visible = False
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadPharmacist()
        Dim dtP As New DataTable


        dtP = ctlPs.GetPerson_ByLocation(txtLocationID.Text)


        If dtP.Rows.Count > 0 Then
            With grdPharmacist
                .Visible = True
                .DataSource = dtP
                .DataBind()
            End With
        Else
            grdPharmacist.Visible = False
        End If
        dtP = Nothing
    End Sub
    Private Sub LoadProvinceToDDL()
        Dim ctlbase As New ApplicationBaseClass
        dt = ctlbase.LoadProvince
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadBankToDDL()
        Dim ctlbase As New ApplicationBaseClass
        dt = ctlbase.LoadBank
        If dt.Rows.Count > 0 Then
            With ddlBank
                .Enabled = True
                .DataSource = dt
                .DataTextField = "BankName"
                .DataValueField = "BankID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadLocationGroupToDDL()

        Dim ctlLG As New LocationGroupController
        dt = ctlLG.LocationGroup_Get
        If dt.Rows.Count > 0 Then
            With ddlType
                .Enabled = True
                .DataSource = dt
                .DataTextField = "Name"
                .DataValueField = "Code"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub



    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadLocationToGrid()
    End Sub


    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())

                    LoadHospitalToDLL()
                Case "imgDel"
                    If ctlL.Location_Delete(e.CommandArgument) Then

                        objUser.User_GenLogfile(Session("Username"), ACTTYPE_DEL, "Location", "ลบร้านยา:" & e.CommandArgument, "")

                        objUser.User_DeleteByUsername(e.CommandArgument)
                        objUser.User_GenLogfile(Session("Username"), "DEL", "User", "ลบ user :" & e.CommandArgument, "")

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบข้อมูลเรียบร้อย');", True)
                        LoadLocationToGrid()
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If


            End Select


        End If
    End Sub


    Private Sub EditData(ByVal pID As String)
        Dim dtE As New DataTable

        dtE = ctlL.Location_GetByID(pID)
        Dim objList As New LocationInfo
        If dtE.Rows.Count > 0 Then
            With dtE.Rows(0)
                isAdd = False

                Me.hdLocationID_Old.Value = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f00_LocationID).fldName))
                txtLocationID.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f00_LocationID).fldName))

                'txtCode.Text = String.Concat(.Item("LocationCode"))
                txtName.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f01_LocationName).fldName))


                ddlType.SelectedValue = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f02_LocationGroupID).fldName))

                ddlTypeShop.SelectedValue = DBNull2Str(.Item("LocationType"))
                txtTypeName.Text = DBNull2Str(.Item("TypeOther"))
                txtYear.Text = DBNull2Str(.Item("RegisYear"))

                txtAddress.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f06_Address).fldName))
                ddlProvince.SelectedValue = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f07_ProvinceID).fldName))
                Me.txtZipCode.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f09_ZipCode).fldName))

                txtTel.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f10_Office_Tel).fldName))
                txtFax.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f11_Office_Fax).fldName))
                txtCoName.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f12_Co_Name).fldName))

                txtCoMail.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f14_Co_Mail).fldName))
                txtCoTel.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f15_Co_Tel).fldName))

                txtAccNo.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f16_AccNo).fldName))
                txtAccName.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f17_AccName).fldName))
                ddlBank.SelectedValue = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f18_BankID).fldName))
                txtBrunch.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f19_BankBrunch).fldName))
                optBankType.SelectedValue = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f20_BankType).fldName))

                txtCardID.Text = DBNull2Str(dtE.Rows(0)("CardID"))

                chkStatus.Checked = CBool(dtE.Rows(0)(objList.tblField(objList.fldPos.f23_isPublic).fldName))

                LoadPharmacist()
            End With

            'chkProject.ClearSelection()
            'Dim dtLP As New DataTable
            'dtLP = ctlUser.LocationProject_GetByLocationID(lblID.Text)

            'If dtLP.Rows.Count > 0 Then
            '    'For i = 0 To dtLP.Rows.Count - 1
            '    '    Select Case dtLP.Rows(i)("ProjectID")
            '    '        Case "1"
            '    '            chkProject.Items(0).Selected = True
            '    '        Case "2"
            '    '            chkProject.Items(1).Selected = True
            '    '        Case "3"
            '    '            chkProject.Items(2).Selected = True
            '    '    End Select
            '    'Next
            'End If
            'dtLP = Nothing

            LoadHospitalToGrid()

        End If
        dtE = Nothing
        objList = Nothing
    End Sub

    Protected Function validateData() As Boolean
        Dim result As Boolean = True
        lblValidate.Text = ""


        If hdLocationID_Old.Value = "" Then
            If ddlType.SelectedIndex = -1 Then
                result = False
                lblValidate.Text &= "- กรุณาเลือกประเภทร้านยา  <br />"
                lblValidate.Visible = True
            End If
        End If

        If txtName.Text = "" Then
            result = False
            lblValidate.Text &= "- กรุณาระบุชื่อร้านยา  <br />"
            lblValidate.Visible = True

        End If
        'If chkProject.Items(0).Selected = False And chkProject.Items(1).Selected = False And chkProject.Items(2).Selected = False Then

        '    result = False
        '    lblValidate.Text &= "- เลือกโครงการที่จะให้สิทธิ์ก่อน <br />"
        '    lblValidate.Visible = True

        'End If

        'Dim n As Integer = 0
        'For i = 0 To optGroup.Items.Count - 1
        '    If optGroup.Items(i).Selected Then
        '        n = n + 1
        '    End If
        'Next

        'If n = 0 Then
        '    lblValidate.Text = "กรุณาเลือกประเภทร้านยา"
        '    lblValidate.Visible = True
        'End If

        Return result
    End Function


    Private Sub ClearData()
        Me.hdLocationID_Old.Value = ""
        txtLocationID.Text = ""
        'txtCode.Text = ""
        txtName.Text = ""
        txtAddress.Text = ""
        ddlProvince.SelectedIndex = 1
        Me.txtZipCode.Text = ""
        chkStatus.Checked = True
        txtCoName.Text = ""
        txtCoMail.Text = ""
        txtAccNo.Text = ""
        txtAccName.Text = ""
        txtBrunch.Text = ""
        txtCoTel.Text = ""
        txtTel.Text = ""
        txtFax.Text = ""
        txtSearch.Text = ""
        grdHospital.Visible = False
        grdPharmacist.Visible = False

        'GenLocationNumber()
    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(7).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If


    End Sub

    Private Sub LoadHospitalToDLL()
        dt = ctlH.Hospital_GetByStatus("A")
        ddlHospital.DataSource = dt
        ddlHospital.ValueField = "HospitalUID"
        ddlHospital.TextField = "HospitalName"
        ddlHospital.DataBind()
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If validateData() Then

            lblValidate.Visible = False
            Dim item As Integer

            If hdLocationID_Old.Value = "" Then
                item = ctlL.Location_Add(txtLocationID.Text, txtName.Text, ddlType.SelectedValue, ddlTypeShop.SelectedValue, txtTypeName.Text, txtAddress.Text, ddlProvince.SelectedValue, ddlProvince.SelectedItem.Text, txtZipCode.Text, txtTel.Text, txtFax.Text, txtCoName.Text, txtCoMail.Text, txtCoTel.Text, txtAccNo.Text, txtAccName.Text, ddlBank.SelectedValue, txtBrunch.Text, optBankType.SelectedValue, Session("Username"), Boolean2Decimal(chkStatus.Checked), txtMail.Text, txtCardID.Text, txtYear.Text)

                objUser.User_GenLogfile(Session("Username"), ACTTYPE_ADD, "Locations", "เพิ่มใหม่ ร้านยา:" & txtName.Text, "")

                item = objUser.User_Add(txtLocationID.Text, objEn.EncryptString("1234", True), txtName.Text, ddlProvince.SelectedItem.Text, 0, 1, 1, txtLocationID.Text, "", 0, 1)

                objUser.User_GenLogfile(Session("Username"), ACTTYPE_ADD, "Users", "add new user :" & txtLocationID.Text, "Result:" & item)

                'objUser.RunningNumber_Update(ddlType.SelectedValue, ddlProvince.SelectedValue)
            Else

                item = ctlL.Location_Update(hdLocationID_Old.Value, txtLocationID.Text, txtName.Text, ddlType.SelectedValue, ddlTypeShop.SelectedValue, txtTypeName.Text, txtAddress.Text, ddlProvince.SelectedValue, ddlProvince.SelectedItem.Text, txtZipCode.Text, txtTel.Text, txtFax.Text, txtCoName.Text, txtCoMail.Text, txtCoTel.Text, txtAccNo.Text, txtAccName.Text, ddlBank.SelectedValue, txtBrunch.Text, optBankType.SelectedValue, Session("Username"), Boolean2Decimal(chkStatus.Checked), txtMail.Text, txtCardID.Text, txtYear.Text)

                objUser.User_UpdateUsername(hdLocationID_Old.Value, txtLocationID.Text, Session("UserID"))

                objUser.User_GenLogfile(Session("Username"), ACTTYPE_UPD, "Locations", "แก้ไข ร้านยา:" & txtName.Text, "")

            End If

            'ctlUser.User_AddLocationProject(1, txtLocationID.Text, "A")

            'For i = 0 To chkProject.Items.Count - 1
            'ctlUser.User_AddLocationProject(i + 1, LocationID, Boolean2StatusFlag(chkProject.Items(i).Selected))
            'Next

            LoadLocationToGrid()
            ClearData()
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','บันทึกข้อมูลเรียบร้อย');", True)

        End If
    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadLocationToGrid()
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlType.SelectedIndexChanged
        'GenLocationNumber()
    End Sub

    Protected Sub ddlProvince_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvince.SelectedIndexChanged
        'GenLocationNumber()
    End Sub

    Protected Sub cmdAddHos_Click(sender As Object, e As EventArgs) Handles cmdAddHos.Click
        If txtLocationID.Text = "" Then
            DisplayMessage(Me.Page, "กรุณระบุรหัสร้านยาก่อน")
            Exit Sub
        End If
        If txtName.Text = "" Then
            DisplayMessage(Me.Page, "กรุณระบุชื่อร้านยาก่อน")
            Exit Sub
        End If

        ctlL.LocationHospital_Save(txtLocationID.Text, StrNull2Zero(ddlHospital.Value), txtName.Text.Trim())
        LoadHospitalToGrid()
    End Sub

    Protected Sub grdHospital_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdHospital.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(3).FindControl("imgDelH")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Protected Sub grdHospital_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdHospital.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDelH"
                    ctlL.LocationHospital_Delete(e.CommandArgument)
                    LoadHospitalToGrid()
            End Select
        End If
    End Sub

    Protected Sub grdPharmacist_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdPharmacist.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(4).FindControl("imgDelP")
            imgD.Attributes.Add("onClick", scriptString)

        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Protected Sub grdPharmacist_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdPharmacist.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEditP"
                    EditPersonData(e.CommandArgument())
                Case "imgDelP"
                    If ctlPs.Person_Delete(e.CommandArgument) Then
                        objUser.User_GenLogfile(Session("Username"), "DEL", "Persons", "ลบเภสัชกรประจำร้านยา :" & e.CommandArgument, "")
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If

                    LoadPharmacist()

            End Select

        End If
    End Sub

    Protected Sub cmdSavePerson_Click(sender As Object, e As EventArgs) Handles cmdSavePerson.Click
        If txtFirstName.Text = "" Or txtLastName.Text = "" Then
            DisplayMessage(Me.Page, "กรุณป้อนข้อมูลให้ครบถ้วน")
            Exit Sub
        End If
        If txtLocationID.Text = "" Then
            DisplayMessage(Me.Page, "กรุณระบุรหัสร้านยาก่อน")
            Exit Sub
        End If


        If lblPersonID.Text = "" Then

            ctlPs.Person_Add(ddlPrefix.SelectedValue, txtFirstName.Text, txtLastName.Text, txtPositionName.Text, txtLocationID.Text, Session("Username"))

            objUser.User_GenLogfile(Session("Username"), ACTTYPE_ADD, "Persons", "เพิ่มรายชื่อเภสัชกรประจำร้านยาใหม่ :" & txtFirstName.Text & " " & txtLastName.Text, "เพิ่มแบบทีละคน")
        Else
            ctlPs.Person_Update(StrNull2Zero(lblPersonID.Text), ddlPrefix.SelectedValue, txtFirstName.Text, txtLastName.Text, txtPositionName.Text, txtLocationID.Text, Session("Username"))
            objUser.User_GenLogfile(Session("Username"), ACTTYPE_UPD, "Persons", "แก้ไขชื่อเภสัชกรประจำร้านยาใหม่ :" & txtFirstName.Text & " " & txtLastName.Text, "เพิ่มแบบทีละคน")

        End If

        isAdd = True
        LoadPharmacist()
        ClearPersonData()
        LoadHospitalToGrid()
    End Sub
    Private Sub EditPersonData(ByVal pID As String)
        ClearPersonData()

        dt = ctlPs.GetPerson_ByID(pID)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                lblPersonID.Text = .Item("PersonID")

                If String.Concat(.Item("PrefixID")) <> "" Then
                    Me.ddlPrefix.SelectedValue = String.Concat(.Item("PrefixID"))
                End If

                Me.txtFirstName.Text = String.Concat(.Item("FirstName"))
                txtLastName.Text = String.Concat(.Item("LastName"))
                txtPositionName.Text = String.Concat(.Item("PositionName"))
            End With

        End If

        dt = Nothing
    End Sub

    Private Sub ClearPersonData()
        txtFirstName.Text = ""
        txtLastName.Text = ""
        ddlPrefix.SelectedIndex = 0
        lblPersonID.Text = ""
        txtPositionName.Text = ""
    End Sub
End Class

